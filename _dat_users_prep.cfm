<cfparam name="FORM.CSP_Page_Form" default="">
<cfparam name="FORM.CSP_Per_Page" default="5">
<cfparam name="FORM.CSP_Current_Page" default="1">
<cfparam name="VARIABLES.Page_Start" default="1">
<cfparam name="VARIABLES.Page_End" default="5">

<cfparam name="FORM.User_Cust_ID" default="">
<cfparam name="FORM.User_EmailAddress" default="">
<cfparam name="FORM.User_FirstName" default="">
<cfparam name="FORM.User_LastName" default="">

<cfscript>
	UsersInfoObj = CreateObject("component", "_cfcs.UserInfo");
	UsersCount = DeserializeJSON(UsersInfoObj.GetUsersCount(FORM.User_Cust_ID,FORM.User_EmailAddress,FORM.User_FirstName,FORM.User_LastName));
	UsersList = DeserializeJSON(UsersInfoObj.GetUsersList(FORM.CSP_Current_Page,FORM.CSP_Per_Page,FORM.User_Cust_ID,FORM.User_EmailAddress,FORM.User_FirstName,FORM.User_LastName));
	
	VARIABLES.Page_Start = ((FORM.CSP_Current_Page-1)*FORM.CSP_Per_Page)+1;
	VARIABLES.Page_End = FORM.CSP_Current_Page*FORM.CSP_Per_Page;
</cfscript>