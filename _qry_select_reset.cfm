<cfparam name="QUERY" default="#StructNew()#">
<cfparam name="QUERY.QueryName" default="qry_getUser">
<cfparam name="QUERY.Datasource" default="#APPLICATION.Datasource#">
<cfparam name="QUERY.Reset_Exp" default="#CreateODBCDateTime(DateAdd('h', -2, Now()))#"><!---//2 hour timelimit on reset--->
<cfparam name="QUERY.Reset_IsCompleted" default="0">

<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#">
	SELECT rest.*
	FROM [ServicePortal].[dtbl_Pass_Reset] rest
	WHERE 0=0
	<cfif StructKeyExists(QUERY,"Reset_IsCompleted")>
	AND rest.Reset_IsCompleted = <cfqueryparam cfsqltype="cf_sql_tinyint" value="#QUERY.Reset_IsCompleted#">
	</cfif>
	<cfif StructKeyExists(QUERY,"Reset_UID")>
	AND rest.Reset_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.Reset_UID#">
	</cfif>
	<cfif StructKeyExists(QUERY,"Reset_Exp")>
	AND rest.Reset_DTS >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#QUERY.Reset_Exp#">
	</cfif>
</cfquery>
<cfif StructClear(QUERY)></cfif>