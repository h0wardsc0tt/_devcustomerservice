<cftry> 
	<!---<cfscript> 
        ws = CreateObject("webservice", "http://powaxdev:8101/DynamicsAx/Services/HMECustomerService"); 
        ws.create(Company = "HMEC"); 
    </cfscript>--->
    
    <cfinvoke webservice="http://powaxdev:8101/DynamicsAx/Services/HMECustomerService" method="create" returnvariable="getDataResult">
        <cfinvokeargument name="Company" value="HMEC" />
    </cfinvoke>
    
    <cfdump var="#getDataResult#" />

    <cfcatch type="Any"> 
        <cfdump var="#cfcatch#">
    </cfcatch> 
</cftry>


<!---
<cfsavecontent variable="soapBody">
    <cfoutput>

        <?xml version="1.0" encoding="utf-8" ?> 
        <soap:Envelope
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">

            <soap:Body>
            	<CustomerService.create xmlns="http://schemas.microsoft.com/dynamics/2008/01/services/CustomerService/create">
                    <Company>hmec</Company>
                    <AccountNum>WS005</AccountNum>
                    <CustGroup>30</CustGroup>
                    <LanguageID>en-us</LanguageID>
                    <Name>Web Service Customer 005</Name>
                </CustomerService.create>
            </soap:Body>
        </soap:Envelope>

    </cfoutput>
</cfsavecontent>

<cfhttp url="http://powaxdev:8101/DynamicsAx/Services/HMECustomerService" method="post" result="httpResponse">
    <cfhttpparam type="header" name="SOAPAction" value="http://schemas.microsoft.com/dynamics/2008/01/services/CustomerService/create" />
    <cfhttpparam type="header" name="accept-encoding" value="no-compression" />
    <cfhttpparam type="xml" value="#trim(soapBody)#" />
</cfhttp>

<cfif find( "200", httpResponse.statusCode )>
    <cfset soapResponse = xmlParse( httpResponse.fileContent ) />
    <cfset responseNodes = xmlSearch(soapResponse,"//*[ local-name() = 'HMECustomerService.create' ]") />

    <cfoutput>
        Code: #responseNodes[ 1 ].Code.xmlText#
        <br />
        Success: #responseNodes[ 1 ].Message.xmlText#
    </cfoutput>
<cfelse>
	<cfdump var="#httpResponse#">
</cfif>
--->