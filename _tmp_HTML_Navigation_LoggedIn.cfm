
<!--- Only show if User is logged in and not at Login Screen --->
<cfif NOT User_IsAtLogin>
    <nav class="navbar navbar">
        <div class="container">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span> 
                    </button>
                    <a class="navbar-brand navbar-logo" href="<cfif User_IsAdmin>./?pg=Customers<cfelse>./?pg=Orders</cfif>"><img src="./images/hme-companies.png" alt="HME" <!---width="80"---> height="35" /></a>
                    <a class="navbar-brand" href="#">Customer Service Portal</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav navbar-right">
                        <cfif User_IsFullAdmin OR ListFind(ValueList(User_Apps.App_FullName), "PurchaseRequisition") NEQ 0>
                            <li><a href="./?pg=PurchaseRequisitionLookUp">Purchase Requisition</a></li>
                        </cfif>
                        <cfif User_IsFullAdmin OR ListFind(ValueList(User_Apps.App_FullName), "Customers") NEQ 0>
                            <li><a href="./?pg=Customers">Customers</a></li>
                        </cfif>
                        <cfif User_IsFullAdmin OR ListFind(ValueList(User_Apps.App_FullName), "Orders") NEQ 0>
                        	<li><a href="./?pg=Orders">Orders</a></li>
                        </cfif>
                        <cfif User_IsFullAdmin OR ListFind(ValueList(User_Apps.App_FullName), "Invoices") NEQ 0>
                        	<li><a href="./?pg=Invoices">Invoices</a></li>
                        </cfif>
                        <cfif User_IsFullAdmin OR ListFind(ValueList(User_Apps.App_FullName), "Store") NEQ 0>
                        	<li><a href="./?pg=Products">Store</a></li>
                        </cfif>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);"><span class="glyphicon glyphicon-cog"></span></a>
                            <ul class="dropdown-menu">
                                <cfif User_IsFullAdmin OR ListFind(ValueList(User_Apps.App_FullName), "User Management") NEQ 0>
                                    <li><a href="./?pg=Users">User Management</a></li>
                                </cfif>
                                <li><a href="./?pg=Account">My Account</a></li>
                                <li><a href="./?pg=Logout">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    
    <cfif (CGI.QUERY_STRING CONTAINS "pg=Products" OR CGI.QUERY_STRING CONTAINS "pg=Cart") AND (User_IsFullAdmin OR ListFind(ValueList(User_Apps.App_FullName), "Store") NEQ 0)>
    	<div class="navbar-store animated fadeInDown">
        	<div class="container no-pad">
                <ul class="nav navbar-nav navbar-left col-lg-12 no-pad">
                	<li class="col-lg-3 no-pad">
                    	<ul class="nav navbar-nav navbar-left col-lg-12 no-pad">
                            <li class="dropdown no-pad">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">Products<span class="glyphicon glyphicon-triangle-bottom"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="./?pg=Account">Accessories</a></li>
                                    <li><a href="./?pg=Logout">Headsets</a></li>
                                    <li><a href="./?pg=Logout">Batteries</a></li>
                                </ul>
                            </li>
                            <li class="dropdown no-pad">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">Company<span class="glyphicon glyphicon-triangle-bottom"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="./?pg=Account">HSC</a></li>
                                    <li><a href="./?pg=Logout">JTECH</a></li>
                                    <li><a href="./?pg=Logout">CE REPAIRS</a></li>
                                    <li><a href="./?pg=Logout">CLEAR-COM</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="col-lg-6">
                    	<div class="product-search">
                            <form action="./?pg=Products&st=View" method="POST" name="Product_Form" id="Product_Form">
                            	<div class="col-lg-11 mini-pad">
                                	<input type="text" class="form-control" id="Product_Search" name="Product_Search" placeholder="Search by" value="<cfoutput>#FORM.Product_Search#</cfoutput>"/>
                                </div>
                                <div class="col-lg-1 no-pad">
                                	<button class="btn btn-primary" id="Product_Search_Submit" name="Product_Search_Submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </div>
                            </form>
                        </div>
                    </li>
                    <li class="col-lg-3 no-pad">
                        <a class="product-cart product-shopping-cart pull-right" href="./?pg=Cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Cart<span class="product-cart-count-symbol">0</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </cfif>
</cfif>

