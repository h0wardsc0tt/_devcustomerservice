<cfcomponent>
	<cffunction name="GetUsersList" access="public" returntype="any">
        <cfargument name="PageNumber" type="string" required="yes">
        <cfargument name="ItemsPerPage" type="string" required="yes">
		<cfargument name="User_Cust_ID" type="string" required="no" default="">
        <cfargument name="User_EmailAddress" type="string" required="no" default="">
        <cfargument name="User_FirstName" type="string" required="no" default="">
        <cfargument name="User_LastName" type="string" required="no" default="">
        <cfargument name="User_UID" type="string" required="no" default="">
        
        <cfscript>
			QUERY = StructNew();
			QUERY.QueryName = "getUsers";
			if(Len(User_UID) NEQ 0){
				QUERY.User_UID = User_UID;
			}
			if(Len(User_Cust_ID) NEQ 0){
				QUERY.User_Cust_ID = User_Cust_ID;
			}
			if(Len(User_FirstName) NEQ 0){
				QUERY.User_FirstName = User_FirstName;
			}
			if(Len(User_LastName) NEQ 0){
				QUERY.User_LastName = User_LastName;
			}
			if(Len(User_EmailAddress) NEQ 0){
				QUERY.User_EmailAddress = User_EmailAddress;
			}
			QUERY.PageNumber = PageNumber;
			QUERY.ItemsPerPage = ItemsPerPage;
			QUERY.User_IsVerified = 1;
			QUERY.SelectSQL = "[User_UID],[User_Cust_ID],[User_IsVerified],[User_IsActive],[User_FirstName],[User_LastName],[User_EmailAddress],[User_Address_Line1],[User_Address_Line2],[User_Phone_Number],[User_Locality],[User_Region],[User_PostCode],[User_Country],[User_Company],[User_CompanyName],[UserType_Name]";
			QUERY.OrderBy = "[User_Cust_ID],[User_IsVerified],[User_IsActive],[User_FirstName],[User_LastName],[User_EmailAddress],[User_Address_Line1],[User_Address_Line2],[User_Phone_Number],[User_Locality],[User_Region],[User_PostCode],[User_Country],[User_Company],[User_CompanyName],[User_UID],[UserType_Name]";
		</cfscript>
		<cfinclude template="../_qry_select_users.cfm">

		<cfset myResult = SerializeJSON(getUsers,true)>
		<cfreturn myResult>
	</cffunction>
    
    <cffunction name="GetUsersCount" access="public" returntype="any">
		<cfargument name="User_Cust_ID" type="string" required="no" default="">
        <cfargument name="User_EmailAddress" type="string" required="no" default="">
        <cfargument name="User_FirstName" type="string" required="no" default="">
        <cfargument name="User_LastName" type="string" required="no" default="">
        <cfargument name="User_UID" type="string" required="no" default="">
        
        <cfscript>
			QUERY = StructNew();
			QUERY.QueryName = "getUsersCount";
			if(Len(User_UID) NEQ 0){
				QUERY.User_UID = User_UID;
			}
			if(Len(User_Cust_ID) NEQ 0){
				QUERY.User_Cust_ID = User_Cust_ID;
			}
			if(Len(User_FirstName) NEQ 0){
				QUERY.User_FirstName = User_FirstName;
			}
			if(Len(User_LastName) NEQ 0){
				QUERY.User_LastName = User_LastName;
			}
			if(Len(User_EmailAddress) NEQ 0){
				QUERY.User_EmailAddress = User_EmailAddress;
			}
			QUERY.User_IsVerified = 1;
			QUERY.SelectSQL = "COUNT([User_UID]) AS ITEMCOUNT";
		</cfscript>
		<cfinclude template="../_qry_select_users_count.cfm">

		<cfset myResult = getUsersCount.ITEMCOUNT>
		<cfreturn myResult>
	</cffunction>
    
    <cffunction name="getUserCustID" access="public" returntype="any">
        <cfargument name="User_EmailAddress" type="string" required="no" default="">
        
        <cfquery name="qry_getUserCustID" datasource="#APPLICATION.Datasource#" maxrows="1">
        	SELECT [User_Cust_ID]
            FROM [ServicePortal].[tbl_Users]
            WHERE [User_EmailAddress] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#User_EmailAddress#">
        </cfquery>
        
     	<cfset myResult = qry_getUserCustID.User_Cust_ID>
		<cfreturn myResult>
	</cffunction>
</cfcomponent>
