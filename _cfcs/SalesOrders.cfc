<cfcomponent>

	<!---                                                                  --->
	<!---                      VALIDATE SALES ORDER                        --->
    <!---                                                                  --->
    
	<cffunction name="ValidateSalesOrder" access="public" returntype="string">
    	<cfargument name="Company" type="string" required="yes">
		<cfargument name="SalesId" type="string" required="yes">
        
        <!--- Create SalesOrderRead XML --->
        <cfsavecontent variable="soapBody">
			<cfoutput>
                <?xml version="1.0" encoding="utf-8"?>
                <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dat="http://schemas.microsoft.com/dynamics/2010/01/datacontracts" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:ser="http://schemas.microsoft.com/dynamics/2008/01/services" xmlns:ent="http://schemas.microsoft.com/dynamics/2006/02/documents/EntityKeyList" xmlns:ent1="http://schemas.microsoft.com/dynamics/2006/02/documents/EntityKey">
					<soapenv:Header>
                      <dat:CallContext>
                         <dat:Company>#Arguments.Company#</dat:Company>
                         <dat:LogonAsUser>HME\chrisg</dat:LogonAsUser>
                      </dat:CallContext>
					</soapenv:Header>
                    <soapenv:Body>
                        <ser:SalesOrderServiceReadRequest>
                        <ent:EntityKeyList>
                            <ent1:EntityKey>
                                <ent1:KeyData>
                                    <ent1:KeyField>
                                        <ent1:Field>SalesId</ent1:Field>
                                        <ent1:Value>#Arguments.SalesId#</ent1:Value>
                                    </ent1:KeyField>
                                </ent1:KeyData>
                            </ent1:EntityKey>
                        </ent:EntityKeyList>
                        </ser:SalesOrderServiceReadRequest>
                    </soapenv:Body>
                </soapenv:Envelope>
        	</cfoutput>
        </cfsavecontent>
        
        <cfhttp url="http://powaxdev.hme.com:83/MicrosoftDynamicsAXAif60/WebSalesOrder/xppservice.svc?wsdl" method="POST" result="soapResponse" domain="HME" username="chrisg" password="Shipping5" authType="NTLM" redirect="true">
            <cfhttpparam type="header" name="accept-encoding" value="no-compression" />
            <cfhttpparam type="header" name="SOAPAction" value="http://schemas.microsoft.com/dynamics/2008/01/services/SalesOrderService/read" />
            <cfhttpparam type="xml" value="#trim(soapBody)#" />
        </cfhttp>
        
        <cfif soapResponse.Statuscode CONTAINS "200">
        	<cfset responseNodes = xmlSearch(soapResponse.fileContent,"//*[local-name()='SalesOrder']")>
			<!---<cfset myResult = responseNodes[1].SalesTable.SalesId.xmlText>--->
            <cfset myResult = "Sales order is valid.">
        <cfelse>
        	<!--- AX ERROR --->
            <!---
        	<cfset responseNodes = xmlSearch(soapResponse.fileContent,"//*[local-name()='Message']")>
        	<cfset myResult = responseNodes[1]>
            --->
            <cfset myResult = "Invalid Sales order.">
        </cfif>
        
		<cfreturn myResult>
	</cffunction>
    
    <!---                                                                --->
    <!---                      CREATE SALES ORDER                        --->
    <!---                                                                --->
    
    <cffunction name="CreateSalesOrder" access="public" returntype="string">
    	<cfargument name="Company" type="string" required="yes">
        <cfargument name="CurrencyCode" type="string" required="yes">
        <cfargument name="CustAccount" type="string" required="yes">
        <cfargument name="PurchOrderFormNum" type="string" required="no" default="">
        <cfargument name="ReceiptDateRequested" type="string" required="no" default="#DateFormat(Now(),'yyyy-mm-dd')#">
        <cfargument name="SalesLines" type="string" required="yes">
        
        <cfset SalesLinesObj = deserializeJSON(SalesLines)>
        
        <!--- Create SalesOrderCreate XML --->
        <cfsavecontent variable="soapBody">
			<cfoutput>
                <?xml version="1.0" encoding="utf-8"?>
                <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dat="http://schemas.microsoft.com/dynamics/2010/01/datacontracts" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:ser="http://schemas.microsoft.com/dynamics/2008/01/services" xmlns:sal="http://schemas.microsoft.com/dynamics/2008/01/documents/SalesOrder" xmlns:shar="http://schemas.microsoft.com/dynamics/2008/01/sharedtypes">
                    <soapenv:Header>
                        <dat:CallContext>
                            <dat:Company>#Arguments.Company#</dat:Company>
                            <dat:LogonAsUser>HME\chrisg</dat:LogonAsUser>
                        </dat:CallContext>
                    </soapenv:Header>
					<soapenv:Body>
                        <SalesOrderServiceCreateRequest xmlns="http://schemas.microsoft.com/dynamics/2008/01/services">
                            <SalesOrder xmlns="http://schemas.microsoft.com/dynamics/2008/01/documents/SalesOrder">
                                <SalesTable class="entity">
                                    <CurrencyCode>#Arguments.CurrencyCode#</CurrencyCode>
                                    <CustAccount>#Arguments.CustAccount#</CustAccount>
                                    <PurchOrderFormNum>#Arguments.PurchOrderFormNum#</PurchOrderFormNum>
                                    <ReceiptDateRequested>#Arguments.ReceiptDateRequested#</ReceiptDateRequested>
                                    <cfloop from="1" to="#ArrayLen(SalesLinesObj.SalesLines)#" index="SalesLine">
                                        <SalesLine class="entity">
                                            <ItemId>#SalesLinesObj.SalesLines[SalesLine].ItemId#</ItemId>
                                            <SalesQty>#SalesLinesObj.SalesLines[SalesLine].SalesQty#</SalesQty>
                                            <SalesUnit>#SalesLinesObj.SalesLines[SalesLine].SalesUnit#</SalesUnit>
                                        </SalesLine>
                                    </cfloop>
                                </SalesTable>
                            </SalesOrder>
                        </SalesOrderServiceCreateRequest>
					</soapenv:Body>
                </soapenv:Envelope>
            </cfoutput>
        </cfsavecontent>
        
        <cfhttp url="http://powaxdev.hme.com:83/MicrosoftDynamicsAXAif60/WebSalesOrder/xppservice.svc?wsdl" method="POST" result="soapResponse" domain="HME" username="chrisg" password="Shipping5" authType="NTLM" redirect="true">
            <cfhttpparam type="header" name="accept-encoding" value="no-compression" />
            <cfhttpparam type="header" name="SOAPAction" value="http://schemas.microsoft.com/dynamics/2008/01/services/SalesOrderService/create" />
            <cfhttpparam type="xml" value="#trim(soapBody)#"/>
        </cfhttp>
        
        <cfif soapResponse.Statuscode CONTAINS "200">
        	<cfset responseNodes = xmlSearch(soapResponse.fileContent,"//*[local-name()='SalesOrder']")>
			<!---<cfset myResult = responseNodes[1].SalesTable.SalesId.xmlText>--->
            <cfset myResult = "Sales order created.">
        <cfelse>
        	<!--- AX ERROR --->
        	<cfset responseNodes = xmlSearch(soapResponse.fileContent,"//*[local-name()='Message']")>
            
            <cfif responseNodes[1].xmlText CONTAINS "PO number is required">
            	<cfset myResult = "PO number is required.">
            <cfelse>
            	<cfset myResult = responseNodes[1].xmlText>
            </cfif>
        </cfif>
        
		<cfreturn myResult>
	</cffunction>
    
    <!---                                                      --->
    <!---                   GET PRODUCT COUNT                  --->
    <!---                                                      --->
    
    <cffunction name="GetProductCount" access="public" returntype="string">
    	<cfargument name="Company" type="string" required="yes">
		<cfargument name="CustomerId" type="string" required="yes">
		<cfargument name="SearchCriteria" type="string" required="no" default="">
      
        <cfquery name="qry_getProductCount" datasource="#APPLICATION.Datasource_AX#">
            SELECT COUNT([ITEMID]) AS ITEMCOUNT
            FROM [INVENTTABLE]
            WHERE [NAMEALIAS] NOT LIKE '%*%'
            <cfif Len(Arguments.SearchCriteria)>
            	AND 
                (
                	[NAMEALIAS] LIKE '%#Arguments.SearchCriteria#%'
                    OR
                    [ITEMID] LIKE '%#Arguments.SearchCriteria#%'
                )
            </cfif>
        </cfquery>
        
        <cfset myResult = SerializeJSON(qry_getProductCount,true)>
		<cfreturn myResult>
	</cffunction>
    
    <!---                                                      --->
    <!---                   GET PRODUCTS LIST                  --->
    <!---                                                      --->
    
    <cffunction name="GetProductList" access="public" returntype="string">
    	<cfargument name="Company" type="string" required="yes">
		<cfargument name="CustomerId" type="string" required="yes">
		<cfargument name="SearchCriteria" type="string" required="no" default="">
        <cfargument name="PageNumber" type="string" required="yes">
        <cfargument name="ItemsPerPage" type="string" required="yes">
      
        <cfquery name="qry_getProductList" datasource="#APPLICATION.Datasource_AX#">
            SELECT [ITEMID], [NAMEALIAS], [DAXINTEGRATIONKEY], [DATAAREAID]
            FROM [INVENTTABLE]
            WHERE [NAMEALIAS] NOT LIKE '%*%'
            <cfif Len(Arguments.SearchCriteria)>
            	AND 
                (
                	[NAMEALIAS] LIKE '%#Arguments.SearchCriteria#%'
                    OR
                    [ITEMID] LIKE '%#Arguments.SearchCriteria#%'
                )
            </cfif>
            ORDER BY [ITEMID]
            OFFSET (#(Arguments.PageNumber*Arguments.ItemsPerPage)-Arguments.ItemsPerPage#) ROWS
            FETCH NEXT #Arguments.ItemsPerPage# ROWS ONLY
        </cfquery>
        
        <cfset myResult = SerializeJSON(qry_getProductList,true)>
		<cfreturn myResult>
	</cffunction>
    
    <!---                                                      --->
    <!---                  GET PRODUCT DETAIL                  --->
    <!---                                                      --->
    
    <cffunction name="GetProductDetail" access="public" returntype="string">
		<cfargument name="CustomerId" type="string" required="yes">
		<cfargument name="ItemUID" type="string" required="yes">
        
        <!--- Add Unit Description, Unit Price, Unit Freight, Unit Type --->
        <cfquery name="qry_getProductDetail" datasource="#APPLICATION.Datasource_AX#">
            SELECT [ITEMID], [NAMEALIAS], [DAXINTEGRATIONKEY], [DATAAREAID]
            FROM [INVENTTABLE]
            WHERE [DAXINTEGRATIONKEY] = <cfqueryparam cfsqltype="cf_sql_char" value="#Arguments.ItemUID#">
        </cfquery>
        
		<cfset myResult = SerializeJSON(qry_getProductDetail,true)>
		<cfreturn myResult>
	</cffunction>
</cfcomponent>