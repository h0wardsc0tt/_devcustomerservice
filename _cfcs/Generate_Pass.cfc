<cfcomponent 
	displayname="Generate_UID" 
	output="yes" 
	hint="Generates a Alpha Numeric UID">
	
	<cffunction 
		name="genUID" 
		returntype="string" 
		access="public">
		<!--- default length of the password to be generated --->
		<cfargument name="Length" default="6" type="numeric">
		<!--- types of characters to use in the password (default Uppercase, Lowercase letters and 0-9 --->
		<cfargument name="CharSet" default="alphanumeric" type="string">
		<!--- name of the variable to store the generated UID (default Generated_UID) --->
		<cfargument name="ReturnVariable" default="Generated_UID" type="string">
		<!--- yes/no include only uppercase characters in the UID? (default yes)--->
		<cfargument name="UCaseOnly" default="yes" type="boolean">
		
		<cfif NOT isnumeric(ARGUMENTS.Length)>
			<cfthrow detail="The attribute <b>Length</b> must be a valid numeric value">
		</cfif>
		
		<!--- valid CharSets are handled as the defaultcase below --->
		
		<!--- ATTRIBUTES.Ucase is only checked for as "YES" if it is NOT "YES" uppercase letters are simply not included
		no errors will be thrown no matter what you specify as this attribute--->
		
		<cfset alphaLcase  = "a|c|e|g|i|k|m|q|s|u|w|y|b|d|f|h|j|n|p|r|t|v|x|z">
		<cfset alphaUcase = "A|C|E|G|I|K|M|Q|S|U|W|Y|B|D|F|H|J|N|P|R|T|V|X|Z">
		<cfset numeric      = "2|4|6|8|9|7|5|3">
		
		<!--- decide what cars to use in generating the password --->
		<cfswitch expression="#ARGUMENTS.CharSet#">
			<!--- Create only alpha passwords (NO NUMBERS ALLOWED) --->
			<cfcase value="alpha">
				<cfif ARGUMENTS.UCaseOnly IS "Yes">
					<cfset charlist = "">
					<cfset charList = listappend(charlist, alphaUcase, "|")>
				<cfelse>
					<cfset charlist = alphaLcase>
					<cfset charList = listappend(charlist, alphaUcase, "|")>
				</cfif>
			</cfcase>
			<!--- Create Alphanumeric passwords --->
			<cfcase value="alphanumeric">
				<cfif ARGUMENTS.UCaseOnly IS "Yes">
					<cfset charlist = "#numeric#">
					<cfset charList = listappend(charlist, alphaUcase, "|")>
				<cfelse>
					<cfset charlist = "#alphaLcase#|#numeric#">
					<cfset charList = listappend(charlist, alphaUcase, "|")>
				</cfif>
			</cfcase>
			<!--- Create only numeric passwords --->
			<cfcase value="numeric">
				<cfset charlist = numeric>
			</cfcase>
			<cfdefaultcase>
				<cfthrow detail="Valid values of the attribute <b>CharSet</b> are Alpha, AlphaNumeric, and Numeric">
			</cfdefaultcase>
		</cfswitch>
		
		<cfparam name="ThisPass" default="">
		
		<!--- each loop count gets one new character and adds it to the end of the password,
		so loop from 1 to "the number set in the calling template in Length --->
		<cfloop from="1" to="#Length#" index="i">
			<!--- pick a random number between 1 and the length of the list of chars to use --->
			<cfset ThisNum = RandRange(1,listlen(charlist, "|"))>
			<!--- get the character that is at the random numbers position in the list of characters --->
			<cfset ThisChar = ListGetAt(Charlist, ThisNum, "|")>
			<!--- append the new random character to the UID --->
			<cfset ThisPass = ListAppend(ThisPass, ThisChar, " ")>
		</cfloop>
		
		<cfset ThisPass = replace(ThisPass, " ", "", "ALL")>
		<cfset ARGUMENTS.ReturnVariable = ThisPass>
		<cfreturn ARGUMENTS.ReturnVariable>
	</cffunction>
</cfcomponent>