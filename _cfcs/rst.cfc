<cfcomponent rest="true" restpath="restService">

	<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="GetAuthorized" access="remote" returntype="String" httpmethod="GET">
    	<cfargument type="string" name="df_token" required="yes">
        <cfargument type="string" name="duid" required="yes">
        <cfargument type="xml" name="xmlData" required="yes">
        
        <cfscript>
			var variables.dsn = "db_admin";
			var qry_verifyToken = '';
			var ErrorFound = false;
			var AuthCode = "";
    	</cfscript>
        
        <cftry>
            <cfquery name="qry_verifyToken" datasource="#variables.dsn#">
                SELECT	datafeed_id 
                FROM	dbo.Datafeed WITH (NOLOCK) 
                WHERE	datafeed_token = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.df_token#">
            </cfquery>
        	<cfcatch type="any">
            	<cfset ErrorFound = true>
            </cfcatch>
        </cftry>
            
        <cftry>
            <cfif ErrorFound>
				<cfset AuthCode = "2">
            <cfelse>
                <cfif qry_verifyToken.RecordCount EQ 0>
                    <cfset AuthCode = "2">
                    <cfset ErrorFound = true>
                <cfelse>
                    <cfset AuthCode = "1">
                    
                    <cffile
                    	action="UPLOAD"
                    	filefield="xmlData"
                    	destination="#ExpandPath( '../data/' )#"
                    	nameconflict="MAKEUNIQUE"
                    />
                    
                </cfif>
            </cfif>
        	
            <cfquery datasource="#variables.dsn#">
                INSERT INTO dbo.Datafeed_Log(
                        datafeed_token,
                        datafeed_id,
                        duid,
                        datafeed_status_code,
                        datafeed_log_dts
                ) 
                VALUES
                (
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.df_token#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="1">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.duid#">,
                    <cfqueryparam cfsqltype="cf_sql_tinyint" value="#AuthCode#">,
                    <cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(Now())#">
                )
            </cfquery>
        	<cfcatch type="any">
            	<cfset AuthCode = "3">
                <cfset ErrorFound = true>
            </cfcatch>
        </cftry>

		<cfif NOT ErrorFound>
        	<!--- Invoke Data Import .cfc 
        	<cfscript>
            	carData = CreateObject("component", "_cfcs.rst2");
				carDataImport = carData.ImportData(Arguments.df_token);
            </cfscript>--->
        </cfif>
        
        <cfreturn AuthCode>
    </cffunction>
    
</cfcomponent>