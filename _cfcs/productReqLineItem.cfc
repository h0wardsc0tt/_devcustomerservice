<cfcomponent name="PurchaseReq" hint="Purchase Req functions">

  <cffunction name="getCustomerFilter" output="false" returntype="query" access="public" description="Set category to Active">
       	<cfargument type="string" name="lookup" dbvarname="@lookup" required="yes"> 
		<cftry>
			<cfstoredproc procedure="[dbo].[getCustomer]" datasource="#APPLICATION.Datasource#" >                            
                <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        value="#Arguments.lookup#">                                                        
            	<cfprocresult name="qry_getCustomerFilter">     
			</cfstoredproc>	
            <cfcatch type="Database"></cfcatch>
		</cftry>
		<cfreturn qry_getCustomerFilter />
    </cffunction>      


  <cffunction name="getshipping" output="false" returntype="query" access="public" description="Set category to Active">
       	<cfargument type="string" name="key" dbvarname="@key" required="yes"> 
		<cftry>
			<cfstoredproc procedure="[dbo].[getShipping]" datasource="#APPLICATION.Datasource#" >                            
                <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        value="#Arguments.key#">                                                        
            	<cfprocresult name="qry_getshipping">     
			</cfstoredproc>	
            <cfcatch type="Database"></cfcatch>
		</cftry>
		<cfreturn qry_getshipping />
    </cffunction>      

 <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: ---> 
  
	<cffunction name="updatePurchaseReq" output="false" returntype="string" access="public" description="Update Purchase Req">
        <cfargument type="any" name="id" dbvarname="@id" required="yes">
        <cfargument type="string" name="type" dbvarname="@type" required="no">
        <cfargument type="string" name="purchaseOrder" dbvarname="@purchaseOrder" required="no">
        <cfargument type="string" name="buyer" dbvarname="@buyer" required="no">
        <cfargument type="string" name="shipVia" dbvarname="@shipVia" required="no">
        <cfargument type="string" name="supplier" dbvarname="@supplier" required="no">
        <cfargument type="string" name="shipper" dbvarname="@shipper" required="no">
        <cfargument type="string" name="requestor" dbvarname="@requestor" required="no">
        <cfargument type="string" name="requestedDate" dbvarname="@requestedDate" required="no">
        <cfargument type="string" name="accountNumber" dbvarname="@accountNumber" required="no">
        <cfargument type="string" name="department" dbvarname="@department" required="no">
        <cfargument type="string" name="comments" dbvarname="@comments" required="no">
                         
        <cfset results = "OK">
		<cftry>
 			<cfstoredproc procedure="dbo.UpdatePurchaseRequisition" datasource="powsql" >         
				<cfif "#Arguments.id#" neq "">
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                     	type="In"
                   		value=#Arguments.id#>
                 <cfelse>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                      	type="In"
                        null="Yes">
                 </cfif>
                 
                <cfif isDefined("Arguments.purchaseOrder")>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                            type="In"
                        value="#Arguments.purchaseOrder#">
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.buyer")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.buyer#">
               <cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                
                <cfif isDefined("Arguments.shipVia")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        value="#Arguments.shipVia#">
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                                   
                <cfif isDefined("Arguments.supplier")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        value="#Arguments.supplier#">
               <cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                                    
                <cfif isDefined("Arguments.shipper")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        value="#Arguments.shipper#">
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                                   
                <cfif isDefined("Arguments.requestor")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        value="#Arguments.requestor#">
            	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                                   
                <cfif isDefined("Arguments.requestedDate")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        value="#Arguments.requestedDate#">
               <cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                                    
                <cfif isDefined("Arguments.accountNumber")>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        value="#Arguments.accountNumber#">
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
 				</cfif> 
                                                            
                <cfif isDefined("Arguments.department")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        value="#Arguments.department#">
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                
                <cfif isDefined("Arguments.comments")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        value="#Arguments.comments#">
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 

                <cfif isDefined("Arguments.type")>
                   <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.type#">
                <cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                
               <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    type="Out" 
                    value=""
                    variable="updateType"
                    >
               <cfprocparam
                    cfsqltype="cf_sql_integer"
                    type="Out" 
                    value=0
                    variable="identity"
                    >                  
             </cfstoredproc>
             	            
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#"> 
            </cfcatch>
        </cftry> 
         <cfscript>
		 	response = '{"identity":"' & identity & '", "type":"' & updateType & '", "results":"' & results & '"}';
		 </cfscript>
        <cfreturn response />
	</cffunction>    
        
</cfcomponent>
