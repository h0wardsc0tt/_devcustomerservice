<cfcomponent name="UserPermission" hint="User Access Permission for the Customer Portal">
	<cfscript>
		this.init();
	</cfscript>

	<cffunction name="init" access="public" output="false" returntype="any">
    	<cfargument type="String" name="dns" required="no" default="#APPLICATION.Datasource#">
        
		<cfscript>
			variables.dns = Arguments.dns;
			
			return this;
		</cfscript>
	</cffunction>
    
    <!---------------------------------------
		USER PERMISSION
	---------------------------------------->
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="getPermissionByUserID" output="false" returntype="query" access="public" description="Get user permission by user_id">
        <cfargument type="numeric" name="User_ID" required="yes">
    
    	<cftry>
            <cfquery name="local.qGetUserPermission" datasource="#variables.dns#">
            	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
				SET NOCOUNT ON;

                SELECT	ut.UserType_ID,
                        ut.UserType_Name,
                        ur.TypeRole_ID,
                        ur.TypeRole_Name,
                        po.PO_Code,
                        up.PurchaseOrder,
                        up.CreditCard,
                        up.DepositCard
                FROM	[Permission].[User_Permission] up
                		LEFT JOIN [Permission].[UserType_Role] ur ON up.TypeRole_ID = ur.TypeRole_ID
                		LEFT JOIN [Permission].[UserType] ut ON ut.UserType_ID = ur.UserType_ID
                        LEFT JOIN [Permission].[PriceOverride] po ON up.PO_ID = po.PO_ID
                WHERE	up.User_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.User_ID#">
            </cfquery>
        
            <cfcatch type="database"></cfcatch>
        </cftry>
        
        <cfreturn local.qGetUserPermission />
    </cffunction>
    
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="createUserPermission" output="false" returntype="any" access="public" description="Insert user permission">
        <cfargument type="numeric" name="User_ID" required="yes">
        <cfargument type="numeric" name="TypeRole_ID" required="yes">
        <cfargument type="numeric" name="PO_ID" required="yes">
        <cfargument type="boolean" name="PurchaseOrder" required="yes">
        <cfargument type="boolean" name="CreditCard" required="yes">
        <cfargument type="boolean" name="DepositCard" required="yes">
        <cfargument type="string" name="Created_By" required="yes">
    
    	<cftry>
            <cfquery datasource="#variables.dns#">
				SET NOCOUNT ON;

				INSERT INTO [Permission].[User_Permission](
                        User_ID,
                        TypeRole_ID,
                        PO_ID,
                        PurchaseOrder,
                        CreditCard,
                        Created_By,
                        Created_DTS,
                        LastUpdated_DTS,
                        DepositCard
                )
                SELECT	<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.User_ID#">,
                        <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.TypeRole_ID#">,
                        <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.PO_ID#">,
                        <cfqueryparam cfsqltype="cf_sql_bit" value="#Arguments.PurchaseOrder#">,
                        <cfqueryparam cfsqltype="cf_sql_bit" value="#Arguments.CreditCard#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.Created_By#">,
                        GetDate(),
                        GetDate(),
                        <cfqueryparam cfsqltype="cf_sql_bit" value="#Arguments.DepositCard#">
            </cfquery>
        
            <cfcatch type="database"></cfcatch>
        </cftry>
    </cffunction>
    
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="updateUserPermission" output="false" returntype="any" access="public" description="Update user permission">
        <cfargument type="numeric" name="User_ID" required="yes">
        <cfargument type="numeric" name="TypeRole_ID" required="yes">
        <cfargument type="numeric" name="PO_ID" required="yes">
        <cfargument type="boolean" name="PurchaseOrder" required="yes">
        <cfargument type="boolean" name="CreditCard" required="yes">
        <cfargument type="boolean" name="DepositCard" required="yes">
        <cfargument type="string" name="Created_By" required="yes">
    
    	<cftry>
            <cfquery datasource="#variables.dns#">
				SET NOCOUNT ON;

				UPDATE	[Permission].[User_Permission]
                SET     TypeRole_ID = <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.TypeRole_ID#">,
                        PO_ID = <cfqueryparam cfsqltype="cf_sql_tinyint" value="#Arguments.PO_ID#">,
                        PurchaseOrder = <cfqueryparam cfsqltype="cf_sql_bit" value="#Arguments.PurchaseOrder#">,
                        CreditCard = <cfqueryparam cfsqltype="cf_sql_bit" value="#Arguments.CreditCard#">,
                        Created_By = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.Created_By#">,
                        LastUpdated_DTS = GetDate(),
                        DepositCard = <cfqueryparam cfsqltype="cf_sql_bit" value="#Arguments.DepositCard#">
                WHERE	User_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.User_ID#">
            </cfquery>
        
            <cfcatch type="database"></cfcatch>
        </cftry>
    </cffunction>
    
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="deleteUserPermission" output="false" returntype="any" access="public" description="Delete user permission">
        <cfargument type="numeric" name="User_ID" required="yes">
    
    	<cftry>
            <cfquery datasource="#variables.dns#">
				SET NOCOUNT ON;

				DELETE FROM	[Permission].[User_Permission]
                WHERE  User_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.User_ID#">
            </cfquery>
        
            <cfcatch type="database"></cfcatch>
        </cftry>
    </cffunction>
    
    
    <!---------------------------------------
		USER APPLICATION
	---------------------------------------->
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="getApplicationByType" output="false" returntype="query" access="public" description="Get application by userType">
        <cfargument type="string" name="UserType" required="yes">
    
    	<cftry>
            <cfquery name="local.qGetApplicationByType" datasource="#variables.dns#">
            	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
				SET NOCOUNT ON;

                SELECT	a.App_ID,
                        a.App_FullName
                FROM	[Permission].[Application] a
                		INNER JOIN [Permission].[UserType_Application] ua ON ua.App_ID = a.App_ID
                		INNER JOIN [Permission].[UserType] ut ON ut.UserType_ID = ua.UserType_ID
                WHERE	ut.UserType_Name = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.UserType#">
                AND		a.IsActive = 1
            </cfquery>
        
            <cfcatch type="database"></cfcatch>
        </cftry>
        
        <cfreturn local.qGetApplicationByType />
    </cffunction>
    
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="getApplicationByUserID" output="false" returntype="query" access="public" description="Get application by user">
    	<cfargument type="numeric" name="User_ID" required="yes">
    
    	<cftry>
            <cfquery name="local.qGetUserApplication" datasource="#variables.dns#">
            	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
				SET NOCOUNT ON;

                SELECT	a.App_ID,
						a.App_FullName
                FROM	[Permission].[Application] a
                		INNER JOIN [Permission].[User_Application] ua ON ua.App_ID = a.App_ID
                WHERE	ua.User_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.User_ID#">
            </cfquery>
        
            <cfcatch type="database"></cfcatch>
        </cftry>
        
        <cfreturn local.qGetUserApplication />
    </cffunction>
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="getApplicationIDByName" output="false" returntype="query" access="public" description="Get application by name">
    	<cfargument type="string" name="App_Names" required="yes">
    
    	<cftry>
            <cfquery name="local.getApplicationIDByName" datasource="#variables.dns#">
            	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
				SET NOCOUNT ON;

                SELECT	App_ID
                FROM	Permission.Application
                WHERE	App_FullName IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.App_Names#" list="yes">)
            </cfquery>
        
            <cfcatch type="database"></cfcatch>
        </cftry>
        
        <cfreturn local.getApplicationIDByName />
    </cffunction>
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="createApplicationUser" output="false" returntype="any" access="public" description="Insert user applications">
    	<cfargument type="numeric" name="User_ID" required="yes">
        <cfargument type="string" name="AppIDs" required="yes">
        
        <cftry>
            <cfquery datasource="#variables.dns#">
				SET NOCOUNT ON;

				INSERT INTO [Permission].[User_Application](User_ID, App_ID)
                SELECT	<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.User_ID#">, a.cValue
				FROM	dbo.split('#Arguments.AppIDs#', ',') a
            </cfquery>
        
            <cfcatch type="database"></cfcatch>
        </cftry>
    </cffunction>
    
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="deleteApplicationUser" output="false" returntype="any" access="public" description="Delete applications by user">
    	<cfargument type="numeric" name="User_ID" required="yes">
    
    	<cftry>
            <cfquery datasource="#variables.dns#">
				SET NOCOUNT ON;

				DELETE FROM	[Permission].[User_Application]
                WHERE  User_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.User_ID#">
            </cfquery>
        
            <cfcatch type="database"></cfcatch>
        </cftry>
    </cffunction>
    
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="updateApplicationUser" output="false" returntype="any" access="public" description="Update user_application">
    	<cfargument type="numeric" name="User_ID" required="yes">
        <cfargument type="string" name="AppIDs" required="yes">
        
        <cfscript>
			transaction {
				deleteApplicationUser(Arguments.User_ID);
				createApplicationUser(Arguments.User_ID, Arguments.AppIDs);
			}
        </cfscript>
    </cffunction>
    
    
    <!---------------------------------------
		USER COMPANY
	---------------------------------------->
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="getAllCompanies" output="false" returntype="query" access="public" description="Get all companies">
    	<cftry>
            <cfquery name="local.qGetAllCompanies" datasource="#variables.dns#">
            	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
				SET NOCOUNT ON;

                SELECT	Company_ID,
                        Company_Name,
                        Company_Code
                FROM	[Permission].[Company]
            </cfquery>
        
            <cfcatch type="database"></cfcatch>
        </cftry>
        
        <cfreturn local.qGetAllCompanies />
    </cffunction>
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="getCompanyIDByName" output="false" returntype="query" access="public" description="Get companies by name">
    	<cfargument type="string" name="Company_Code" required="yes">
        
    	<cftry>
            <cfquery name="local.getCompanyIDByName" datasource="#variables.dns#">
            	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
				SET NOCOUNT ON;

                SELECT	Company_ID
                FROM	[Permission].[Company]
                WHERE   Company_Code IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.Company_Code#" list="yes">)
            </cfquery>
        
            <cfcatch type="database"></cfcatch>
        </cftry>
        
        <cfreturn local.getCompanyIDByName />
    </cffunction>
    
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="getCompanyByUserID" output="false" returntype="query" access="public" description="Get company by user">
    	<cfargument type="numeric" name="User_ID" required="yes">
    
    	<cftry>
            <cfquery name="local.qGetUserCompany" datasource="#variables.dns#">
            	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
				SET NOCOUNT ON;

                SELECT	c.Company_ID,
						c.Company_Name
                FROM	[Permission].[Company] c
                		INNER JOIN [Permission].[User_Company] uc ON uc.Company_ID = c.Company_ID
                WHERE	uc.User_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.User_ID#">
            </cfquery>
        
            <cfcatch type="database"></cfcatch>
        </cftry>
        
        <cfreturn local.qGetUserCompany />
    </cffunction>
    
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="createCompanyUser" output="false" returntype="any" access="public" description="Insert user companies">
    	<cfargument type="numeric" name="User_ID" required="yes">
        <cfargument type="string" name="CompanyIDs" required="yes">
        
        <cftry>
            <cfquery datasource="#variables.dns#">
				SET NOCOUNT ON;

				INSERT INTO [Permission].[User_Company](User_ID, Company_ID)
                SELECT	<cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.User_ID#">, cValue
				FROM	dbo.split('#Arguments.CompanyIDs#', ',')
            </cfquery>
        
            <cfcatch type="database"></cfcatch>
        </cftry>
    </cffunction>
    
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="deleteCompanyUser" output="false" returntype="any" access="public" description="Delete companies by user">
    	<cfargument type="numeric" name="User_ID" required="yes">
    
    	<cftry>
            <cfquery datasource="#variables.dns#">
				SET NOCOUNT ON;

				DELETE FROM	[Permission].[User_Company]
                WHERE  User_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.User_ID#">
            </cfquery>
        
            <cfcatch type="database"></cfcatch>
        </cftry>
    </cffunction>
    
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="updateCompanyUser" output="false" returntype="any" access="public" description="Update user_company">
    	<cfargument type="numeric" name="User_ID" required="yes">
        <cfargument type="string" name="CompanyIDs" required="yes">
        
        <cfscript>
			transaction {
				deleteCompanyUser(Arguments.User_ID);
				createCompanyUser(Arguments.User_ID, Arguments.CompanyIDs);
			}
        </cfscript>
    </cffunction>
    

	<!---------------------------------------
		OTHER
	---------------------------------------->
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="getAllUserType" output="false" returntype="query" access="public" description="Get user types">
    	<cftry>
            <cfquery name="local.qGetUserTypes" datasource="#variables.dns#">
            	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
				SET NOCOUNT ON;

                SELECT	UserType_ID,
                        UserType_Name
                FROM	[Permission].[UserType]
            </cfquery>
        
            <cfcatch type="database"></cfcatch>
        </cftry>
        
        <cfreturn local.qGetUserTypes />
    </cffunction>
    
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="getUserRoleByType" output="false" returntype="query" access="public" description="Get user role by userType">
        <cfargument type="string" name="UserType" required="yes">
    
    	<cftry>
            <cfquery name="local.qGetUserRoleByType" datasource="#variables.dns#">
            	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
				SET NOCOUNT ON;

                SELECT	ur.TypeRole_ID,
                        ur.TypeRole_Name
                FROM	[Permission].[UserType_Role] ur
                		INNER JOIN [Permission].[UserType] ut ON ut.UserType_ID = ur.UserType_ID
                WHERE	ut.UserType_Name = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.UserType#">
            </cfquery>
        
            <cfcatch type="database"></cfcatch>
        </cftry>
        
        <cfreturn local.qGetUserRoleByType />
    </cffunction>
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="getPriceOverrideByType" output="false" returntype="query" access="public" description="Get price override by userType">
        <cfargument type="string" name="UserType" required="yes">
    
    	<cftry>
            <cfquery name="local.qGetPriceOverrideByType" datasource="#variables.dns#">
            	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
				SET NOCOUNT ON;

                SELECT	po.PO_ID,
                        po.PO_Code
                FROM	[Permission].[PriceOverride] po
                		INNER JOIN [Permission].[UserType] ut ON ut.UserType_ID = po.UserType_ID
                WHERE	ut.UserType_Name = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.UserType#">
            </cfquery>
        
            <cfcatch type="database"></cfcatch>
        </cftry>
        
        <cfreturn local.qGetPriceOverrideByType />
    </cffunction>
</cfcomponent>