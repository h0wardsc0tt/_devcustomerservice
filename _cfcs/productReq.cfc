<cfcomponent name="PurchaseReq" hint="Purchase Req functions">

  <cffunction name="getCustomerFilter" output="false" returntype="query" access="public" description="Set category to Active">
       	<cfargument type="string" name="lookup" dbvarname="@lookup" required="yes"> 
		<cftry>
			<cfstoredproc procedure="[dbo].[getCustomer]" datasource="#APPLICATION.Datasource#" >                            
                <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        value="#Arguments.lookup#">                                                        
            	<cfprocresult name="qry_getCustomerFilter">     
			</cfstoredproc>	
            <cfcatch type="Database"></cfcatch>
		</cftry>
		<cfreturn qry_getCustomerFilter />
    </cffunction>      


  <cffunction name="getshipping" output="false" returntype="query" access="public" description="Set category to Active">
       	<cfargument type="string" name="key" dbvarname="@key" required="yes"> 
		<cftry>
			<cfstoredproc procedure="[dbo].[getShipping]" datasource="#APPLICATION.Datasource#" >                            
                <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        value="#Arguments.key#">                                                        
            	<cfprocresult name="qry_getshipping">     
			</cfstoredproc>	
            <cfcatch type="Database"></cfcatch>
		</cftry>
		<cfreturn qry_getshipping />
    </cffunction>      

 <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: ---> 
  
	<cffunction name="updatePurchaseReq" output="false" returntype="string" access="public" description="Update Purchase Req">
        <cfargument type="any" name="id" dbvarname="@id" required="yes">
        <cfargument type="string" name="type" dbvarname="@type" required="no">
        <cfargument type="string" name="purchaseOrder" dbvarname="@purchaseOrder" required="no">
        <cfargument type="string" name="buyer" dbvarname="@buyer" required="no">
        <cfargument type="string" name="shipVia" dbvarname="@shipVia" required="no">
        <cfargument type="string" name="supplier" dbvarname="@supplier" required="no">
        <cfargument type="string" name="shipper" dbvarname="@shipper" required="no">
        <cfargument type="string" name="requestor" dbvarname="@requestor" required="no">
        <cfargument type="string" name="requestedDate" dbvarname="@requestedDate" required="no">
        <cfargument type="string" name="accountNumber" dbvarname="@accountNumber" required="no">
        <cfargument type="string" name="department" dbvarname="@department" required="no">
        <cfargument type="string" name="comments" dbvarname="@comments" required="no">
                         
        <cfset results = "OK">
		<cftry>
 			<cfstoredproc procedure="dbo.UpdatePurchaseRequisition" datasource="powsql" >         
				<cfif "#Arguments.id#" neq "">
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                     	type="In"
                   		value=#Arguments.id#>
                 <cfelse>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                      	type="In"
                        null="Yes">
                 </cfif>
                 
                <cfif isDefined("Arguments.purchaseOrder")>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="#NOT len(trim(Arguments.purchaseOrder))#"
                        value="#Arguments.purchaseOrder#">
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.buyer")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        null="#NOT len(trim(Arguments.buyer))#"
                        value="#Arguments.buyer#">
               <cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                
                <cfif isDefined("Arguments.shipVia")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="#NOT len(trim(Arguments.shipVia))#"
                        value="#Arguments.shipVia#">
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                                   
                <cfif isDefined("Arguments.supplier")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="#NOT len(trim(Arguments.supplier))#"
                        value="#Arguments.supplier#">
               <cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                                    
                <cfif isDefined("Arguments.shipper")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="#NOT len(trim(Arguments.shipper))#"
                        value="#Arguments.shipper#">
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                                   
                <cfif isDefined("Arguments.requestor")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="#NOT len(trim(Arguments.requestor))#"
                        value="#Arguments.requestor#">
            	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                                   
                <cfif isDefined("Arguments.requestedDate")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="#NOT len(trim(Arguments.requestedDate))#"
                        value="#Arguments.requestedDate#">
               <cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                                    
                <cfif isDefined("Arguments.accountNumber")>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="#NOT len(trim(Arguments.accountNumber))#"
                        value="#Arguments.accountNumber#">
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
 				</cfif> 
                                                            
                <cfif isDefined("Arguments.department")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="#NOT len(trim(Arguments.department))#"
                        value="#Arguments.department#">
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                
                <cfif isDefined("Arguments.comments")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="#NOT len(trim(Arguments.comments))#"
                        value="#Arguments.comments#">
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 

                <cfif isDefined("Arguments.type")>
                   <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.type#">
                <cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                
               <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    type="Out" 
                    value=""
                    variable="updateType"
                    >
               <cfprocparam
                    cfsqltype="cf_sql_integer"
                    type="Out" 
                    value=0
                    variable="identity"
                    >                  
             </cfstoredproc>
             	            
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#"> 
            </cfcatch>
        </cftry> 
         <cfscript>
		 	lineitem = '{"lineItemId":"","identity":"", "type":"", "results":""}';
		 	response = '{"identity":"' & identity & '", "type":"' & updateType & '", "results":"' & results & '", "lineItems":[]}';
		 </cfscript>
        <cfreturn response />
	</cffunction> 
       
 <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: ---> 
 
	<cffunction name="updatePurchaseReqLineItem" output="false" returntype="string" access="public" description="Update Purchase Req Line Item">
        <cfargument type="any" name="id" dbvarname="@id" required="yes">
        <cfargument type="any" name="key" dbvarname="@key" required="no">
        <cfargument type="string" name="type" dbvarname="@type" required="no">
        <cfargument type="string" name="lineItem" dbvarname="@lineItem" required="no">
        <cfargument type="string" name="quantity" dbvarname="@quantityRequested" required="no">
        <cfargument type="string" name="umo" dbvarname="@sumo" required="no">
        <cfargument type="string" name="estcost" dbvarname="@estimatedCost" required="no">
        <cfargument type="string" name="needdate" dbvarname="@requestedDate" required="no">
        <cfargument type="string" name="manufacturedesc" dbvarname="@manufacturePinDescription" required="no">
        <cfargument type="string" name="purchasequantity" dbvarname="@purchasedQuantity" required="no">
        <cfargument type="string" name="price" dbvarname="@purchasedCost" required="no">
        <cfargument type="string" name="dockDate" dbvarname="@dockDate" required="no">
        <cfargument type="string" name="lineItemId" required="yes">
                                 
        <cfset results = "OK">
		<cftry>
 			<cfstoredproc procedure="dbo.UpdatePurchaseRequisitionLineItem" datasource="powsql" >         
				<cfif "#Arguments.id#" neq "">
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                     	type="In"
                   		value=#Arguments.id#>
                 <cfelse>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                      	type="In"
                        null="Yes">
                 </cfif>
  
                 <cfif isDefined("Arguments.key")>
                	<cfprocparam
                        cfsqltype="cf_sql_integer"
                        type="In"
                        value="#Arguments.key#">
               	<cfelse>
                	<cfprocparam
                        cfsqltype="cf_sql_integer"
                   		type="In"
                        null="Yes">
				</cfif> 
               
                <cfif isDefined("Arguments.lineItem")>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="#NOT len(trim(Arguments.lineItem))#"
                        value="#Arguments.lineItem#">
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.quantity")>
                    <cfprocparam
                        cfsqltype="cf_sql_integer"
                    	type="In"
                        value="#Arguments.quantity#"
                        null="#NOT len(trim(Arguments.quantity))#"
                        >
               <cfelse>
                	<cfprocparam
                        cfsqltype="cf_sql_integer"
                   		type="In"
                        value="88"
                        null="Yes">
				</cfif> 
                
                <cfif isDefined("Arguments.umo")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        value="#Arguments.umo#"
                        null="#NOT len(trim(Arguments.umo))#"
						>
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                                   
                <cfif isDefined("Arguments.estcost")>
                    <cfprocparam
                        cfsqltype="cf_sql_money"
                   		type="In"
                        value="#Arguments.estcost#"
                        null="#NOT len(trim(Arguments.estcost))#"
						>
               <cfelse>
                	<cfprocparam
                        cfsqltype="cf_sql_money"
                   		type="In"
                        null="Yes">
				</cfif> 
                                    
                <cfif isDefined("Arguments.needdate")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        value="#Arguments.needdate#"
                        null="#NOT len(trim(Arguments.needdate))#"
						>
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                                   
                <cfif isDefined("Arguments.manufacturedesc")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        value="#Arguments.manufacturedesc#"
                        null="#NOT len(trim(Arguments.manufacturedesc))#"                        
                        >
            	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                                   
                <cfif isDefined("Arguments.purchasequantity")>
                    <cfprocparam
                        cfsqltype="cf_sql_integer"
                   		type="In"
                        value="#Arguments.purchasequantity#"
                        null="#NOT len(trim(Arguments.purchasequantity))#"
						>
               <cfelse>
                	<cfprocparam
                        cfsqltype="cf_sql_integer"
                   		type="In"
                        null="Yes">
				</cfif> 
                                    
                <cfif isDefined("Arguments.price")>
                	<cfprocparam
                        cfsqltype="cf_sql_money"
                   		type="In"
                        value="#Arguments.price#"
                        null="#NOT len(trim(Arguments.price))#"
						>
               	<cfelse>
                	<cfprocparam
                        cfsqltype="cf_sql_money"
                   		type="In"
                        null="Yes">
 				</cfif>

                                   
                <cfif isDefined("Arguments.dockDate")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        value="#Arguments.dockDate#"
                        null="#NOT len(trim(Arguments.dockDate))#"                        
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 

                <cfif isDefined("Arguments.type")>
                   <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.type#">
                <cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                
               <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    type="Out" 
                    value=""
                    variable="updateType"
                    >
               <cfprocparam
                    cfsqltype="cf_sql_integer"
                    type="Out" 
                    value=0
                    variable="identity"
                    >                  
             </cfstoredproc>
             	            
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#"> 
            </cfcatch>
        </cftry> 
         <cfscript>
		 	response = '{"lineItemId":"' & lineItemId & '","identity":"' & identity & '", "type":"' & updateType & '", "results":"' & results & '"}';
		 </cfscript>
        <cfreturn response />
	</cffunction>   
    
<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: ---> 
 
	<cffunction name="PurchaseRequisitionSelect" output="false" returntype="any" access="public" description="Select PR or Search for PR's">
        <cfargument type="any" name="id" dbvarname="@id" required="no">
        <cfargument type="string" name="type" dbvarname="@type" required="no">
        <cfargument type="any" name="pageNumber" dbvarname="@pageNumber" required="no">
        <cfargument type="any" name="itemsPerPage" dbvarname="@itemsPerPage" required="no">
        <cfargument type="string" name="purchaseOrder" dbvarname="@purchaseOrder" required="no">
        <cfargument type="string" name="supplier" dbvarname="@supplier" required="no">
                                 
        <cfset results = "OK">
		<cftry>
 			<cfstoredproc procedure="dbo.[PurchaseRequisitionSelect]" datasource="powsql" >         
				<cfif "#Arguments.id#" neq "">
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                     	type="In"
                   		value=#Arguments.id#>
                 <cfelse>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                      	type="In"
                        null="Yes">
                 </cfif>

                <cfif isDefined("Arguments.type")>
                   <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="#NOT len(trim(Arguments.type))#"
                        value="#Arguments.type#">
                <cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 


				<cfif "#Arguments.pageNumber#" neq "">
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                     	type="In"
                   		value=#Arguments.pageNumber#>
                 <cfelse>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                      	type="In"
                        null="Yes">
                 </cfif>

				<cfif "#Arguments.itemsPerPage#" neq "">
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                     	type="In"
                   		value=#Arguments.itemsPerPage#>
                 <cfelse>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                      	type="In"
                        null="Yes">
                 </cfif>


               
                <cfif isDefined("Arguments.purchaseOrder")>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="#NOT len(trim(Arguments.purchaseOrder))#"
                        value="#Arguments.purchaseOrder#">
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.supplier")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.supplier#"
                        null="#NOT len(trim(Arguments.supplier))#"
                        >
               <cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 

            	<cfprocresult name="qry_getSelect">     
             </cfstoredproc>
             	            
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#"> 
            </cfcatch>
        </cftry> 
		<cfif "#Arguments.id#" neq "">
        	<cfreturn qry_getSelect />
        <cfelse>
        	<cfreturn SerializeJSON(qry_getSelect, 'true') />
        </cfif>
	</cffunction>     
    
         
</cfcomponent>
