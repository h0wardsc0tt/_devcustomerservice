<cfcomponent>

	<!--- Get Cart Items --->
    <cffunction name="GetCartItems" access="public" returntype="string">
        
        <cftry>
        	<cfquery name="qry_GetCartItems" datasource="#APPLICATION.Datasource#">
            	SELECT [Cart_ID],[Cart_Product_UID],[Cart_Product_QTY],[Cart_Product_Price_SubTotal],[Cart_Product_Additional_Freight],[Cart_Product_Price_Type],[Cart_Product_Company]
                FROM [ServicePortal].[stbl_User_Cart_Products]
                WHERE [Cart_Session_UID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.Session_UID#">
                AND [Cart_User_UID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.User_UID#">
            </cfquery>
            
        	<cfcatch type="any">
            	<cfmail from="no-reply@hme.com" to="chrisg@hme.com" subject="Customer Service Portal: Database Error" type="html">
                	#cfcatch.Message#<br />
                    #cfcatch.Detail#
                </cfmail>
            </cfcatch>
        </cftry>
        
		<cfset CartItems = SerializeJSON(qry_GetCartItems,true)>
		<cfreturn CartItems>
	</cffunction>

	<!--- Add Cart Item --->
	<cffunction name="AddCartItem" access="public" returntype="string">
		<cfargument name="CustomerId" type="string" required="yes">
		<cfargument name="ItemUID" type="string" required="yes">
        <cfargument name="ItemQty" type="string" required="yes">
        
        <cftry>
        	<cfscript>
				ProductObj = CreateObject("component", "_cfcs.SalesOrders");
				ProductDetail = DeserializeJSON(ProductObj.GetProductDetail(Arguments.CustomerId,Arguments.ItemUID));
				ProductPrice = "6.00";
				ProductSubTotal = ProductPrice*Arguments.ItemQty;
				ProductFreight = "0.00";
				ProductType = "Retail";
				ProductCompany = "HSC";
			</cfscript>
            
            <cfquery name="qry_CheckCartItem" datasource="#APPLICATION.Datasource#" maxrows="1">
            	SELECT [Cart_ID], [Cart_Product_QTY]
                FROM [ServicePortal].[stbl_User_Cart_Products]
                WHERE [Cart_Session_UID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.Session_UID#">
                AND [Cart_User_UID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.User_UID#">
                AND [Cart_Product_UID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#ItemUID#">
            </cfquery>
            
            <cfif ArrayLen(ProductDetail.DATA.ITEMID) NEQ 0>
            	<cfif qry_CheckCartItem.RecordCount EQ 0>
                    <cfquery name="qry_AddCartItem" datasource="#APPLICATION.Datasource#">
                        INSERT INTO [ServicePortal].[stbl_User_Cart_Products]
                        (
                            [Cart_IsCompleted],
                            [Cart_Session_UID],
                            [Cart_User_UID],
                            [Cart_Product_UID],
                            [Cart_Product_QTY],
                            [Cart_Product_Price_SubTotal],
                            [Cart_Product_Additional_Freight],
                            [Cart_Product_Price_Type],
                            [Cart_Product_Company],
                            [Cart_DTS]
                        )
                        VALUES
                        (
                            <cfqueryparam cfsqltype="cf_sql_tinyint" value="0">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.Session_UID#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.User_UID#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.ItemUID#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.ItemQty#">,
                            <cfqueryparam cfsqltype="cf_sql_money" value="#ProductSubTotal#">,
                            <cfqueryparam cfsqltype="cf_sql_money" value="#ProductFreight#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#ProductType#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#ProductCompany#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#DateFormat(Now(),'yyyy-mm-dd')# #TimeFormat(Now(),'HH:mm:ss')#">
                        )
                    </cfquery>
                <cfelse>
                	<cfquery name="qry_UpdateCartItem" datasource="#APPLICATION.Datasource#">
                        UPDATE [ServicePortal].[stbl_User_Cart_Products]
                        SET [Cart_Product_QTY] = <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.ItemQty+qry_CheckCartItem.Cart_Product_QTY#">,
                        	[Cart_Product_Price_SubTotal] = <cfqueryparam cfsqltype="cf_sql_money" value="#(Arguments.ItemQty+qry_CheckCartItem.Cart_Product_QTY)*ProductPrice#">
                        WHERE [Cart_Product_UID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.ItemUID#">
                        AND [Cart_Session_UID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.Session_UID#">
                        AND [Cart_User_UID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.User_UID#">
                    </cfquery>
                </cfif>
            </cfif>
            
        	<cfcatch type="any">
            	<cfmail from="no-reply@hme.com" to="chrisg@hme.com" subject="Customer Service Portal: Database Error" type="html">
                	#cfcatch.Message#<br />
                    #cfcatch.Detail#
                </cfmail>
            </cfcatch>
        </cftry>
        
		<cfset AddStatus="200">
		<cfreturn AddStatus>
	</cffunction>
    
    <!--- Remove Cart Item --->
    <cffunction name="RemoveCartItem" access="public" returntype="string">
		<cfargument name="ItemUID" type="string" required="yes">
        
        <cftry>
        	<cfset ItemList = ReplaceList(Arguments.ItemUID, '[,],"', '')>
        
        	<cfquery name="qry_RemoveCartItem" datasource="#APPLICATION.Datasource#">
            	DELETE FROM [ServicePortal].[stbl_User_Cart_Products]
                WHERE [Cart_Product_UID] IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#ItemList#" list="yes">)
                AND [Cart_Session_UID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.Session_UID#">
                AND [Cart_User_UID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.User_UID#">
            </cfquery>
            
        	<cfcatch type="any">
            	<cfmail from="no-reply@hme.com" to="chrisg@hme.com" subject="Customer Service Portal: Database Error" type="html">
                	#cfcatch.Message#<br />
                    #cfcatch.Detail#
                </cfmail>
            </cfcatch>
        </cftry>
        
		<cfset RemoveStatus="200">
		<cfreturn RemoveStatus>
	</cffunction>
    
    <!--- Update Cart Item--->
    <cffunction name="UpdateCartItem" access="public" returntype="string">
		<cfargument name="ItemUID" type="string" required="yes">
        <cfargument name="ItemQty" type="string" required="yes">
        
        <cftry>
        	<cfset ItemPrice = 0>
        
        	<!--- Query AX to get Unit Price --->
        	<!---<cfquery name="qry_GetCartItemPrice" datasource="#APPLICATION.Datasource#" maxrows="1">
            	SELECT [Cart_Product_Price_SubTotal],[Cart_Product_Additional_Freight]
                FROM [ServicePortal].[stbl_User_Cart_Products]
                WHERE [Cart_Session_UID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.Session_UID#">
                AND [Cart_User_UID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.User_UID#">
            </cfquery>
            
            <cfset ItemPrice = qry_GetCartItemPrice.ItemPrice>--->
            
            <cfset ItemPrice = "6.00">
            <cfset CartItemPrice = ItemPrice*Arguments.ItemQty>
        
        	<cfquery name="qry_UpdateCartItem" datasource="#APPLICATION.Datasource#">
            	UPDATE [ServicePortal].[stbl_User_Cart_Products]
                SET [Cart_Product_QTY] = <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.ItemQty#">,
                    [Cart_Product_Price_SubTotal] = <cfqueryparam cfsqltype="cf_sql_money" value="#CartItemPrice#">
                WHERE [Cart_Product_UID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.ItemUID#">
                AND [Cart_Session_UID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.Session_UID#">
                AND [Cart_User_UID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.User_UID#">
            </cfquery>

        	<cfcatch type="any">
            	<cfmail from="no-reply@hme.com" to="chrisg@hme.com" subject="Customer Service Portal: Database Error" type="html">
                	#cfcatch.Message#<br />
                    #cfcatch.Detail#
                </cfmail>
            </cfcatch>
        </cftry>

		<cfset CartItemPrice = DollarFormat(CartItemPrice)>
		<cfreturn CartItemPrice>
	</cffunction>
    
    <!--- Get Cart Subtotal --->
    <cffunction name="GetCartSubtotal" access="public" returntype="string">
        
        <cftry>
        	<cfset Subtotal = 0>
        
        	<cfquery name="qry_GetCartSubtotal" datasource="#APPLICATION.Datasource#">
            	SELECT [Cart_Product_Price_SubTotal],[Cart_Product_Additional_Freight]
                FROM [ServicePortal].[stbl_User_Cart_Products]
                WHERE [Cart_Session_UID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.Session_UID#">
                AND [Cart_User_UID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.User_UID#">
            </cfquery>
            
            <cfoutput query="qry_GetCartSubtotal">
            	<cfset Subtotal = Subtotal + Cart_Product_Price_SubTotal>
            </cfoutput>
            
        	<cfcatch type="any">
            	<cfmail from="no-reply@hme.com" to="chrisg@hme.com" subject="Customer Service Portal: Database Error" type="html">
                	#cfcatch.Message#<br />
                    #cfcatch.Detail#
                </cfmail>
            </cfcatch>
        </cftry>
        
		<cfset CartSubtotal = DollarFormat(Subtotal)>
		<cfreturn CartSubtotal>
	</cffunction>

	<!--- Get Cart Count --->
    <cffunction name="GetCartCount" access="public" returntype="string">
        
        <cftry>
        	<cfset CartCount = 0>
        
        	<cfquery name="qry_GetCartCount" datasource="#APPLICATION.Datasource#">
            	SELECT [Cart_ID]
                FROM [ServicePortal].[stbl_User_Cart_Products]
                WHERE [Cart_Session_UID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.Session_UID#">
                AND [Cart_User_UID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.User_UID#">
            </cfquery>
            
        	<cfcatch type="any">
            	<cfmail from="no-reply@hme.com" to="chrisg@hme.com" subject="Customer Service Portal: Database Error" type="html">
                	#cfcatch.Message#<br />
                    #cfcatch.Detail#
                </cfmail>
            </cfcatch>
        </cftry>
        
		<cfset CartCount = qry_GetCartCount.RecordCount>
		<cfreturn CartCount>
	</cffunction>
</cfcomponent>