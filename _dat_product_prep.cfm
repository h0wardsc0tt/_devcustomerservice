<cfparam name="FORM.Product_Form" default="">
<cfparam name="FORM.Product_Per_Page" default="12">
<cfparam name="FORM.Product_Current_Page" default="1">
<cfparam name="VARIABLES.Page_Start" default="1">
<cfparam name="VARIABLES.Page_End" default="12">
<cfparam name="URL.id" default="">

<cfscript>
	SalesOrderObj = CreateObject("component", "_cfcs.SalesOrders");
	ProductCount = DeserializeJSON(SalesOrderObj.GetProductCount("HSC","112253",FORM.Product_Search));
	ProductList = DeserializeJSON(SalesOrderObj.GetProductList("HSC","112253",FORM.Product_Search,FORM.Product_Current_Page,FORM.Product_Per_Page));
	
	if(LEN(URL.id) EQ 36){
		ProductDetails = DeserializeJSON(SalesOrderObj.GetProductDetail("112253",URL.id));
	}
	
	VARIABLES.Page_Start = ((FORM.Product_Current_Page-1)*FORM.Product_Per_Page)+1;
	VARIABLES.Page_End = FORM.Product_Current_Page*FORM.Product_Per_Page;
</cfscript>

<cfif Len(URL.id) EQ 36>
	<cfif ArrayLen(ProductDetails.DATA.ITEMID) EQ 0>
        <cflocation addtoken="no" url="./?pg=Products">
    </cfif>
</cfif>

<cfif CGI.QUERY_STRING CONTAINS "Details" AND Len(URL.id) NEQ 36>
	<cflocation addtoken="no" url="./?pg=Products&st=View">
</cfif>