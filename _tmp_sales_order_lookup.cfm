
<cfoutput>
    <div class="col-lg-8">
    	<h2>Select Order </h2>
    </div>
    
    <!--- Customer ID Tag --->
    <div class="col-lg-4 text-right">
    	<cfif Len(FORM.Cust_Number_Anchor) GT 0>
        	<div class="cust_tag_cont">
    			<span class="cust_tag">
                	<strong>Customer ID:</strong> #FORM.Cust_Number_Anchor# <a href="http://#APPLICATION.rootURL#/?pg=Orders"><span class="glyphicon glyphicon-remove"></span></a>
                </span>
            </div>
        </cfif>
    </div>
    
    <!--- Error Message --->
    <cfif ERR.ErrorFound>
    	<div class="col-lg-12">
            <div class="form-message form-error">
                <ul>
                    <cfloop list="#ERR.ErrorMessage#" index="thisError"><li>#thisError#</li></cfloop>
                </ul>
            </div>
        </div>
    </cfif>
    
    <!--- Order Search Form --->
    <form action="" method="POST" id="SO_Search_Form" class="form-horizontal col-lg-12" role="form">
    	<div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="SO_Number" class="control-label col-lg-5">Sales Order:</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="SO_Number" id="SO_Number" class="form-control" value="#FORM.SO_Number#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="PO_Number" class="control-label col-lg-5">Purchase Order:</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="PO_Number" id="PO_Number" class="form-control" value="#FORM.PO_Number#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="Date_From" class="control-label col-lg-5">Date From: <div class="cal-cont"><span class="glyphicon glyphicon-calendar"></span></div></label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="Date_From" id="Date_From" class="form-control cal-field" value="#FORM.Date_From#" readonly="readonly"/>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="INV_Number" class="control-label col-lg-5">Invoice Number:</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="INV_Number" id="INV_Number" class="form-control" value="#FORM.INV_Number#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="Part_Number" class="control-label col-lg-5">Part Number:</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="Part_Number" id="Part_Number" class="form-control" value="#FORM.Part_Number#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="Date_To" class="control-label col-lg-5">Date To: <div class="cal-cont"><span class="glyphicon glyphicon-calendar"></span></div></label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="Date_To" id="Date_To" class="form-control cal-field" value="#FORM.Date_To#" readonly="readonly" />
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="INV_Company" class="control-label col-lg-5">Company: </label>
                    <div class="col-lg-7">
                        <select name="INV_Company" id="INV_Company" class="form-control">
                            <option value="hsc" <cfif FORM.INV_Company EQ "hsc">selected="selected"</cfif>>HME</option>
                            <option value="jtch" <cfif FORM.INV_Company EQ "jtch">selected="selected"</cfif>>JTECH</option>
                            <option value="cel" <cfif FORM.INV_Company EQ "cel">selected="selected"</cfif>>CE Repairs</option>
                            <option value="clrc" <cfif FORM.INV_Company EQ "clrc">selected="selected"</cfif>>Clear-Com</option>
                        </select>
                    </div>
                </div>
                <cfif User_IsAdmin>
                    <div class="form-group">
                        <label for="Cust_Number" class="control-label col-lg-5">Customer ID:</label>
                        <div class="col-lg-7">
                            <input type="text" maxlength="100" name="Cust_Number" id="Cust_Number" class="form-control" value="#FORM.Cust_Number#" />
                        </div>
                    </div>
                </cfif>
            </div>
        </div>
        <div class="row">
        	<div class="col-lg-4"></div>
            <div class="col-lg-4">
                <div class="form-group">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6 no-pad-search">
                    	<input type="hidden" name="SESSION_User_UID" id="SESSION_User_UID" value="#SESSION.User_UID#" />
                        <input type="submit" name="SO_Search_Submit" id="SO_Search_Submit" class="btn btn-primary form-control" value="Search" />
                    </div>
                </div>
            </div>
        </div>
    
        <div class="col-lg-12">&nbsp;</div>
        
        <!--- Order Search Results --->
        <div class="col-lg-12 table-responsive no-pad">
            <div class="col-lg-10 visible-lg no-pad">
                <h4>Search Results</h4>
            </div>
            
            <cfif User_IsDeposit>
                <input type="button" class="btn btn-success pull-right col-lg-2 col-md-12 col-sm-12 col-xs-12 " id="pay-invoice" value="Pay Deposit(s)" data-toggle="modal" data-target="##payModal"/>
            </cfif>
        
            <table id="SO_Results" class="table" style="white-space:nowrap;margin:0px;">
                <thead id="SO_Results_Header">
                    <tr>
                        <th style="width:5%;"></th>
                        <th style="width:15%;">SALES ORDER</th>
                        <th style="width:15%;">CUSTOMER ID</th>
                        <th style="width:20%;">PURCHASE ORDER</th>
                        <th style="width:15%;">ON HOLD</th>
                        <th style="width:15%;">INVOICED</th>
                        <th style="width:10%;">DATE</th>
                        <th style="width:5%;"></th>
                    </tr>
                </thead>
                <tbody>
                    <cfloop from="1" to="#ArrayLen(OrdersList.DATA.SALESORDERNUMBER)#" index="order">
                    	
                        <!--- Get Sales Order Lines --->
                        <cfquery name="qry_getOrdersLines" datasource="#APPLICATION.Datasource_AX#">
                        	SELECT olin.[SALESLINENUMBER],olin.[SALESORDERNUMBER],olin.[PARTNUMBER],olin.[PARTDESCRIPTION],olin.[SALESLINEQTYORDERED],olin.[SALESLINEUNITPRICE],olin.[SALESLINETOTALPRICE]
                            FROM HMECPSALESORDERSLINES olin
                            WHERE olin.[SALESORDERNUMBER] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#OrdersList.DATA.SALESORDERNUMBER[order]#">
                        </cfquery>
                    	
                        <tr class="SO_Result_Rows SO_Result_Row_#order#" id="#order#" data-toggle="collapse" data-target=".order-line-#order#" onclick="updateStatus(#order#);">
                        	<td><input type="checkbox" name="selectINV_#OrdersList.DATA.SALESORDERNUMBER[order]#" id="selectINV_#OrdersList.DATA.SALESORDERNUMBER[order]#" class="selectCHK selectCust_#OrdersList.DATA.CUSTOMERID[order]#"/></td>
                            <td>
                            	<cfif OrdersList.DATA.SALESSTATUS[order] EQ 3>
                            		<a href="javascript:void(0);" onclick="pauseStatus('#OrdersList.DATA.SALESORDERNUMBER[order]#')">#OrdersList.DATA.SALESORDERNUMBER[order]#</a>
                                <cfelse>
                                	#OrdersList.DATA.SALESORDERNUMBER[order]#
                                </cfif>
                            </td>
                            <td>#OrdersList.DATA.CUSTOMERID[order]#</td>
                            <td>#OrdersList.DATA.PURCHASEORDERNUMBER[order]#</td>
                            <td>#OrdersList.DATA.MCRHOLDCODEDESCRIPTION[order]#</td>
                            <td>
                                <cfif OrdersList.DATA.SALESSTATUS[order] EQ 3>
                                    <span class="glyphicon glyphicon-ok-circle greenStatus"></span>
                                <cfelse>
                                    <span class="glyphicon glyphicon-remove-circle redStatus"></span>
                                </cfif>
                            </td>
                            <td>#DateFormat(OrdersList.DATA.SALESORDERDATE[order],'mm/dd/yyyy')#</td>
                            <td class="text-right"><span class="plus#order# glyphicon glyphicon-plus-sign"></span></td>
                        </tr>
                        
                        <cfif qry_getOrdersLines.RecordCount NEQ 0>
                        	<tr class="ord-row ord-row-#order#">
                            	<td></td>
                                <td></td>
                                <td colspan="6">
                                	<div class="order-line-#order# collapse">
                                    	<table class="table-striped">
                                        	<thead>
                                            	<tr>
                                                	<th>LINE</th>
                                                    <th>PART NUMBER</th>
                                                    <th>DESCRIPTION</th>
                                                    <th>QUANTITY</th>
                                                    <th>UNIT PRICE</th>
                                                    <th>TOTAL PRICE</th>                                                  
												</tr>
											</thead>
											<tbody>
                                            	<cfloop from="1" to="#qry_getOrdersLines.RecordCount#" index="line">
                                                	<tr>
                                                    	<td>#NumberFormat(qry_getOrdersLines.SALESLINENUMBER[line],"009")#</td>
                                                        <td>#qry_getOrdersLines.PARTNUMBER[line]#</td>
                                                        <td>#LEFT(qry_getOrdersLines.PARTDESCRIPTION[line],25)#</td>
                                                        <td>#NumberFormat(qry_getOrdersLines.SALESLINEQTYORDERED[line])#</td>
                                                        <td>#DollarFormat(qry_getOrdersLines.SALESLINEUNITPRICE[line])#</td>
                                                        <td>#DollarFormat(qry_getOrdersLines.SALESLINETOTALPRICE[line])#</td>
                                                    </tr>
                                                </cfloop>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </cfif>
                        
                    </cfloop>
                </tbody>
            </table>
            
            <div class="col-lg-12 no-pad csp-paginate-cont">
                <div class="col-lg-6 no-pad">
                    <div class="col-lg-1 no-pad">
                        <button id="CSP_Prev_Page" name="CSP_Prev_Page" class="btn btn-primary btn-left" <cfif VARIABLES.Page_Start EQ 1>disabled="disabled"</cfif>><span class="glyphicon glyphicon-arrow-left"></span></button>
                    </div>
                    <div class="col-lg-11 text-left">
                        <div class="csp-perpage">
                            <span>
                                Show
                                <select id="CSP_Per_Page" name="CSP_Per_Page">
                                    <option value="5" <cfif FORM.CSP_Per_Page EQ 5>selected="selected"</cfif>>5</option>
                                    <option value="10" <cfif FORM.CSP_Per_Page EQ 10>selected="selected"</cfif>>10</option>
                                    <option value="20" <cfif FORM.CSP_Per_Page EQ 20>selected="selected"</cfif>>20</option>
                                </select>
                                items per page
                           </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 no-pad">
                    <div class="col-lg-11 text-right"><div class="csp-results">Showing <span class="csp-page-start">#VARIABLES.Page_Start#</span> - <span class="csp-page-end">#VARIABLES.Page_End#</span> of (#OrdersCount#) Results</div></div>
                    <div class="col-lg-1 no-pad">
                        <button id="CSP_Next_Page" name="CSP_Next_Page" class="btn btn-primary btn-right pull-right" <cfif VARIABLES.Page_End GTE OrdersCount>disabled="disabled"</cfif>><span class="glyphicon glyphicon-arrow-right"></span></button>
                    </div>
                </div>
                <input type="hidden" id="CSP_Current_Page" name="CSP_Current_Page" value="#FORM.CSP_Current_Page#" />
            </div>
        </div>
    </form>

    <form name="Cust_Anchor_Form" id="Cust_Anchor_Form" method="post" action="./?pg=Invoices">
        <input type="hidden" name="Cust_Number_Anchor" id="Cust_Number_Anchor" value="#FORM.Cust_Number_Anchor#" />
        <input type="hidden" name="SO_Number_Anchor" id="SO_Number_Anchor" value="#FORM.SO_Number_Anchor#" />
    </form>

</cfoutput>

<cfset paymentType = "Deposit">
<cfinclude template="./_tmp_payment_form.cfm">
<cfinclude template="./_tmp_search_form.cfm">