<cfparam name="FORM.CSP_Page_Form" default="">
<cfparam name="FORM.CSP_Per_Page" default="5">
<cfparam name="FORM.CSP_Current_Page" default="1">
<cfparam name="VARIABLES.Page_Start" default="1">
<cfparam name="VARIABLES.Page_End" default="5">

<cfparam name="FORM.Cust_Number" default="">
<cfparam name="FORM.Cust_Name" default="">
<cfparam name="FORM.Cust_City" default="">
<cfparam name="FORM.Cust_State" default="">
<cfparam name="FORM.Cust_PostCode" default="">
<cfparam name="FORM.Cust_SO" default="">
<cfparam name="FORM.Cust_INV" default="">
<cfparam name="FORM.Cust_SN" default="">
<cfparam name="FORM.Cust_Email" default="">

<cfscript>
	CustInfoObj = CreateObject("component", "_cfcs.CustInfo");
	CustomerCount = DeserializeJSON(CustInfoObj.GetCustomerCount(SESSION.User_UID,FORM.Cust_Number,FORM.Cust_Name,FORM.Cust_City,FORM.Cust_State,FORM.Cust_PostCode,FORM.Cust_SO,FORM.Cust_INV,FORM.Cust_SN,FORM.Cust_Email));
	CustomerList = DeserializeJSON(CustInfoObj.GetCustomerList(SESSION.User_UID,FORM.CSP_Current_Page,FORM.CSP_Per_Page,FORM.Cust_Number,FORM.Cust_Name,FORM.Cust_City,FORM.Cust_State,FORM.Cust_PostCode,FORM.Cust_SO,FORM.Cust_INV,FORM.Cust_SN,FORM.Cust_Email));
	
	VARIABLES.Page_Start = ((FORM.CSP_Current_Page-1)*FORM.CSP_Per_Page)+1;
	VARIABLES.Page_End = FORM.CSP_Current_Page*FORM.CSP_Per_Page;
</cfscript>

