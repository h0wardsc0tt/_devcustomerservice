<cfparam name="FORM.Card_Number" default="4111111111111111">
<cfparam name="FORM.Card_Exp" default="012020">
<cfparam name="FORM.Card_CVC" default="0822">
<cfparam name="FORM.Card_Holder" default="Christopher D Garwick">
<cfparam name="FORM.Card_Amount" default="12">
<cfparam name="FORM.Card_Zip" default="92064">

<cfset firstName = ListFirst(FORM.Card_Holder, " ")>
<cfset lastName = ListRest(FORM.Card_Holder, " ")>

<cfscript>
	//PayPal Values
	APPLICATION.PayPal_PARTNER = "wfb";
	APPLICATION.PayPal_VENDOR = "hme174223514";
	APPLICATION.PayPal_USER = "webSite";
	APPLICATION.PayPal_PASS = "HmeWfb97531";
	APPLICATION.PayPal_SIGN = "AghJC9P7dquxmrwVUyV6BnNg6dVlAB9BBrp2PzZjVnAJUNRWrkf7kXOq";
	//APPLICATION.PayPal_URL 	= "https://payflowpro.paypal.com";
	APPLICATION.PayPal_URL 	= "https://pilot-payflowpro.paypal.com";
	APPLICATION.PayPal_CURR = "USD";
	APPLICATION.PayPal_APIV = "92.0";
	APPLICATION.PayPal_useProxy = "false";
	APPLICATION.PayPal_proxyName = "";
	APPLICATION.PayPal_proxyPort = "";
</cfscript>

<!---Capture Info and Submit Payment--->
<cfobject component="_cfcs.CallerService" name="caller">

<cfscript>
	requestData = StructNew();
	requestData.METHOD = "DoDirectPayment";
	requestData.PAYMENTACTION = "Sale";
	requestData.USER = "#APPLICATION.PayPal_USER#";
	requestData.VENDOR = "#APPLICATION.PayPal_VENDOR#";
	requestData.PARTNER = "#APPLICATION.PayPal_PARTNER#";
    requestData.PWD = "#APPLICATION.PayPal_PASS#";
	requestData.FIRSTNAME = "#firstName#";
	requestData.LASTNAME = "#lastName#";
	requestData.STREET = "11440 Stowe Dr";
	requestData.CITY = "San Diego";
	requestData.STATE = "California";
	requestData.COUNTRYCODE = "US";
	requestData.ZIP = "92064";
	requestData.IPADDRESS="#CGI.REMOTE_ADDR#";
	requestData.SUBJECT = "";//No Subject Needed for 3 token: User, PWD, Signature
	requestData.VERSION = "#APPLICATION.PayPal_APIV#";
	requestData.CURRENCYCODE = "USD";
	requestData.SIGNATURE = "#APPLICATION.PayPal_SIGN#";
	requestData.CREDITCARDTYPE = "Visa";
	requestData.TENDER="C";
	requestData.TRXTYPE="S";
	requestData.ACCT = "#FORM.Card_Number#";
	requestData.EXPDATE = "#FORM.Card_Exp#";
	requestData.AMT = "#FORM.Card_Amount#"; 
</cfscript>

<!--- Calling doHttppost for API call --->
<cfinvoke component="_cfcs.CallerService" method="doHttppost" returnvariable="response">
    <cfinvokeargument name="requestData" value="#requestData#">
    <cfinvokeargument name="serverURL" value="#APPLICATION.PayPal_URL#">
    <cfinvokeargument name="proxyName" value="#APPLICATION.PayPal_proxyName#">
    <cfinvokeargument name="proxyPort" value="#APPLICATION.PayPal_proxyPort#">
    <cfinvokeargument name="useProxy" value="#APPLICATION.PayPal_useProxy#">
</cfinvoke>

<cfset responseStruct = caller.getNVPResponse(#URLDecode(response)#)>

<cfdump var="#responseStruct#">