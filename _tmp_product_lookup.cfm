
<cfoutput>
    <div class="col-lg-12 product-head">
    	<div class="col-lg-12 no-pad">
    		<h2>Search Results</h2>
        </div>
        <!---<div class="col-lg-6">
        	<div class="form-group pull-right">
            	<form class="form-inline">
                    <label for="product-cust">Customer ID: </label>
                    <select class="form-control" id="Product_Cust" name="Product_Cust">
                    	<cfloop list="#SESSION.User_CustId#" index="custId">
                        	<option value="#custId#">#custId#</option>
                        </cfloop>
                    </select>
                </form>
            </div>
        </div> --->       
    </div>
    <div class="col-lg-12 no-pad product-cont">	
    	<cfloop from="1" to="#ArrayLen(ProductList.DATA.ITEMID)#" index="product">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="col-lg-12 product-image">
                    <a href="./?pg=Products&st=Details&id=#ProductList.DATA.DAXINTEGRATIONKEY[product]#"><img src="./images/no-image.jpg" class="animated fadeIn grow" alt="No Image Available" /></a>
                </div>
                <div class="col-lg-12 no-pad">
                    <h6><a href="./?pg=Products&st=Details&id=#ProductList.DATA.DAXINTEGRATIONKEY[product]#" data-toggle="tooltip" title="#ProductList.DATA.NAMEALIAS[product]#"><cfif LEN(ProductList.DATA.NAMEALIAS[product]) GT 23>#LEFT(ProductList.DATA.NAMEALIAS[product], 23)#...<cfelse>#ProductList.DATA.NAMEALIAS[product]#</cfif></a></h6>
                    <div class="product-part"><strong>Part Number: </strong> #ProductList.DATA.ITEMID[product]#</div>
                    <div class="product-new">New</div>
                    <div class="product-price">$6.00</div>
                </div>
            </div>
        </cfloop>
    </div>
    <div class="col-lg-12 no-pad paginate-cont">
    	<form action="" method="POST" name="Product_Page_Form" id="Product_Page_Form">
            <div class="col-lg-6">
                <div class="col-lg-1 no-pad">
                    <button id="Product_Prev_Page" name="Product_Prev_Page" class="btn btn-primary btn-left" <cfif VARIABLES.Page_Start EQ 1>disabled="disabled"</cfif>><span class="glyphicon glyphicon-arrow-left"></span></button>
                </div>
                <div class="col-lg-11 text-left">
                    <div class="product-perpage">
                        <span>
                            Show
                            <select id="Product_Per_Page" name="Product_Per_Page">
                                <option value="12" <cfif FORM.Product_Per_Page EQ 12>selected="selected"</cfif>>12</option>
                                <option value="20" <cfif FORM.Product_Per_Page EQ 20>selected="selected"</cfif>>20</option>
                                <option value="80" <cfif FORM.Product_Per_Page EQ 80>selected="selected"</cfif>>80</option>
                            </select>
                            items per page
                       </span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="col-lg-11 text-right"><div class="product-results">Showing <span class="product-page-start">#VARIABLES.Page_Start#</span> - <span class="product-page-end">#VARIABLES.Page_End#</span> of (#ProductCount.DATA.ITEMCOUNT[1]#) Results</div></div>
                <div class="col-lg-1 no-pad">
                    <button id="Product_Next_Page" name="Product_Next_Page" class="btn btn-primary btn-right pull-right" <cfif VARIABLES.Page_End GTE ProductCount.DATA.ITEMCOUNT[1]>disabled="disabled"</cfif>><span class="glyphicon glyphicon-arrow-right"></span></button>
                </div>
            </div>
            <input type="hidden" id="Product_Search_Page" name="Product_Search_Page" value="" />
            <input type="hidden" id="Product_Current_Page" name="Product_Current_Page" value="#FORM.Product_Current_Page#" />
        </form>
    </div>
</cfoutput>