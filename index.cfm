<cfparam name="Page_Title" default="Customer Service Portal">
<cfparam name="Page_MetaTitle" default="Customer Service Portal">
<cfparam name="Page_MetaKeywords" default="">
<cfparam name="Page_MetaDescription" default="">
<cfparam name="User_IsLoggedIn" default="false">
<cfparam name="User_IsAdmin" default="">
<cfparam name="User_IsFullAdmin" default="">
<cfparam name="User_IsAtLogin" default="false">
<cfparam name="URL.pg" default="">
<cfparam name="URL.st" default="">
<cfparam name="URL.cust" default="">
<cfparam name="Requires_Login" default="false"><!---Does page require login--->
<cfparam name="CurrentDateTime" default="">
<cfparam name="SESSION.registerAttempts" default="0">
<cfparam name="FORM.Product_Search" default="">
<cfparam name="FORM.Product_Search_Page" default="">

<cfset CurrentDateTime = DateFormat(Now(),'yyyy-mm-dd') & ' ' & TimeFormat(Now(),'HH:mm:ss')>

<cfif 
	StructKeyExists(SESSION, "Session_UID") 
	AND 
	StructKeyExists(SESSION, "User_UID")
	AND 
	StructKeyExists(SESSION, "IsLoggedIn")>
	
	<cfscript>
		checkSession = CreateObject("component", "_cfcs.SessionMgmt");
		statsSession = checkSession.verifySession(Session_UID=SESSION.Session_UID,User_UID=SESSION.User_UID);
	</cfscript>
	
	<cfif statsSession EQ 1>
    
    	<cfset User_IsAdmin = SESSION.User_IsAdmin>
        <cfset User_IsFullAdmin = SESSION.User_IsFullAdmin>
   
		<cfscript>
			QUERY = StructNew(); //Only retrieve user information for valid sessions
			QUERY.QueryName = "qry_getUserInfo";
			QUERY.Datasource = APPLICATION.Datasource;
			QUERY.User_UID = SESSION.User_UID;
		</cfscript>
		<cfinclude template="/_qry_select_user.cfm">

		<!--- Check to see if User is active --->
		<cfif qry_getUserInfo.User_IsActive EQ 0>
			<cfset ERR.ErrorFound = true>
			<cfset ERR.ErrorMessage = ListAppend(ERR.ErrorMessage,"This user account is not active. Please contact customer service.")>
		</cfif>
        
		<cfif ERR.ErrorFound>
			<!---Force Logout and push to login page with error message--->
			<cfif StructClear(SESSION)></cfif>
			<cfset THIS.OnApplicationStart()>
			<cfset URL.pg = "Login">
		<cfelse>
			<cfset User_IsLoggedIn = true>
            
            <cfscript>
				getUsersPermissions = CreateObject("component", "_cfcs.UserPermission");
				getUsersType = getUsersPermissions.getPermissionByUserID(qry_getUserInfo.User_ID);
				
				if(getUsersType.DepositCard OR getUsersType.UserType_ID EQ 4){
					User_IsDeposit = 1;
				} else {
					User_IsDeposit = 0;	
				}
				
				if(getUsersType.CreditCard OR getUsersType.UserType_ID EQ 4){
					User_IsCredit = 1;
				} else {
					User_IsCredit = 0;	
				}
			
				getUserApps = CreateObject("component", "_cfcs.UserPermission");
				User_Apps = getUserApps.getApplicationByUserID(qry_getUserInfo.User_ID);
				
				if(ListLen(ValueList(User_Apps.App_FullName))){
					User_App_Default = ListFirst(ValueList(User_Apps.App_FullName));
				} else {
					User_App_Default = "Account";
				}
			</cfscript>
            
            <cfif Len(FORM.Product_Search_Page)>
				<cfset FORM.Product_Search = FORM.Product_Search_Page>
            </cfif>
		</cfif>
 
	</cfif>
<cfelse>
	<cfset noRedirect = "Login,ManageAccount,CreateAccount">
	<cfif NOT ListFind(noRedirect, URL.pg)>
		<cflocation url="./?pg=Login" addtoken="no">
		<cfabort>
	</cfif>
</cfif>

<cfif URL.pg EQ "Login" AND Len(URL.st) EQ 0>
	<cfset User_IsAtLogin = true>
</cfif>

<cfif URL.pg EQ "Login" AND URL.st EQ "Validate">
	<cfset User_IsAtLogin = true>
</cfif>

<cfsetting showdebugoutput="no">

<cfinclude template="./_tmp_HTML_Header.cfm">

<!--- Check if User is Logged in --->
<cfif User_IsLoggedIn>
	<cfinclude template="./_tmp_HTML_Navigation_LoggedIn.cfm">
<cfelse>
	<cfinclude template="./_tmp_HTML_Navigation_LoggedOut.cfm">
</cfif>

<!--- Check if User is at Login screen --->
<cfif User_IsAtLogin>
	<div class="container container-table">
		<div id="Content" class="content-login">
<cfelse>
	<div class="container container-main">
		<div id="Content">
</cfif>

<cfswitch expression="#URL.pg#">
    <cfcase value="Login">
        <cfinclude template="./security/_dat_login_prep.cfm">
        <cfswitch expression="#URL.st#">
            <cfcase value="Create">
                <cfinclude template="./security/_tmp_login_create.cfm">
            </cfcase>
            <cfcase value="CreateConfirm">
                <cfinclude template="./security/_tmp_login_create_confirm.cfm">
            </cfcase>
            <cfcase value="Validate">
                <cfinclude template="./security/_tmp_login_check.cfm">
            </cfcase>
            <cfdefaultcase>
                <cfinclude template="./security/_tmp_login_form.cfm">
            </cfdefaultcase>
        </cfswitch>
    </cfcase>
    <cfcase value="Logout">
        <cfinclude template="./security/_tmp_logout.cfm">
    </cfcase>
    <cfcase value="ManageAccount">
        <cfswitch expression="#URL.st#">
            <cfcase value="rq">
                <cfinclude template="./security/_tmp_login_resetreq.cfm">
            </cfcase>
            <cfcase value="rp">
                <cfinclude template="./security/_tmp_login_resetpas.cfm">
            </cfcase>
            <cfcase value="rl">
                <cfinclude template="./security/_tmp_login_resetlnk.cfm">
            </cfcase>
            <cfcase value="rc">
                <cfinclude template="./security/_tmp_login_resetcnf.cfm">
            </cfcase>
            <cfdefaultcase>
                <cfinclude template="./security/_tmp_login_form.cfm">
            </cfdefaultcase>
        </cfswitch>
    </cfcase>
    <cfcase value="Account">
        <cfinclude template="./_dat_account_prep.cfm">
        <cfswitch expression="#URL.st#">
            <cfcase value="Confirm">
                <cfinclude template="./_tmp_account_confirm.cfm">
            </cfcase>
            <cfdefaultcase>
                <cfinclude template="./_tmp_account_edit.cfm">
            </cfdefaultcase>
        </cfswitch>
    </cfcase>
    <cfcase value="Verify">
    	<cfif User_IsFullAdmin OR ListFind(ValueList(User_Apps.App_FullName), "User Management") NEQ 0>
            <cfinclude template="./security/_dat_verify_prep.cfm">
            <cfswitch expression="#URL.st#">
                <cfcase value="Confirm">
                    <cfinclude template="./security/_tmp_login_verify_confirm.cfm">
                </cfcase>
                <cfcase value="View">
                    <cfinclude template="./security/_tmp_login_verify.cfm">
                </cfcase>
                <cfdefaultcase>
                    <cfinclude template="./security/_tmp_login_verify_lookup.cfm">
                </cfdefaultcase>
            </cfswitch>
        <cfelse>
        	<cflocation url="./?pg=#User_App_Default#" addtoken="no">
        </cfif>
    </cfcase>
    <cfcase value="Users">
    	<cfif User_IsFullAdmin OR ListFind(ValueList(User_Apps.App_FullName), "User Management") NEQ 0>
            <cfswitch expression="#URL.st#">
                <cfcase value="Edit">
                	<cfinclude template="./_dat_user_prep.cfm">
                    <cfinclude template="./_tmp_user_edit.cfm">
                </cfcase>
                <cfcase value="Confirm">
                	<cfinclude template="./_dat_user_prep.cfm">
                    <cfinclude template="./_tmp_user_confirm.cfm">
                </cfcase>
                <cfdefaultcase>
                	<cfinclude template="./_dat_users_prep.cfm">
                    <cfinclude template="./_tmp_user_lookup.cfm">
                </cfdefaultcase>
            </cfswitch>
        <cfelse>
        	<cflocation url="./?pg=#User_App_Default#" addtoken="no">
        </cfif>
    </cfcase>
    <cfcase value="Customers">
    	<cfif User_IsFullAdmin OR ListFind(ValueList(User_Apps.App_FullName), "Customers") NEQ 0>
        	<cfinclude template="./_dat_customer_prep.cfm">
            <cfswitch expression="#URL.st#">
                <cfdefaultcase>
                    <cfinclude template="./_tmp_customer_lookup.cfm">
                </cfdefaultcase>
            </cfswitch>
        <cfelse>
        	<cflocation url="./?pg=#User_App_Default#" addtoken="no">
        </cfif>
    </cfcase>
    <cfcase value="PurchaseRequisitionLookUp">
    	<cfif User_IsFullAdmin OR ListFind(ValueList(User_Apps.App_FullName), "PurchaseRequisitionLookUp") NEQ 0>
        	<cfinclude template="./_dat_PurchaseRequisitionLookUp_prep.cfm">
            <cfswitch expression="#URL.st#">
                <cfdefaultcase>
                    <cfinclude template="./_tmp_PurchaseRequisition_lookup.cfm">
                </cfdefaultcase>
            </cfswitch>
        <cfelse>
        	<cflocation url="./?pg=#User_App_Default#" addtoken="no">
        </cfif>
    </cfcase>
    <cfcase value="PurchaseRequisition">
    	<cfif User_IsFullAdmin OR ListFind(ValueList(User_Apps.App_FullName), "PurchaseRequisition") NEQ 0>
        	<cfinclude template="./_dat_PurchaseRequisition_prep.cfm">
            <cfswitch expression="#URL.st#">
                <cfdefaultcase>
                    <cfinclude template="./_tmp_PurchaseRequisition.cfm">
                </cfdefaultcase>
            </cfswitch>
        <cfelse>
        	<cflocation url="./?pg=#User_App_Default#" addtoken="no">
        </cfif>
    </cfcase>
    <cfcase value="Orders">
    	<cfif User_IsFullAdmin OR ListFind(ValueList(User_Apps.App_FullName), "Orders") NEQ 0>
        	<cfinclude template="./_dat_order_prep.cfm">
            <cfswitch expression="#URL.st#">
                <cfdefaultcase>
                    <cfinclude template="./_tmp_sales_order_lookup.cfm">
                </cfdefaultcase>
            </cfswitch>
        <cfelse>
        	<cflocation url="./?pg=#User_App_Default#" addtoken="no">
		</cfif>
    </cfcase>
    <cfcase value="Invoices">
    	<cfif User_IsFullAdmin OR ListFind(ValueList(User_Apps.App_FullName), "Invoices") NEQ 0>
            <cfswitch expression="#URL.st#">
                <cfcase value="View">
                    <cfinclude template="./_dat_invoice_prep.cfm">
                    <cfinclude template="./_tmp_invoice_view.cfm">
                </cfcase>
                <cfcase value="Print">
                    <cfinclude template="./_dat_invoice_prep.cfm">
                    <cfinclude template="./_tmp_invoice_print.cfm">
                </cfcase>
                <cfdefaultcase>
                    <cfinclude template="./_tmp_invoice_lookup.cfm">
                </cfdefaultcase>
            </cfswitch>
        <cfelse>
        	<cflocation url="./?pg=#User_App_Default#" addtoken="no">
        </cfif>
    </cfcase>
    <cfcase value="Products">
    	<cfif User_IsFullAdmin OR ListFind(ValueList(User_Apps.App_FullName), "Store") NEQ 0>
        	<cfinclude template="./_dat_product_prep.cfm">
            <cfswitch expression="#URL.st#">
            	<cfcase value="View">
                	<cfinclude template="./_tmp_product_lookup.cfm">
                </cfcase>
                <cfcase value="Details">
                	<cfinclude template="./_tmp_product_details.cfm">
                </cfcase>
                <cfdefaultcase>
                    <cfinclude template="./_tmp_product_categories.cfm">
                </cfdefaultcase>
            </cfswitch>
        <cfelse>
        	<cflocation url="./?pg=#User_App_Default#" addtoken="no">
		</cfif>
    </cfcase>
    <cfcase value="Cart">
    	<cfif User_IsFullAdmin OR ListFind(ValueList(User_Apps.App_FullName), "Store") NEQ 0>
        	<cfinclude template="./_dat_cart_prep.cfm">
            <cfswitch expression="#URL.st#">
                <cfdefaultcase>
                    <cfinclude template="./_tmp_cart_view.cfm">
                </cfdefaultcase>
            </cfswitch>
        <cfelse>
        	<cflocation url="./?pg=#User_App_Default#" addtoken="no">
		</cfif>
    </cfcase>
    <cfdefaultcase>
        <cfset Requires_Login = true>
        <cfif NOT User_IsLoggedIn>
            <cflocation url="./?pg=Login" addtoken="no">
            <cfabort>
        <cfelse>
            <cfif User_IsAdmin>
                <cflocation url="./?pg=Customers" addtoken="no">
            <cfelse>
                <cflocation url="./?pg=Orders" addtoken="no">
            </cfif>
        </cfif>
    </cfdefaultcase>
</cfswitch>
<cfinclude template="./_tmp_HTML_Footer.cfm">