
<cfsetting showdebugoutput="no">
<cfparam name="URL.lookup" default="">
<cfinclude template="../serverSettings.cfm">

<cfscript>	

	
	if (#FindNoCase('application/json', cgi.content_type)# > 0)
	{
    	json = deserializeJSON(ToString(getHTTPRequestData().content));
		type = json.type;
		purchase_req  = json.json;
	}
	else
	{
		//phrase = URL.lookup;
		//search_type = URL.seachType;
		//search_for  = URL.searhFor;
	}
	supplier = 1;
	shipping = 2;
	requestor = 3;
	requestor_date = 4;
	account_number = 5;
	dept = 6;
	po = 7;
	buyer = 8;
	shipvia = 9;
	comments = 10;
 	object = CreateObject("component", "_cfcs.productReq");
	order = purchase_req.order[1];
	returnResult = object.updatePurchaseReq(
		id = #purchase_req.key#,
		type = "#type#",
		purchaseOrder = "#purchase_req['order'][po]['value']#",
		buyer = "#purchase_req.order[buyer]['value']#",
		shipVia = "#purchase_req.order[shipvia]['value']#",
		supplier = "#purchase_req.order[supplier]['value']#",
		shipper = "#purchase_req.order[shipping]['value']#",
		requestor = "#purchase_req.order[requestor]['value']#",
		requestedDate = "#purchase_req.order[requestor_date]['value']#",
		accountNumber = "#purchase_req.order[account_number]['value']#",
		department = "#purchase_req.order[dept]['value']#",	
		comments = "#purchase_req.order[comments]['value']#"
	); 

	returnObj = DeserializeJSON(returnResult);
	pr_key = #purchase_req.key#;
	if(pr_key == '')
		pr_key = #returnObj.identity#;
		
	lineItem = 1;
	quantity = 2;
	umo = 3;
	estcost = 4;
	needdate = 5;
	manufacturedesc = 6;
	purchasequantity = 7;
	price = 8;
	dockdate = 9;

	for(i = 1; i LTE ArrayLen(purchase_req.line_items); i++){
		liArray = purchase_req.line_items[i];
		li = liArray.line_item[1];
		returnResultLineitem = object.updatePurchaseReqLineitem(
			id = #liArray.key#,
			key = #pr_key#,
			type = "#type#",
			lineItem = "#purchase_req.line_items[i].line_item[lineItem].value#",
			quantity = #purchase_req.line_items[i].line_item[quantity].value#,
			umo = "#purchase_req.line_items[i].line_item[umo].value#",
			estcost = "#ReplaceNoCase(ReplaceNoCase(purchase_req.line_items[i].line_item[estcost].value,'$',''),',','','ALL')#",
			needdate = "#purchase_req.line_items[i].line_item[needdate].value#",
			manufacturedesc = "#purchase_req.line_items[i].line_item[manufacturedesc].value#",
			purchasequantity = #purchase_req.line_items[i].line_item[purchasequantity].value#,
			price = "#ReplaceNoCase(ReplaceNoCase(purchase_req.line_items[i].line_item[price].value,'$',''),',','','ALL')#",
			dockDate = "#purchase_req.line_items[i].line_item[dockDate].value#",
			lineItemId = "#purchase_req.line_items[i].id#"
		); 			 
		returnLineItemObj = DeserializeJSON(returnResultLineitem);
		arrayAppend(returnObj.lineItems, returnLineItemObj, true);
	}
	//if(type == 'update' && purchase_req.status == 'new')
	//	type = 'new';
	//response = '{"status":"OK", "type":"' & type & '", "error":""}';
	//response = '{"status":"OK", "error":"'  & purchase_req.status & '"}';
	//response = '{"status":"OK", "error":"'  & purchase_req.order[1].value & '"}';

	//writeOutput(purchase_req.status);
	//writeOutput(serializer.serialize(purchase_req));
	serializer = new lib.JsonSerializer();
	writeOutput(serializer.serialize(returnObj));
</cfscript>



    
  


