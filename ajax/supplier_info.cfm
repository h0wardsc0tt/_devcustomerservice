<cfsetting showdebugoutput="no">
<cfparam name="URL.lookup" default="">
<cfinclude template="../serverSettings.cfm">

<cfscript>	

	
	if (#FindNoCase('application/json', cgi.content_type)# > 0)
	{
    	myData = deserializeJSON(ToString(getHTTPRequestData().content));
		key = myData.key;
	}
	else
	{
		key = URL.key;
	}
	
	object = CreateObject("component", "#cfcs#.productReq"); 
	returnResult = object.getshipping(key = "#key#"); 

	serializer = new lib.JsonSerializer()
		.asString( "street" )
		.asString( "city" )
		.asString( "state_code" )
		.asString( "zip" )
	;
	writeOUtput(serializer.serialize(returnResult));
</cfscript>
