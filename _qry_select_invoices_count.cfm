<cfparam name="QUERY" default="#StructNew()#">
<cfparam name="QUERY.QueryName" default="qry_getAllInvoices">
<cfparam name="QUERY.Datasource" default="#APPLICATION.Datasource_AX#">
<cfparam name="QUERY.SelectSQL" default="sinv.*">
<cfparam name="QUERY.OrderBy" default="sinv.INVOICENUMBER">

<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#" maxrows="1000">
	SELECT #QUERY.SelectSQL#
	FROM 
		HMECPSALESORDERSINVOICELOOKUP sinv
	WHERE 0=0
	<cfif StructKeyExists(QUERY,"CUSTOMERID")>
	AND sinv.CUSTOMERID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.CUSTOMERID#" list="yes">)
	</cfif>
    <cfif StructKeyExists(QUERY,"DATAAREAID")>
	AND sinv.DATAAREAID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.DATAAREAID#">
	</cfif>
    <cfif StructKeyExists(QUERY,"SALESORDERNUMBER")>
	AND sinv.SALESORDERNUMBER LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.SALESORDERNUMBER#%">
	</cfif>
    <cfif StructKeyExists(QUERY,"INVOICENUMBER")>
	AND sinv.INVOICENUMBER LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.INVOICENUMBER#%">
	</cfif>
    <cfif StructKeyExists(QUERY,"INVOICEDATEFROM") AND StructKeyExists(QUERY,"INVOICEDATETO")>
	AND sinv.INVOICEDATE BETWEEN <cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.INVOICEDATEFROM#"> AND <cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.INVOICEDATETO#">
	</cfif>
    <cfif StructKeyExists(QUERY,"INVOICEBALANCE")>
    	<cfif QUERY.INVOICEBALANCE EQ "Open">
			AND sinv.INVOICEBALANCE <> <cfqueryparam cfsqltype="cf_sql_varchar" value="0.00"> 
        <cfelse>
        	AND sinv.INVOICEBALANCE = <cfqueryparam cfsqltype="cf_sql_varchar" value="0.00"> 
        </cfif>
	</cfif>
</cfquery>

<cfquery name="#QUERY.QueryName#_Total" dbtype="query">
	SELECT Count(*) AS Records
	FROM #QUERY.QueryName#
</cfquery>
<cfif StructClear(QUERY)></cfif>