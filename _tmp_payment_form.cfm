<cfoutput>

    <!--- Payment Modal --->
    <div id="payModal" class="modal fade" role="dialog">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content">
                    <form action="./" method="POST" id="Card_Payment" class="form-horizontal" role="form">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><span>#paymentType#</span> Payment</h4>
                        </div>
                        <div class="modal-body">
                        	<div class="payments-cont">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th style="width:10%;"></th>
                                                    <th style="width:50%;">#paymentType# Number</th>
                                                    <th style="width:40%;">#paymentType# Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="total-row">
                                                    <td></td>
                                                    <td><strong>Number of #paymentType#s: </strong><span class="card-count"></span></td>
                                                    <td><strong>Total Payment: </strong><span class="card-amount"></span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>    
                                
                                <div class="row">
                                    <div class="col-lg-10 Card_Cont">
                                        <div class="form-group">
                                            <div class="col-lg-2"></div>
                                            <label for="Card_Holder" class="control-label col-lg-3">Card Holder:<span class="red-text">*</span></label>
                                            <div class="col-lg-7">
                                                <input type="text" maxlength="100" name="Card_Holder" id="Card_Holder" class="form-control" value="" required/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-2"></div>
                                            <label for="Card_Number" class="control-label col-lg-3">Card Number:<span class="red-text">*</span></label>
                                            <div class="col-lg-7">
                                                <input type="text" maxlength="16" name="Card_Number" id="Card_Number" class="form-control" value="" onkeypress="return isNumberKey(event);" onblur="getCardType();" pattern=".{15,16}" title="Must be 16 characters" required/>
                                            	<input type="hidden" name="Card_Type" id="Card_Type" value="none" />
                                                <div class="card-img"><img src="" alt="Credit Card" class=""/></div>
                                            </div>
                                            
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-2"></div>
                                            <label for="Card_Zip" class="control-label col-lg-3">Card Zip:<span class="red-text">*</span></label>
                                            <div class="col-lg-7">
                                                <input type="text" maxlength="5" name="Card_Zip" id="Card_Zip" class="form-control" value="" onkeypress="return isNumberKey(event);" pattern=".{5,}" title="Must be 5 digits" required/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-2"></div>
                                            <label for="Card_Exp" class="control-label col-lg-3">Card Exp:<span class="red-text">*</span></label>
                                            <div class="col-lg-4">
                                                <select name="Card_Exp" id="Card_Exp" class="form-control" required>
                                                    <cfset count_to = 60 - Month(Now())>
                                                    <option value=""></option>
                                                    <option value="#DateFormat(Now(),'mmyyyy')#">#DateFormat(Now(),'mm/yyyy')#</option>
                                                	<cfloop from="1" to="#count_to#" index="exp_count">
                                                    	<option value="#DateFormat(DateAdd('m',exp_count,Now()),'mmyyyy')#">#DateFormat(DateAdd('m',exp_count,Now()),'mm/yyyy')#</option>
                                                    </cfloop>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-2"></div>
                                            <label for="Card_CVC" class="control-label col-lg-3">Card CVC:<span class="red-text">*</span></label>
                                            <div class="col-lg-3">
                                                <input type="text" maxlength="4" name="Card_CVC" id="Card_CVC" class="form-control" value="" onkeypress="return isNumberKey(event);" pattern=".{3,4}" title="Must be 3 digits" required/>
                                            </div>
                                            <div class="col-lg-2 no-pad cvv2">
                                            	<a href="##" style="font-size:8px;" data-toggle="modal" data-target="##payCVV2"><img src="./images/cvv2_sm.gif" alt="CVC2" /><br />What's This?</a>
                                            </div>
                                            <div class="col-lg-2 paypal-img"><img src="./images/PayPal1.png" alt="PayPal" class="pull-right" /></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="no-payments-cont">
                            	<div class="alert alert-info text-center">
                                    <strong><span class="glyphicon glyphicon-info-sign"></span></strong> You must select at least one #paymentType# to proceed.
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-success" id="payConfirm_submit">Submit Payment</button>
                        </div>
                    </form>
                </div>
            
            </div>
        </div>
    </div>
    
    <!--- Payment CVV2 Modal --->
    <div id="payCVV2" class="modal fade" role="dialog">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-confirm vertical-align-center">
                <div class="modal-content">
                	<form action="./" method="POST" class="form-horizontal" role="form">
                    	<div class="modal-header">
                            <h4 class="modal-title">Security Code</h4>
                        </div>
                        <div class="modal-body text-center">
                            <img src="./images/SecurityCode.jpg" class="cvv2-img"/>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <!--- Payment Confirmation Modal --->
    <div id="payConfirm" class="modal fade" role="dialog">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-confirm vertical-align-center">
                <div class="modal-content">
                	<form action="./" method="POST" id="Card_Confirm" class="form-horizontal" role="form">
                        <div class="modal-header">
                            <h4 class="modal-title">Payment Confirmation</h4>
                        </div>
                        <div class="modal-body text-center">
                            <div class="payMessage">
                                <p>Your credit card will be charged for the amount of: </p>
                                <p class="card-amount-confirm"></p>
                            </div>
                            
                            <div class="paySpinner" style="display:none;">
                                <p>Please wait while we process your request...</p>
                                <div class="load-cont">
                                    <div class="loader"></div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="paySpinnerBtns">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-success" id="payProceed">Proceed</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <!--- Payment Message Modal --->
    <div id="payMessage" class="modal fade" role="dialog">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-message vertical-align-center">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Payment Notification</h4>
                    </div>
                    <div class="modal-body text-center">
                        <div class="payMessageCont">
                            <div class="alert alert-success pay-success text-left">
                            	<strong><span class="glyphicon glyphicon-ok-circle"></span> Payment Successful!</strong> Your payment has been successfully processed.
                                You will receive a confirmation email shortly. Thank you for your business.
  							</div>
                            <div class="alert alert-info pay-success">
								<strong>Confirmation: </strong><span class="pay-confirm-number"></span>
  							</div>
 
                            <div class="alert alert-danger pay-error text-left" style="display:none;">
								<strong><span class="glyphicon glyphicon-alert"></span> ERROR:</strong> Your payment has not been processed. Please correct the necessary information and try your request again.
  							</div>
                            
                            <div class="alert alert-danger pay-error pay-error-list text-left" style="display:none;margin-top:15px;">
								<ul>
                                	<li></li>
                                </ul>
  							</div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="payRecipients">Recipients</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!--- Recipient Modal --->
    <div id="payReciept" class="modal fade" role="dialog">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-message vertical-align-center" style="width:575px;">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: ##337ab7;">
                        <h4 class="modal-title">Email Recipients</h4>
                    </div>
                    <div class="modal-body text-center">
                    	<div class="send-receipt-cont">
                            <input type="text" name="Recipient_List" id="Recipient_List" value="" class="form-control" />
                            <span class="note-field">Use commas to enter multiple emails: (Ex: johndoe@yahoo.com,janedoe@yahoo.com,johnsmith@gmail.com)</span>
                        </div>
                        <div class="send-receipt-msg" style="display:none;">
                        	<div class="alert alert-success text-left">
                            	<strong><span class="glyphicon glyphicon-ok-circle"></span> Success!</strong> The recipients will receive a confirmation email shortly.
  							</div>
                        </div>
                        <div class="send-receipt-err" style="display:none;">
                        	<div class="alert alert-danger text-left">
								<strong><span class="glyphicon glyphicon-alert"></span> ERROR:</strong> The recipient list may not be blank. Please try again.
  							</div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="sendReceipt">Send</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</cfoutput>