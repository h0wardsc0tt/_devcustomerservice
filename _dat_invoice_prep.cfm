<cfparam name="URL.inv" default="">

<!---<cfif User_IsAdmin>--->
    <cfquery name="qry_getInvoiceDetails" datasource="#APPLICATION.Datasource_AX#" maxrows="1">
        SELECT ilup.[SALESTAXES],ilup.[SALESFREIGHT],ilup.[INVOICEAMOUNT],ilup.[INVOICENUMBER],ilup.[INVOICEPAYMENTS],ilup.[INVOICEBALANCE],ilup.[DATAAREAID],olup.[SALESORDERNUMBER],olup.[PURCHASEORDERNUMBER],olup.[BILLTOCUSTOMERID],olup.[SALESSTATUSLABEL],olup.[SALESORDERDATE],olup.[SHIPTOCUSTOMERID],olup.[SOLDTOCUSTOMERID],clup.[CUSTOMERID],clup.[CUSTOMERNAME],clup.[CONTACTNAME],clup.[CONTACTPHONE]
        FROM   [HMECPSALESORDERSINVOICELOOKUP] ilup
        INNER JOIN [HMECPSALESORDERSLOOKUP] olup ON ilup.[SALESORDERNUMBER] = olup.[SALESORDERNUMBER]
        INNER JOIN [HMECPCUSTOMERLOOKUP] clup ON ilup.[CUSTOMERID] = clup.[CUSTOMERID]
        WHERE  ilup.[INVOICENUMBER] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#URL.inv#">
    </cfquery>
    
    <cfquery name="qry_getInvoiceLines" datasource="#APPLICATION.Datasource_AX#">
        SELECT [LINENUM],[PARTNUMBER],[DESCRIPTION],[QUANTITY],[UNITPRICE],[DISCAMOUNT],[DISCPERCENT],[TAXES],[INVOICEDATE],[DATAAREAID],[TOTALPRICE]
        FROM   [HMECPSALESORDERSINVOICELINES]
        WHERE  [INVOICENUMBER] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#URL.inv#">
    </cfquery>
    
    <cfscript>
		billToRows = 0;
		soldToRows = 0;
		shipToRows = 0;
	
		if(Len(qry_getInvoiceDetails.BILLTOCUSTOMERID) GT 0){
			billToRows = 1;	
		}
		
		if(Len(qry_getInvoiceDetails.SOLDTOCUSTOMERID) GT 0){
			soldToRows = 1;	
		}
		
		if(Len(qry_getInvoiceDetails.SHIPTOCUSTOMERID) GT 0){
			shipToRows = 1;	
		}
	</cfscript>
    
    <cfquery name="qry_getDefaultAddress" datasource="#APPLICATION.Datasource_AX#" maxrows="1">
        SELECT DISTINCT [CUSTOMERNAME],[ADDRESSCITY],[ADDRESSSTATE],[ADDRESSZIPCODE],[ADDRESS],[CONTACTNAME]
        FROM   [HMECPCUSTOMERLOOKUP] clup
        WHERE  [CUSTOMERID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#qry_getInvoiceDetails.CUSTOMERID#">
    </cfquery>
    
    <cfquery name="qry_getShipToAddress" datasource="#APPLICATION.Datasource_AX#" maxrows="#shipToRows#">
        SELECT DISTINCT [SHIPTOCUSTOMERID],[CUSTOMERNAME],[ADDRESSCITY],[ADDRESSSTATE],[ADDRESSZIPCODE],[ADDRESS],[CONTACTNAME]
        FROM   [HMECPCUSTOMERLOOKUP] clup
        INNER JOIN [HMECPSALESORDERSLOOKUP] olup ON clup.[CUSTOMERID] = olup.[CUSTOMERID]
        WHERE  [SHIPTOCUSTOMERID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#qry_getInvoiceDetails.SHIPTOCUSTOMERID#">
    </cfquery>
    
    <cfif qry_getShipToAddress.RecordCount EQ 0>
    	<cfset qry_getShipToAddress = qry_getDefaultAddress>
    </cfif>
    
    <cfquery name="qry_getSoldToAddress" datasource="#APPLICATION.Datasource_AX#" maxrows="#soldToRows#">
        SELECT DISTINCT [SOLDTOCUSTOMERID],[CUSTOMERNAME],[ADDRESSCITY],[ADDRESSSTATE],[ADDRESSZIPCODE],[ADDRESS],[CONTACTNAME]
        FROM   [HMECPCUSTOMERLOOKUP] clup
        INNER JOIN [HMECPSALESORDERSLOOKUP] olup ON clup.[CUSTOMERID] = olup.[CUSTOMERID]
        WHERE  [SOLDTOCUSTOMERID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#qry_getInvoiceDetails.SOLDTOCUSTOMERID#">
    </cfquery>
    
    <cfif qry_getSoldToAddress.RecordCount EQ 0>
    	<cfset qry_getSoldToAddress = qry_getDefaultAddress>
    </cfif>
    
    <cfquery name="qry_getBillToAddress" datasource="#APPLICATION.Datasource_AX#" maxrows="#billToRows#">
        SELECT DISTINCT [BILLTOCUSTOMERID],[CUSTOMERNAME],[ADDRESSCITY],[ADDRESSSTATE],[ADDRESSZIPCODE],[ADDRESS],[CONTACTNAME]
        FROM   [HMECPCUSTOMERLOOKUP] clup
        INNER JOIN [HMECPSALESORDERSLOOKUP] olup ON clup.[CUSTOMERID] = olup.[CUSTOMERID]
        WHERE  [BILLTOCUSTOMERID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#qry_getInvoiceDetails.BILLTOCUSTOMERID#">
    </cfquery>
    
    <cfif qry_getBillToAddress.RecordCount EQ 0>
    	<cfset qry_getBillToAddress = qry_getDefaultAddress>
    </cfif>
<!---<cfelse>
	<cflocation url="./?pg=Invoices" addtoken="no">
	<cfabort>
</cfif>--->