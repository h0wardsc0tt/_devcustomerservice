<div id="line_item_clone" style="display:none;">
    <div id="itemRow_1" class="row items itemRowClone"  style="border:solid 1px #000000; border-top:0; padding: 3px 0px 3px 0px">
        <div class="line-item"  style="text-align:center; display:inline-block; width:11%; padding:0;">
            <i class="fa fa-plus add-row" style="float:left; padding-top:7px;" 
                onClick="newLineItem();"></i>
            <input itemid="lineItem" type="search" class="text-box-update line-item" style="width:80%; text-align:center;">
                <i class="fa fa-times" style="float:right; padding-top:7px;" 
                onClick="deleteRowRequest($(this).parent().parent());"></i>
            </div>
        <div class=" line-item" style="display:inline-block; width:5%; padding:0;" >
        <input itemid="quantity" type="search" class="text-box-update line-item" style="width:100%;text-align:center;">
        </div>
        <div class="line-item"  style="display:inline-block; width:10%; padding:0;">
        <input itemid="umo" type="search" class="text-box-update line-item" style="width:100%;text-align:center;">
        </div>
        <div class=" line-item"  style="display:inline-block; width:10%; padding:0;">
        <input itemid="estcost" type="search" class="text-box-update line-item numeric" style="width:100%;text-align:center;">
        </div>
        <div class=" line-item" style="display:inline-block; width:10%; padding:0;">
        <input itemid="needdate" type="search" class="datepickerClone text-box-update line-item date" itemid="needdate" 
        	style="width:99%;text-align:center;">
        </div>
        <div class=" line-item"  style="display:inline-block; width:25%; padding:0;">
        <input itemid="manufacturedesc" type="search" class="text-box-update line-item" style="width:100%;">
        </div>
        <div class=" line-item"  style="display:inline-block; width:5%; padding:0;">
        <input itemid="purchasequantity" type="search" class="text-box-update line-item numeric" style="width:100%;text-align:center;">
        </div>
        <div class=" line-item"  style="display:inline-block; width:10%; padding:0;">
        <input itemid="price" type="search"  class="text-box-update line-item numeric" style="width:100%;text-align:center;">
        </div>
        <div class=" line-item" style="display:inline-block; width:10%; padding:0;" >
        <input itemid="dockdate" type="search" class="datepickerClone text-box-update line-item date" style="width:100%;text-align:center;">
        </div>
    </div>  
</div>
