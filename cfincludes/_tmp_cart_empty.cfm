
<cfoutput>
	<!--- Empty Cart --->
    <div class="product-cart-empty-cont text-center">
        <div class="col-lg-4"></div>
        <div class="col-lg-4 product-cart-empty">
            <h2>Your Cart is empty</h2>
            <p><span class="glyphicon glyphicon-shopping-cart"></span></p>
            <a href="./?pg=Products" class="btn btn-primary">Continue Shopping</a>
        </div>
    </div>
</cfoutput>