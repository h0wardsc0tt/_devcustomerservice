 <div id="deleteItemRow" class="modal fade" role="dialog"  data-backdrop="static" >
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 id="delete_title" class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <p id="delete_message" align="center" style="height:100px;">
        	Are you sure you want to delete this line item.
        </p>
        <p id='delete_error' style="display:none;">
           <div id="delete_error_messages" style="text-align:center; width:100%;height:0;" class="recovery-content-row expandable-element error-message"> 
                <div id="delete_error_message" class="system-messages" style="color:#EB1014">ddddddddddddddddddddddddddddd</div>
            </div>            
        </p>
      </div>
      <div class="modal-footer">
        <button id="deleteBtn" type="button" class="btn btn-primary" value="Submit" >OK</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
