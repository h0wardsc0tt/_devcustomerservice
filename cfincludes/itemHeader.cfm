<div class="row">
    <div class="line-item-header" style="display:inline-block;width:73%; border-right: solid 1px #000000;">
    REQUESTOR
    </div>
    <div class="line-item-header" style="display:inline-block;width:25%;">
    PURCHASING
    </div>
</div>
<div class="row" style="background-color:#ecf8fc; color:#4a889f; border:solid 1px #000000;border-bottom:2px solid #c9e3f2;">
    <div class="line-item-col-header" style="display:inline-block;width:11%; border:0;">
    LINE ITEM
    </div>
    <div class="line-item-col-header" style="display:inline-block;width:5%; border:0;">
    QTY 
    </div>
    <div class="line-item-col-header" style="display:inline-block;width:10%; border:0;">
    UOM 
    </div>
    <div class="line-item-col-header" style="display:inline-block;width:10%; border:0;">
    EST. COST
    </div>
    <div class="line-item-col-header"  style="display:inline-block;width:10%; border:0;">
    NEED DATE
    </div>
    <div class="line-item-col-header"  style="display:inline-block;width:25%; border:0;">
    MANUFACTURE PN-DESCRIPTION<br />(Avante PN where applicable)
    </div>
    <div class="line-item-col-header" style="display:inline-block;width:5%; border:0;">
    QTY 
    </div>
    <div class="line-item-col-header"  style="display:inline-block;width:10%; border:0;">
    PRICE 
    </div>
    <div class="line-item-col-header"  style="display:inline-block;width:10%; border:0;">
    DOCK DATE
    </div>
</div>