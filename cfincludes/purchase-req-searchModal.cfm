 <div id="searchModal" class="modal fade" role="dialog"  data-backdrop="static" >
<div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 id="imageTitle" class="modal-title"></h4>
        </div>
        <div class="modal-body">
            <p align="center" >
               Search By:
            </p>
            <p align="center" >

                <label class="radio-inline searchBy-radio"><input name="searchBy" type="radio" value="PartNumber">Part Number</label>
                <label class="radio-inline searchBy-radio"><input name="searchBy" type="radio" value="PurchaseRequisition">Purchase Requisition</label>
                <label class="radio-inline searchBy-radio"><input name="searchBy" type="radio" value="Requester">Requester</label>
                <label class="radio-inline searchBy-radio"><input name="searchBy" type="radio" value="Vendor">Vendor</label>
                <label class="radio-inline searchBy-radio"><input name="searchBy" type="radio" value="Status">Status</label>
            </p>
            <div align="center" >
                <div class="search" style="width:50%;">
                 <input type="search" id="purchase_req_search" name="Search" class="form-control" placeholder="Search..." >
                <span class="fa fa-search" onClick="search_for_purchase_req();"></span>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <button id="deleteBtn" type="button" class="btn btn-primary" value="Submit" onClick="search_for_purchase_req();" >Search</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
    </div>
  </div>
</div>
