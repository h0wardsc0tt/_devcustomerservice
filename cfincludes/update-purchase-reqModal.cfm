 <div id="update_Purchase_Requisition" class="modal fade" role="dialog"  data-backdrop="static" >
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" onclick="update_spinner.stop();" data-dismiss="modal">&times;</button>
         <h4 id="update_Title" class="modal-title"></h4>
      </div>
      <div class="modal-body" style="text-align:center;">
        <p id='update_Purchase_Requisition_spinner' style="height:100px;">
           <div id="update_Purchase_Requisition_messages" style="text-align:center; width:100%;height:0;" class="recovery-content-row expandable-element error-message"> 
                <div id="update_Purchase_Requisition_message" class="system-messages" style="color:#EB1014"></div>
            </div>            
        </p>
       </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
