<cfscript> 
function get_requested_pr(prId) { 
  	object = CreateObject("component", "_cfcs.productReq"); 
	serializer = new lib.JsonSerializer();
	
	pr_Struct =  StructNew();
	pr_Struct['id'] = '31';
	pr_Struct['type'] = 'order';
	
	li_Struct =  StructNew();
	li_Struct['type'] = 'lineItem';
		
	returnSelectResult = object.PurchaseRequisitionSelect(
		argumentCollection=pr_Struct
	);
	
	json = serializer.serialize(returnSelectResult);
	
	returnSelectResultObj = DeserializeJSON(json);
	order = returnSelectResultObj[1];
	
	key = order.key;
	li_Struct['id'] = key;
	
	returnLineItems = object.PurchaseRequisitionSelect(
		argumentCollection=li_Struct
	);
	
	json = serializer.serialize(returnLineItems);
	returnlineItemsObj = DeserializeJSON(json);
	
	StructDelete(order,"key");
	
	purchase_req = '';
	purchase_req = purchase_req & '{"key":"","status":"", "order_change":"", "line_item_change":"",';
	purchase_req = purchase_req & '"order":[]';
	purchase_req = purchase_req & ',"line_items":[]}';
	
	purchase_reqObj = DeserializeJSON(purchase_req);
	purchase_reqObj.key = key;
	
	line_item = '{"id":"", "status":"", "key":"", "line_item":[]}';
	line_itemObj = DeserializeJSON(line_item);
	
	
	for(i = 1; i LTE ArrayLen(returnlineItemsObj); i++){
		line_itemObj = DeserializeJSON(line_item);
		line_itemObj.key = returnlineItemsObj[i].id;
		line_itemObj.id = 'itemRow_' & i;
		StructDelete(returnlineItemsObj[i],"key");
		StructDelete(returnlineItemsObj[i],"id");
		arrayAppend(line_itemObj.line_item, returnlineItemsObj[i], true);
		arrayAppend(purchase_reqObj.line_items, line_itemObj, true);
	 }
	
	arrayAppend(purchase_reqObj.order, order, true);

	return replace(serializer.serialize(purchase_reqObj),"\n","\\n","all");

}  
</cfscript> 
