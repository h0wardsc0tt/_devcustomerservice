<cfparam name="FORM.Cust_Number_Anchor" default="">

<cfoutput>
	<cfif URL.st NEQ "Print">
        <div class="col-lg-8">
            <h2>Invoice Details</h2>
        </div>
    
        <div class="col-lg-4 text-right">
            <div class="cust_tag_cont">
                <div class="col-lg-2"></div>
                <!---<div class="col-lg-5">
                    <input type="button" name="INV_Email" id="INV_Email" class="btn btn-primary form-control" value="Email Invoice" />
                </div>--->
                <div class="col-lg-5">
                    <a href="javascript:void(0);" name="pay-invoice" id="pay-invoice" class="btn btn-success form-control inv-single" data-toggle="modal" <cfif qry_getInvoiceDetails.INVOICEBALANCE EQ "0.00">disabled="disabled"<cfelse>data-target="##payModal"</cfif>>Pay Invoice</a>
                    <input type="hidden" name="Cust_Number_Anchor" id="Cust_Number_Anchor" value="" />
                    <input type="hidden" name="SO_Number_Anchor" id="SO_Number_Anchor" value="" />
                    <input type="hidden" name="INV_Company" id="INV_Company" value="#qry_getInvoiceDetails.DATAAREAID#" />
                </div>
                <div class="col-lg-5">
                    <a href="javascript:void(0);" name="INV_Print" id="INV_Print" class="btn btn-primary form-control" onClick="printInvoice();">Email Invoice</a>
                    <!---<a href="./?pg=Invoices&st=Print&inv=INV000002" name="INV_Print" id="INV_Print" class="btn btn-primary form-control">Print Invoice</a>--->
                </div>
            </div>
        </div>
    <cfelse>
    	<div class="col-lg-12">
            <h2>Invoice Details</h2>
        </div>
    </cfif>
    
    <cfif URL.st NEQ "Print">
		<cfif ERR.ErrorFound>
            <div class="col-lg-12">
                <div class="form-message form-error">
                    <ul>
                        <cfloop list="#ERR.ErrorMessage#" index="thisError"><li>#thisError#</li></cfloop>
                    </ul>
                </div>
            </div>
        </cfif>
    </cfif>
    
    <div class="col-lg-12 inv-detail-cont1">
    	<div class="col-lg-8">
        	<div class="col-lg-2 inv-detail-col">
            	<p>
                	<strong>
                        Customer No:<br />
                        Order No: <br />
                        Order Date: <br />
                        Order Status: <br />
                        Purchase Order: <br />
                        Contact: <br />
                        Phone: 
                    </strong>
                </p>
            </div>
            
            <div class="col-lg-10">
            	<p>
					<span class="cust-id-view">#qry_getInvoiceDetails.CUSTOMERID#</span> <br />
                    #qry_getInvoiceDetails.SALESORDERNUMBER# <br />
                    #DateFormat(qry_getInvoiceDetails.SALESORDERDATE, "yyyy-mm-dd")# <br />
                    #qry_getInvoiceDetails.SALESSTATUSLABEL# <br />
                    #qry_getInvoiceDetails.PURCHASEORDERNUMBER# <br />
                    #qry_getInvoiceDetails.CONTACTNAME# <br />
                    #qry_getInvoiceDetails.CONTACTPHONE#
                </p>
            </div>
        </div>
        <div class="col-lg-4">
        	<h5>Bill To</h5>
            <p class="inv-address">
            	#qry_getBillToAddress.CUSTOMERNAME# <br />
                #qry_getBillToAddress.CONTACTNAME# <br />
                #qry_getBillToAddress.ADDRESS# <br />
                #qry_getBillToAddress.ADDRESSCITY#, #qry_getBillToAddress.ADDRESSSTATE# #qry_getBillToAddress.ADDRESSZIPCODE#
            </p>
        </div>
    </div>
    
    <div class="col-lg-12 inv-detail-cont2">
    	<div class="col-lg-8">
        	<h5>Sold To</h5>
            <p class="inv-address">
            	#qry_getSoldToAddress.CUSTOMERNAME# <br />
                #qry_getSoldToAddress.CONTACTNAME# <br />
                #qry_getSoldToAddress.ADDRESS# <br />
                #qry_getSoldToAddress.ADDRESSCITY#, #qry_getSoldToAddress.ADDRESSSTATE# #qry_getSoldToAddress.ADDRESSZIPCODE#
            </p>
        </div>
        <div class="col-lg-4">
        	<h5>Ship To</h5>
            <p class="inv-address">
            	#qry_getShipToAddress.CUSTOMERNAME# <br />
                #qry_getShipToAddress.CONTACTNAME# <br />
                #qry_getShipToAddress.ADDRESS# <br />
                #qry_getShipToAddress.ADDRESSCITY#, #qry_getShipToAddress.ADDRESSSTATE# #qry_getShipToAddress.ADDRESSZIPCODE#
            </p>
        </div>
    </div>
    
</cfoutput>

<div class="col-lg-12">&nbsp;</div>

<div class="col-lg-12 table-responsive">
    <table id="INV_Results" class="table" style="white-space:nowrap;">
        <thead id="INV_Results_Header">
            <tr>
                <th>LINE</th>
                <th>PART NUMBER</th>
                <th>DESCRIPTION</th>
                <th>QUANTITY</th>
                <th>PRICE</th>
                <th>DISCOUNT</th>
                <th>EXTENDED</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <cfoutput query="qry_getInvoiceLines">
                <tr class="inv_lines_row">
                    <td>#NumberFormat(Int(LINENUM),"000")#</td>
                    <td>#PARTNUMBER#</td>
                    <td>#DESCRIPTION#</td>
                    <td>#Int(QUANTITY)#</td>
                    <td>#DollarFormat(UNITPRICE)#</td>
                    <td>#DollarFormat(DISCAMOUNT)#</td>
                    <td>#DollarFormat(TOTALPRICE)#</td>
                    <td></td>
                </tr>
            </cfoutput>
            <cfoutput>
                <tr class="blank_row blank_row_first">
                    <td colspan="6" class="text-right">Tax</td>
                    <td>#DollarFormat(qry_getInvoiceDetails.SALESTAXES)#</td>
                </tr>
                <tr class="blank_row">
                    <td colspan="6" class="text-right">Freight</td>
                    <td>#DollarFormat(qry_getInvoiceDetails.SALESFREIGHT)#</td>
                </tr>
                <tr class="blank_row">
                    <td colspan="6" class="text-right">Invoice Amount</td>
                    <td>#DollarFormat(qry_getInvoiceDetails.INVOICEAMOUNT)#</td>
                </tr>
                <tr class="blank_row">
                    <td colspan="6" class="text-right">Paid Amount</td>
                    <td>#DollarFormat(qry_getInvoiceDetails.INVOICEPAYMENTS)#</td>
                </tr>
                <tr class="blank_row">
                    <td colspan="6" class="text-right">Open Amount</td>
                    <td class="total-amt">#DollarFormat(qry_getInvoiceDetails.INVOICEBALANCE)#</td>
                </tr>
            </cfoutput>
        </tbody>
    </table>
    
    <div class="col-lg-12 text-center">
        <ul class="pagination"></ul>
    </div>
</div>

<cfif URL.st NEQ "Print">
    <!-- Invoice Print Modal -->
    <div id="invoicePrint" class="modal fade" role="dialog">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <!-- Payment Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"><span class="glyphicon glyphicon-info-sign"></span> Invoice Print Notice</h4>
                    </div>
                    <div class="modal-body text-center">
                        <div class="alert alert-info">
                            The requested Invoice has been sent to <span class="user_email"><strong><cfoutput>#SESSION.User_EmailAddress#</cfoutput></strong></span>
                            <span class="invoice_frame"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</cfif>

<cfset paymentType = "Invoice">
<cfinclude template="./_tmp_payment_form.cfm">
