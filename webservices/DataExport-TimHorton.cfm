<cfparam type="string" name="URL.StoreNumber" default="">
<cfparam type="string" name="URL.StartDate" default="#DateAdd('n', -20, Now())#">
<cfparam type="string" name="URL.EndDate" default="#Now()#">
<cfparam type="numeric" name="URL.BrandID" default="21">	<!--- Tim Horton --->

<cfscript>
	objCSV = CreateObject("component", "_cfcs.utilities.csvexport");
	delimiter = Chr(9);
	header = 'EventID,PickupEndDate,Lane,GreetStart,OrderStart,OrderEnd,PickupStart,PickupEnd';
	filePath = ExpandPath('/download/datafeed');
	//urlPath = 'https://affeed.frameworks.ca/affeed/receive';
	port = '443';
	urlPath = 'https://dev.hmedtcloud.com/download';
</cfscript>

<cfquery name="qry_getStoreNumber" datasource="#APPLICATION.ods_Datasource#">
    SELECT	Store_Number
    FROM	[dbo].[tbl_Stores] WITH (NOLOCK)
    WHERE 	[Store_Brand_ID] = #URL.BrandID#
    <cfif URL.StoreNumber NEQ "">
    AND		Store_Number = '#URL.StoreNumber#'
    </cfif>
</cfquery>

<cfloop query="qry_getStoreNumber">
	<cfset StoreNumber = qry_getStoreNumber.Store_Number>
    <cfset fileName = 'drive-service_v2_#StoreNumber#_#DateFormat(Now(), "yyyymmdd")#.txt'>
    <cfset file = filePath & "\" & fileName>

	<!--- get Tim Horton data from the store proc --->
    <cftry>
        <cfquery name="qry_getCarData" datasource="#APPLICATION.ods_Datasource#">
            EXEC dbo.usp_HME_Cloud_Get_Car_Data_By_Store '#StoreNumber#', '#URL.StartDate#', '#URL.EndDate#', #URL.BrandID#
        </cfquery>
    
        <cfcatch type="database"></cfcatch>
    </cftry>
            
    <!--- convert query result to a tab delimited file --->
    <cfset result = objCSV.QueryToCSV(qry_getCarData, header, 1, delimiter) />
    
    <!--- same the file to a temp loaction --->
    <cffile action="write" file="#file#" output="#result#" nameconflict="overwrite" />
    
    <!--- push the file to Tim Horton's server --->
    <cfhttp method="put" url="#urlPath#" port="#port#" throwonerror="Yes" result="rtnValue"> 
        <cfhttpparam type="file" mimetype="text/html;charset=ISO-8859-1" file="#file#" name="data" >
    </cfhttp> 
    
    <!--- remove the file from cf server --->
    <!--- <cffile action="delete" file="#file#" /> --->
</cfloop>