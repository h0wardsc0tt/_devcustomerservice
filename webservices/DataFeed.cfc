<cfcomponent hint="Export client's data">
	<cfscript>
		//this.init(APPLICATION.ods_datasource);
		this.init('db_qsrdrivethrucloud_ods_live');
	</cfscript>

	<cffunction name="init" access="public" output="false" returntype="any">
    	<cfargument type="String" name="dsn" required="false" default="">
        
		<cfscript>
			variables.dsn    = arguments.dsn;
			variables.objTZ  = CreateObject("component", "_cfcs.utilities.timezone").init(variables.dsn);
			variables.objCSV = CreateObject("component", "_cfcs.utilities.csvexport");
			
			return this;
		</cfscript>
	</cffunction>
    

	<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="GetDatafeed" output="no" returntype="string" access="remote" hint="return user data in csv format">
    	<cfargument type="string" name="df_token" required="yes">
    	<cfargument type="string" name="StoreNumber" required="no" default="">
        <cfargument type="string" name="StartDate" required="no" default="">
        <cfargument type="string" name="EndDate" required="no" default="">
        
        <cfscript>
			// Check authorization for the request call
    		CheckAuthentication(Arguments.df_token);
			
			// atuthentication passed, now get this datafeed meta data
			var metadata = GetDatafeedMetaData(Arguments.df_token, Arguments.StoreNumber);
			var delimiter = metadata.datafeed_delimiter;
			var storedproc_name = metadata.datafeed_DAO;
			var StoreIDs = ValueList(metadata.store_id);
			var timezoneID = metadata.datafeed_timezone_id;
			
			var timeToDelay = -30;	//off set end time by 30 min so that end use won't get real time data
			var LocalCurrentDateTime = variables.objTZ.getClientTimeByID(timezoneID);
			
			var StartDate = Arguments.StartDate;
			var EndDate   = Arguments.EndDate;
			
			var fixedStartDate = '';
			var fixedEndDate = '';
			
			if (StartDate IS '' OR NOT IsDate(StartDate)){
				if (metadata.datafeed_starttime Is '')
					fixedStartDate = LocalCurrentDateTime;
				else
					fixedStartDate = DateFormat(LocalCurrentDateTime, 'yyyy-mm-dd') & ' ' & metadata.datafeed_starttime;
					
				StartDate = DateAdd('n', -metadata.datafeed_startdate_offset, fixedStartDate);
			}
			
			if (EndDate IS '' OR NOT IsDate(EndDate)){
				if (metadata.datafeed_endtime Is '')
					fixedEndDate = DateAdd('n', timeToDelay, LocalCurrentDateTime);
				else
					fixedEndDate = DateFormat(LocalCurrentDateTime, 'yyyy-mm-dd') & ' ' & metadata.datafeed_endtime;
					
				EndDate = DateAdd('n', -metadata.datafeed_enddate_offset, fixedEndDate);
			}	// make sure end user can't get real time data
			else if (DateDiff('n', Arguments.EndDate, LocalCurrentDateTime) LTE 30)
				EndDate = DateAdd('n', timeToDelay, LocalCurrentDateTime);
			

			var qry_getCustomerData = '';
			var header = '';
			var result = '';
			
			var args = {
				datafeed_token = Arguments.df_token,
				datafeed_id = metadata.datafeed_id,
				store_id = StoreIDs,
				datafeed_startdate = StartDate,
				datafeed_enddate = EndDate
			};
    	</cfscript>
        
    	<cftry>
        	<cfstoredproc datasource="#variables.dsn#" procedure="#storedproc_name#">
                <cfprocparam cfsqltype="cf_sql_varchar" dbvarname="Store_IDs" value="#StoreIDs#">
                <cfprocparam cfsqltype="cf_sql_varchar" dbvarname="StartDateTime" value="#DateFormat(StartDate, 'yyyy-mm-dd')# #TimeFormat(StartDate, 'HH:mm:ss')#" null="#IIF(StartDate IS '', 1, 0)#">
                <cfprocparam cfsqltype="cf_sql_varchar" dbvarname="EndDateTime" value="#DateFormat(EndDate, 'yyyy-mm-dd')# #TimeFormat(EndDate, 'HH:mm:ss')#" null="#IIF(EndDate IS '', 1, 0)#">
                <cfprocresult name="qry_getCustomerData">
            </cfstoredproc>
        
            <cfcatch type="database">
            	<!--- write error log --->
            	<cfset args.datafeed_status_code = 500 />
                <cfset args.datafeed_note = 'Database server error.' />
                <cfset CreateDatafeedLog(argumentCollection = args) />
            
            	<!--- Set status code. --->
	            <cfheader statuscode="500" statustext="Database server error. Please try again later."/>
                <cfabort />
            </cfcatch>
        </cftry>

        <cfscript>
			header = qry_getCustomerData.ColumnList;
			result = variables.objCSV.QueryToCSV(qry_getCustomerData, header, 1, Evaluate(delimiter));
			
			args.datafeed_recordcount = qry_getCustomerData.RecordCount;
			args.datafeed_status_code = 200;
			args.datafeed_note = 'Datafeed completed successfully.';
			
			// log each datafeed transaction
			CreateDatafeedLog(argumentCollection = args);
		</cfscript>
        
        <!--- Set status code. --->
        <cfheader statuscode="200" statustext="OK"/><!---2016-03-10 ATN: Added CFFLUSH to push results out--->
		<cfflush>
		<cfreturn result />
    </cffunction>
    
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="CheckAuthentication" output="no" returntype="void" access="private" hint="check to see if the request is authenticated">
    	<cfargument type="string" required="yes" name="datafeed_token">
        
    	<cfif NOT CheckAuthorization(Arguments.datafeed_token)>
        	<!--- write error log --->
			<cfset args = {datafeed_token = Arguments.datafeed_token, datafeed_status_code = 401, datafeed_note = 'Unauthorized. Incorrect token.'} />
            <cfset CreateDatafeedLog(argumentCollection = args) />
            
            <!--- Set status code. --->
            <cfheader statuscode="401" statustext="Unauthorized"/>
 
            <!--- Set authorization header. --->
            <!--- <cfheader name="WWW-Authenticate" value="basic realm=""API"""/> --->
 
            <!--- Stop the page from loading. --->
            <cfabort />
        </cfif>
 
        <!--- Return out. --->
        <cfreturn />
    </cffunction>
    
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="CheckAuthorization" output="no" returntype="boolean" access="private" hint="verify each request by the token it provides">
    	<cfargument type="string" required="yes" name="datafeed_token">
    	
        <cfscript>
			var qry_verifyToken = '';
    	</cfscript>
        
        <cftry>
            <cfquery name="qry_verifyToken" datasource="#variables.dsn#">
                SELECT	datafeed_id 
                FROM	Metadata.Datafeed WITH (NOLOCK) 
                WHERE	datafeed_token = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.datafeed_token#">
            </cfquery>
			
            <cfcatch type="any"><cfreturn false /></cfcatch>
        </cftry>
        
        <cfif qry_verifyToken.RecordCount EQ 0>
        	<cfreturn false />
        <cfelse>
        	<cfreturn true />
        </cfif> 
    </cffunction>
    
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="GetDatafeedMetaData" output="no" returntype="query" access="private" hint="return datafeed meta data">
    	<cfargument type="string" required="yes" name="datafeed_token">
        <cfargument type="string" required="no" name="StoreNumber" default="">
    	
        <cfset var qry_getMetaData = '' />
        
    	<cfquery name="qry_getMetaData" datasource="#variables.dsn#">
        	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
			SET NOCOUNT ON

            SELECT	df.datafeed_id,
            		df.datafeed_brand_id, 
            		df.datafeed_DAO, 
                    df.datafeed_delimiter, 
                    df.datafeed_startdate_offset,
                    df.datafeed_enddate_offset,
                    df.datafeed_starttime,
                    df.datafeed_endtime,
                    df.datafeed_timezone_id,
                    sd.store_id
            FROM	Metadata.Datafeed df
            		INNER JOIN Metadata.Store_Datafeed sd ON sd.datafeed_id = df.datafeed_id
            WHERE	df.datafeed_token = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.datafeed_token#">
            <cfif Arguments.StoreNumber NEQ "">
	            AND	EXISTS(SELECT 1 FROM dbo.tbl_Stores s WHERE s.Store_ID = sd.store_id AND s.Store_Number = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.StoreNumber#">)
            </cfif>
        </cfquery>
        
        <cfif qry_getMetaData.RecordCount EQ 0>
        	<!--- write error log --->
			<cfset args = {datafeed_token = Arguments.datafeed_token, datafeed_status_code = 400, datafeed_note = 'Bad request. Store number "#Arguments.StoreNumber#" could not be found.'} />
            <cfset CreateDatafeedLog(argumentCollection = args) />
            
            <!--- Set status code. --->
            <cfheader statuscode="400" statustext="Bad request. Store number '#Arguments.StoreNumber#' could not be found."/>
            <!--- Stop the page from loading. --->
            <cfabort />
        	<cfreturn />
        </cfif>
        
        <cfreturn qry_getMetaData />
    </cffunction>
    
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="CreateDatafeedLog" output="no" returntype="void" access="private" hint="add a datafeed log entry">
    	<cfargument type="string" required="yes" name="datafeed_token">
        <cfargument type="numeric" required="no" name="datafeed_id" default="0">
        <cfargument type="string" required="no" name="store_id" default="">
        <cfargument type="string" required="no" name="datafeed_startdate" default="">
        <cfargument type="string" required="no" name="datafeed_enddate" default="">
        <cfargument type="numeric" required="no" name="datafeed_recordcount" default="0">
        <cfargument type="numeric" required="no" name="datafeed_status_code" default="">
        <cfargument type="string" required="no" name="datafeed_note" default="">
        
        <cftry>
            <cfquery datasource="#variables.dsn#">
                INSERT INTO Metadata.Datafeed_Log(
                        datafeed_token,
                        datafeed_id,
                        store_id,
                        datafeed_startdate,
                        datafeed_enddate,
                        datafeed_recordcount,
                        datafeed_status_code,
                        datafeed_note
                )
                SELECT	<cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.datafeed_token#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.datafeed_id#" null="#IIF(Arguments.datafeed_id EQ 0, 1, 0)#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.store_id#" null="#IIF(Arguments.store_id IS '', 1, 0)#">,
                        <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Arguments.datafeed_startdate#" null="#IIF(Arguments.datafeed_startdate IS '', 1, 0)#">,
                        <cfqueryparam cfsqltype="cf_sql_timestamp" value="#Arguments.datafeed_enddate#" null="#IIF(Arguments.datafeed_enddate IS '', 1, 0)#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.datafeed_recordcount#" null="#IIF(Arguments.datafeed_recordcount EQ 0, 1, 0)#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#Arguments.datafeed_status_code#" null="#IIF(Arguments.datafeed_status_code IS '', 1, 0)#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.datafeed_note#" null="#IIF(Arguments.datafeed_note IS '', 1, 0)#">
            </cfquery>
            
            <cfcatch type="any"></cfcatch>
        </cftry>
        
        <cfreturn />
    </cffunction>
</cfcomponent>