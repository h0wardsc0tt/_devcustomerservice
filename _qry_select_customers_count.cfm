<cfparam name="QUERY" default="#StructNew()#">
<cfparam name="QUERY.QueryName" default="qry_getAllCustomers">
<cfparam name="QUERY.Datasource" default="#APPLICATION.Datasource_AX#">
<cfparam name="QUERY.SelectSQL" default="cust.*">
<cfparam name="QUERY.OrderBy" default="cust.CUSTOMERID">

<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#" maxrows="1000">
	SELECT #QUERY.SelectSQL#
	FROM 
		HMECPCUSTOMERLOOKUP cust
	WHERE 0=0
	<cfif StructKeyExists(QUERY,"CUSTOMERID")>
	AND cust.CUSTOMERID LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.CUSTOMERID#%">
	</cfif>
    <cfif StructKeyExists(QUERY,"CUSTOMERNAME")>
	AND cust.CUSTOMERNAME LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.CUSTOMERNAME#%">
	</cfif>
    <cfif StructKeyExists(QUERY,"ADDRESSCITY")>
	AND cust.ADDRESSCITY LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.ADDRESSCITY#%">
	</cfif>
    <cfif StructKeyExists(QUERY,"ADDRESSSTATE")>
	AND cust.ADDRESSSTATE LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.ADDRESSSTATE#%">
	</cfif>
    <cfif StructKeyExists(QUERY,"ADDRESSZIPCODE")>
	AND cust.ADDRESSZIPCODE LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.ADDRESSZIPCODE#%">
	</cfif>
    <cfif StructKeyExists(QUERY,"SALESORDERNUMBER")>
	AND olup.SALESORDERNUMBER LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.SALESORDERNUMBER#%">
	</cfif>
    <cfif StructKeyExists(QUERY,"INVOICENUMBER")>
	AND ilup.INVOICENUMBER LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.INVOICENUMBER#%">
	</cfif>
    <cfif StructKeyExists(QUERY,"STORENUMBER")>
	AND cust.STORENUMBER LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.STORENUMBER#%">
	</cfif>
    <cfif StructKeyExists(QUERY,"CONTACTEMAIL")>
	AND cust.CONTACTEMAIL LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.CONTACTEMAIL#%">
	</cfif>
</cfquery>

<cfquery name="#QUERY.QueryName#_Total" dbtype="query">
	SELECT Count(*) AS Records
	FROM #QUERY.QueryName#
</cfquery>
<cfif StructClear(QUERY)></cfif>