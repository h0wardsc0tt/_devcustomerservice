<cfparam name="FORM.CSP_Page_Form" default="">
<cfparam name="FORM.CSP_Per_Page" default="5">
<cfparam name="FORM.CSP_Current_Page" default="1">
<cfparam name="VARIABLES.Page_Start" default="1">
<cfparam name="VARIABLES.Page_End" default="5">

<cfparam name="FORM.Cust_Number_Anchor" default="">
<cfparam name="FORM.SO_Number_Anchor" default="">

<cfparam name="FORM.SO_Number" default="">
<cfparam name="FORM.PO_Number" default="">
<cfparam name="FORM.Date_From" default="">
<cfparam name="FORM.INV_Number" default="">
<cfparam name="FORM.Part_Number" default="">
<cfparam name="FORM.Date_To" default="">
<cfparam name="FORM.INV_Company" default="hsc">
<cfparam name="FORM.Cust_Number" default="#FORM.Cust_Number_Anchor#">

<cfif LEN(FORM.Cust_Number) NEQ 0>
	<cfset FORM.Cust_Number_Anchor = FORM.Cust_Number>
</cfif>

<cfscript>
	OrderInfoObj = CreateObject("component", "_cfcs.OrderInfo");
	OrdersCount = DeserializeJSON(OrderInfoObj.GetOrdersCount(SESSION.User_UID,FORM.SO_Number,FORM.PO_Number,FORM.Date_From,FORM.INV_Number,FORM.Part_Number,FORM.Date_To,FORM.INV_Company,FORM.Cust_Number));
	OrdersList = DeserializeJSON(OrderInfoObj.GetOrdersList(SESSION.User_UID,FORM.CSP_Current_Page,FORM.CSP_Per_Page,FORM.SO_Number,FORM.PO_Number,FORM.Date_From,FORM.INV_Number,FORM.Part_Number,FORM.Date_To,FORM.INV_Company,FORM.Cust_Number));
	
	VARIABLES.Page_Start = ((FORM.CSP_Current_Page-1)*FORM.CSP_Per_Page)+1;
	VARIABLES.Page_End = FORM.CSP_Current_Page*FORM.CSP_Per_Page;
</cfscript>