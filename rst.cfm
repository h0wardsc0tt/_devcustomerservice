<cfsetting showdebugoutput="yes">

<!---<cfquery name="test" datasource="db_admin">
	SELECT CarData_XML
    FROM CarData
    WHERE CarData_ID = 1
</cfquery>

<cfoutput>#test.CarData_XML#</cfoutput><cfabort>--->

<cfparam name="FORM.send_RCD" default="">
<cfparam name="VARIABLES.df_token" default="FC129747-C3B8-475D-83C5-466D491E68AF">
<cfparam name="VARIABLES.duid" default="DA4D89A3-2183-4A20-BE61-96D83AC48DC3">
<cfset xmlPath = ExpandPath('.') & "\data\">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ZOOM</title>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<style>
	body {padding:25px;}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<body>
<form action="" method="post">
	<button type="submit" name="send_RCD" id="send_RCD" class="btn btn-primary" value="Authorize">Get Authorization</button>
</form>

<cfif IsDefined("FORM.send_RCD") AND Len(FORM.send_RCD) NEQ 0>
    <cfoutput>
    
    	<cffile action="read" file="#xmlPath#RCD.xml" variable="textXML">
        
        <!---<Cfdump var="#ToString(textXML)#"><Cfabort>--->
    	
        <!---<cfhttp url="http://dev.hmedtcloud.com/_cfcs/GetCarData.cfc" method="POST" redirect="no" result="isAuth" charset="utf-8">
        <cfhttp url="http://devhmewireless.hme.com/_cfcs/rst.cfc" method="POST" redirect="no" result="isAuth" charset="utf-8">--->
        <cfhttp url="http://devhme.hme.com/webservices/GetCarData.cfc" method="POST" redirect="no" result="isAuth" charset="utf-8">
        	<cfhttpparam name="method" type="formfield" value="GetAuthorized">
        	<cfhttpparam name="df_token" type="formfield" value="#VARIABLES.df_token#">
            <cfhttpparam name="duid" type="formfield" value="#VARIABLES.duid#">
            <cfhttpparam name="xmlData" type="formfield" value="#ToString(textXML)#">
        	<!---<cfhttpparam name="xmlData" file="#xmlPath#RCD.xml" type="file">--->
        </cfhttp>
        
        <br />
        
        <div class="alert alert-success col-lg-5">
            <strong>Authorization Successful!</strong> Your data will now be sent to HME CLOUD.
        </div>

		<!---#isAuth.FileContent#
        
        <cfwddx action="wddx2cfml" input="#isAuth.Filecontent#" output="status_code">
        
        #status_code#--->
        
		<!---<cfif status_code EQ 1>
        	<div class="alert alert-success col-lg-5">
            	<strong>Authorization Successful!</strong> Your data will now be sent to HME CLOUD.
            </div>
        <cfelse> 
        	<div class="alert alert-danger col-lg-5">
            	Authorization Failed...
            </div>
        </cfif>--->

    </cfoutput>
</cfif>

</body>
</html>