<cfcomponent
	displayname="Application"
	output="true"
	hint="Handle the application.">

	<!--- Set up the application. --->
	<cfscript>
		THIS.Name = Hash(getCurrentTemplatePath());
		THIS.ApplicationTimeout = createTimeSpan(0,1,0,0);
		THIS.SessionTimeout = createTimeSpan(0,0,25,0);
		THIS.SessionManagement = true;
		THIS.SetClientCookies = true;
		THIS.ClientManagement = true;
		THIS.ClientStorage = "cookie";
	</cfscript>
	
	<!--- Define the page request properties. --->
	<cfsetting
		requesttimeout="2400"
		showdebugoutput="true"
		enablecfoutputonly="false"/>

	<cffunction
		name="OnApplicationStart"
		access="public"
		returntype="boolean"
		output="false"
		hint="Fires when the application is first created.">
		
		<cfscript>
			APPLICATION.Datasource = "AXWebPortal";
			APPLICATION.Datasource_AX = "AX2012_DEV";
			//Change vars based on environment
			APPLICATION.rootDir = getDirectoryFromPath(getCurrentTemplatePath());
			
			if(APPLICATION.rootDir CONTAINS "_dev") {
				env = "dev";
				APPLICATION.Datasource = "AXWebPortal";
				APPLICATION.Datasource_AX = "AX2012_DEV";
				APPLICATION.Datasource_powsql = "powsql";
			}
			if(APPLICATION.rootDir CONTAINS "_uat") {
				env = "uat";
				APPLICATION.Datasource = "AXWebPortal";
				APPLICATION.Datasource_AX = "AX2012_TEST";
			}
			if(APPLICATION.rootDir CONTAINS "_www") {
				env = "www";
				APPLICATION.Datasource = "AXWebPortal";
				APPLICATION.Datasource_AX = "AX2012_TEST";
			}
			
			APPLICATION.rootURL = "#env#customerservice.hme.com";
			APPLICATION.compDir = APPLICATION.rootDir & "_cfcs";
			APPLICATION.ApplicationTimeout = THIS.ApplicationTimeout;
			APPLICATION.SessionTimeout = THIS.SessionTimeout;
			
			//PayPal Values
			APPLICATION.PayPal_PARTNER = "wfb";
			//HME Credentials
			APPLICATION.PayPal_VENDOR = "hme174223514";
			APPLICATION.PayPal_USER = "webSite";
			APPLICATION.PayPal_PASS = "HmeWfb97531";
			APPLICATION.HME_Email = "InvoiceReceipts@hme.com";
			//JTECH Credentials
			APPLICATION.JTECH_PayPal_VENDOR = "hme328202918";
			APPLICATION.JTECH_PayPal_USER = "website";
			APPLICATION.JTECH_PayPal_PASS = "HmeWfb97531";
			APPLICATION.JTECH_Email = "##_Invoice_Receipts_JTECH@hme.com";
			//CE Repairs Credentials
			APPLICATION.CE_PayPal_VENDOR = "hme174223514";
			APPLICATION.CE_PayPal_USER = "webSite";
			APPLICATION.CE_PayPal_PASS = "HmeWfb97531";
			APPLICATION.CE_Email = "InvoiceReceipts@hme.com";
			//Clear-Com Credentials
			APPLICATION.CC_PayPal_VENDOR = "hme174223514";
			APPLICATION.CC_PayPal_USER = "webSite";
			APPLICATION.CC_PayPal_PASS = "HmeWfb97531";
			APPLICATION.CC_Email = "InvoiceReceipts@hme.com";
			
			APPLICATION.PayPal_SIGN = "AghJC9P7dquxmrwVUyV6BnNg6dVlAB9BBrp2PzZjVnAJUNRWrkf7kXOq";
			//APPLICATION.PayPal_URL 	= "https://payflowpro.paypal.com";
			APPLICATION.PayPal_URL 	= "https://pilot-payflowpro.paypal.com";
			APPLICATION.PayPal_CURR = "USD";
			APPLICATION.PayPal_APIV = "92.0";
			APPLICATION.PayPal_useProxy = "false";
			APPLICATION.PayPal_proxyName = "";
			APPLICATION.PayPal_proxyPort = "";
			
			SESSION.loginAttempts = 0;
			SESSION.registerAttempts = 0;
		</cfscript>
        
		<!--- Return out. --->
		<cfreturn true />
	</cffunction>

	<cffunction
		name="OnRequestStart"
		access="public"
		output="true"
		hint="Fires pre page processing.">

		<cfscript>
			ERR = StructNew();
			ERR.ErrorFound = false;
			ERR.ErrorMessage = "";
			
			LOGIN = StructNew();
			LOGIN.maxAttempts = 5; 
			LOGIN.sessTimeout = CreateTimeSpan(0,0,25,0);
			
			SITE = StructNew();
			SITE.AdminEmail = "Web_Support@hme.com";
			SITE.ITRequest = "ITR@hme.com";
			SITE.SupportEmail = "cloudsupport@hme.com";
			SITE.MaintUser_IP = "192.168.106.232";
			SITE.IsMaintenance = false; //for putting site into maint mode
			SITE.IsDebug = false; //for putting the site into debug mode
			
			THIS.mappings = StructNew();
		</cfscript>
		
		<cfif (Left(GetFileFromPath(CGI.SCRIPT_NAME),1) IS "_")>
			<cfmail 
				to="Web_Support@hme.com" 
				from="no-reply@hme.com" 
				subject="Customer Service Portal: Direct Template Browsing Attempt" 
				type="html">
			#DateFormat(now(), "yyyy/mm/dd")# #TimeFormat(now(), "HH:mm:ss")#
			<cfdump var="#CGI#" label="CGI Vars">
			</cfmail>
			<cfabort>
		</cfif>
		
		<cfif StructKeyExists(URL, "reset")>
			<cfif StructClear(SESSION)></cfif>
			<cfset THIS.OnApplicationStart()>
		</cfif>
		
		<!--- Return out. --->
		<cfreturn />
	</cffunction>
	
	<cffunction
		name="OnRequest"
		access="public"
		returntype="void"
		output="true"
		hint="Processes the requested template.">
		<!--- Define arguments. --->
		<cfargument
			name="TargetPage"
			type="string"
			required="true"
			hint="The template being requested." default="#CGI.SCRIPT_NAME#"
			/>
            
		<cfset requestPage = ReReplaceNoCase(ARGUMENTS.TargetPage, "^/", "./")>
        
		<cfif(APPLICATION.rootDir CONTAINS "_uat") OR (APPLICATION.rootDir CONTAINS "_www")>
			<cfsetting showdebugoutput="no">
        <cfelse>
        	<cfsetting showdebugoutput="yes">
		</cfif>
        
		<cfif CGI.REMOTE_ADDR DOES NOT CONTAIN "10.10." AND SITE.IsMaintenance>
			<cfinclude template="/underconstruction.cfm">
		<cfelse>
			<!--- Include UDF --->
			<cfinclude template="/_udf_global.cfm">
			<!--- Include the requested template. --->
			<cfinclude template="#requestPage#" />
		</cfif>
		 
		<!--- Return out. --->
		<cfreturn />
	</cffunction>
	
	<cffunction name="onError">
		<!---The onError method gets two arguments:
		An exception structure, which is identical to a cfcatch variable.
		The name of the Application.cfc method, if any, in which the error
		happened.--->
		<cfargument name="except" required="yes" />
		<cfargument type="String" name="EventName" required="yes" />
		<!--- Log all errors in an application-specific log file. --->
		<cflog file="#This.Name#" type="error" text="Event Name: #EventName#" >
		<cflog file="#This.Name#" type="error" text="Message: #except.message#">
		<!--- Throw validation errors to ColdFusion for handling. --->
		<cfif Find("coldfusion.filter.FormValidationException", Arguments.Except.StackTrace)>
			<cfthrow object="#except#">
		<cfelse>
			<cfscript>
				ERR = StructNew();
				ERR.Error = except;
			</cfscript>
			<cfinclude template="./security/_tmp_error.cfm">
		</cfif>
	</cffunction>

	<cffunction name="onSessionEnd">
		<cfargument name = "SessionScope" required=true/>
		<cfargument name = "AppScope" required=true/>
		<!---Force Logout--->
		<cfset URL.pg = "Logout">
		<cfinclude template="./index.cfm">	
	</cffunction>

	<cffunction
		name="OnApplicationEnd"
		access="public"
		returntype="void"
		output="false"
		hint="Fires when the application is terminated.">
		
		<!--- Define arguments. --->
		<cfargument
			name="ApplicationScope"
			type="struct"
			required="false"
			default="#StructNew()#"/>
	</cffunction>
</cfcomponent>