<cfparam name="User_IsLoggedIn" default="false">
<cfparam name="User_IsAdmin" default="true">
<cfparam name="User_IsAtLogin" default="false">
<cfparam name="URL.pg" default="">
<cfparam name="URL.st" default="">

<cfsetting showdebugoutput="no">

<cfoutput>
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Invoice PDF</title>
            <meta name="Title" content="Invoice PDF">
            <meta name="Description" content="Invoice PDF">
            <meta name="Keywords" content="Invoice PDF">
            <meta name="format-detection" content="telephone=no">
            <link rel="stylesheet" href="./css/main-print.css?v=17">
        </head>
        <body>
            <div class="container pdf_nav">
                <div class="hme-logo"><img src="./images/HME-Logo-large.png" alt="HME" width="80" /></div>
                <div class="hme-title">Customer Service Portal</div>
            </div>
            <div class="container">
                    <div id="Content">
                        
                        <h2>Invoice Details</h2>
                        
                        <table>
                            <tr>
                                <td>Customer No:</td>
                                <td>165844 </td>
                            </tr>
                            <tr>
                                <td>Order No:</td>
                                <td>HMEC-000300 </td>
                            </tr>
                            <tr>
                                <td>Order Date:</td>
                                <td>2016-03-18 </td>
                            </tr>
                            <tr>
                                <td>Order Status:</td>
                                <td>Invoiced </td>
                            </tr>
                            <tr>
                                <td>Purchase Order:</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Contact:</td>
                                <td>Main </td>
                            </tr>
                            <tr>
                                <td>Phone:</td>
                                <td>800-848-4468</td>
                            </tr>
                        </table>
                        
                    </div>
            </div>
        </body>
    </html>
</cfoutput>