<cfparam name="QUERY" default="#StructNew()#">
<cfparam name="QUERY.QueryName" default="qry_getAll">
<cfparam name="QUERY.Datasource" default="#APPLICATION.Datasource#">
<cfparam name="QUERY.SelectSQL" default="usrs.*">
<cfparam name="QUERY.OrderBy" default="usrs.User_LastName,usrs.User_FirstName">

<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#" maxrows="1000">
	SELECT #QUERY.SelectSQL#
	FROM 
		ServicePortal.tbl_Users usrs
	WHERE 0=0
	<cfif StructKeyExists(QUERY,"User_ID")>
	AND usrs.User_ID LIKE <cfqueryparam cfsqltype="cf_sql_integer" value="%#QUERY.User_ID#%">
	</cfif>
	<cfif StructKeyExists(QUERY,"User_UID")>
	AND usrs.User_UID LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.User_UID#%">
	</cfif>
    <cfif StructKeyExists(QUERY,"User_Cust_ID")>
	AND usrs.User_Cust_ID LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.User_Cust_ID#%">
	</cfif>
    <cfif StructKeyExists(QUERY,"User_FirstName")>
	AND usrs.User_FirstName LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.User_FirstName#%">
	</cfif>
    <cfif StructKeyExists(QUERY,"User_LastName")>
	AND usrs.User_LastName LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.User_LastName#%">
	</cfif>
    <cfif StructKeyExists(QUERY,"User_EmailAddress")>
	AND usrs.User_EmailAddress LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.User_EmailAddress#%">
	</cfif>
    <cfif StructKeyExists(QUERY,"User_IsVerified")>
	AND usrs.User_IsVerified = <cfqueryparam cfsqltype="cf_sql_integer" value="#QUERY.User_IsVerified#">
	</cfif>
</cfquery>

<cfquery name="#QUERY.QueryName#_Total" dbtype="query">
	SELECT Count(*) AS Records
	FROM #QUERY.QueryName#
</cfquery>
<cfif StructClear(QUERY)></cfif>