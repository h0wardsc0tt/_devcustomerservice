<!--- Validate User Account Information --->
<cfsilent>
	<cfscript>
		if(Len(FORM.User_CompanyName) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Company Name may not be blank.");
        }
		if(Len(FORM.User_Address_Line1) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Company Address 1 may not be blank.");
        }
		if(Len(FORM.User_Locality) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Company City/Locality may not be blank.");
        }
		if(Len(FORM.User_Region) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Company State/Region may not be blank.");
        }
		if(Len(FORM.User_Country) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Company Country may not be blank.");
        }
		if(Len(FORM.User_PostCode) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Company Postal Code may not be blank.");
        }
        if(FORM.User_EmailAddress IS NOT "") {
            //Validate proper email
            if(ReFindNoCase("^([a-zA-Z0-9_\-\.\+])+@(([0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5])|((([a-zA-Z0-9\-])+\.)+([a-zA-Z\-])+))$", FORM.User_EmailAddress) EQ 0) {
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Email Address is invalid.");
            }
        } else {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Email Address is invalid.");
        }
		if(Len(FORM.User_FirstName) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "First Name may not be blank.");
        }
		if(Len(FORM.User_LastName) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Last Name may not be blank.");
        }
		if(Len(FORM.User_Phone_Number) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Phone Number may not be blank.");
        }
    </cfscript>
</cfsilent>

<!--- Check if Email Address is Available --->
<cfif Len(FORM.User_EmailAddress) NEQ 0>
    <cfquery name="qry_checkUserEmail" datasource="#APPLICATION.Datasource#" maxrows="1">
        SELECT 1
        FROM ServicePortal.tbl_Users
        WHERE User_EmailAddress = <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_EmailAddress#">
    </cfquery>
    
    <cfif qry_checkUserEmail.RecordCount NEQ 0>
		<cfif FORM.User_EmailAddress NEQ qry_getUserInfo.User_EmailAddress>
			<cfset ERR.ErrorFound = true>
            <cfset ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Email Address is already in use.")>
        </cfif>
    </cfif>
</cfif>

<!--- If Error, return to Form --->
<cfif ERR.ErrorFound>
	<cfinclude template="./_tmp_account_edit.cfm">
	<cfexit method="exittemplate">
</cfif>

<!--- If no Error, update User Account Information --->
<cfquery name="qry_UpdateUser" datasource="#APPLICATION.Datasource#">
	UPDATE [ServicePortal].[tbl_Users]
    SET	   [User_FirstName] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_FirstName#">,
		   [User_LastName] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_LastName#">,
		   [User_EmailAddress] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_EmailAddress#">,
		   [User_Address_Line1] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_Address_Line1#">,
		   [User_Address_Line2] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_Address_Line2#">,
		   [User_Phone_Number] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_Phone_Number#">,
		   [User_Locality] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_Locality#">,
		   [User_Region] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_Region#">,
		   [User_PostCode] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_PostCode#">,
		   [User_Country] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_Country#">,
		   [User_Company] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_Company#">,
		   [User_CompanyName] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_CompanyName#">,
           [User_Brand] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_Brand#">
	WHERE  [User_UID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_UID#">	
</cfquery>

<!--- Show Success Message, reutnr to form --->
<cfset SuccessMsg = "Your account changes have been saved.">
<cfinclude template="./_tmp_account_edit.cfm">
<cfexit method="exittemplate">
