<cfscript>
purchase_req = '';
purchase_req = purchase_req & '{"key":"","status":"", "order_change":"", "line_item_change":"",';


							purchase_req = purchase_req & '"order":';
							purchase_req = purchase_req & '[{"id":"supplier_edit","value":"SHI.com\\nChloe Frew\\n1501 S MoPac Expressway\\nSuite 400\\nAustin, TX 78746~Phone: 512-676-23156\\nFax: 512-676-2315\\nEmail: Chloe_Frew@shi.com", "status":"", "type":"string"},';
							purchase_req = purchase_req & '{"id":"shipping_edit","value":"SHI.com\\nChloe Frew\\n1501 S MoPac Expressway\\nSuite 400\\nAustin, TX 78746~Phone: 512-676-23156\\nFax: 512-676-2315\\nEmail: Chloe_Frew@shi.com", "status":"", "type":"string"},';
							purchase_req = purchase_req & '{"id":"requestor","value":"Eric Buclatin", "status":"", "type":"string"},';
							purchase_req = purchase_req & '{"id":"requestor_date","value":"09/10/2016", "status":"", "type":"date"},';
							purchase_req = purchase_req & '{"id":"account_number","value":"22-555-44444", "status":"", "type":"string"},';
							purchase_req = purchase_req & '{"id":"dept","value":"IT", "status":"", "type":"string"},';
							purchase_req = purchase_req & '{"id":"po","value":"22-555-44444", "status":"", "type":"string"},';
							purchase_req = purchase_req & '{"id":"buyer","value":"Buyer", "status":"", "type":"string"},';
							purchase_req = purchase_req & '{"id":"shipvia","value":"Ship Via", "status":"", "type":"string"},';
							purchase_req = purchase_req & '{"id":"comments","value":"No Comments", "status":"", "type":"string"}]';

							purchase_req = purchase_req & ',"line_items":[';
							purchase_req = purchase_req & '{"id":"itemRow_1", "status":"", "key":"", "line_item":[';
							purchase_req = purchase_req & '{"id":"lineItem", "value":"1", "status":"", "type":"string"},';
							purchase_req = purchase_req & '{"id":"quantity", "value":"1", "status":"", "type":"int"},';
							purchase_req = purchase_req & '{"id":"umo", "value":"123-00", "status":"", "type":"string"},';
							purchase_req = purchase_req & '{"id":"estcost", "value":"$1,00.00", "status":"", "type":"decimal"},';
							purchase_req = purchase_req & '{"id":"needdate", "value":"09/21/2016", "status":"", "type":"date"},';
							purchase_req = purchase_req & '{"id":"manufacturedesc", "value":"Head Set", "status":"", "type":"string"},';
							purchase_req = purchase_req & '{"id":"purchasequantity", "value":"1", "status":"", "type":"int"},';
							purchase_req = purchase_req & '{"id":"price", "value":"$1,000.00", "status":"", "type":"decimal"},';
							purchase_req = purchase_req & '{"id":"dockdate", "value":"09/18/2016", "status":"", "type":"date"}]}';
							/*
							purchase_req = purchase_req & 	',{"id":"itemRow_1", "status":"", "key":"", "info":[';
							purchase_req = purchase_req & 	'  {"id":"lineItem", "value":"", "status":"", "type":"int"}';
							purchase_req = purchase_req & 	', {"id":"quantity", "value":"", "status":"", "type":"int"}';
							purchase_req = purchase_req & 	', {"id":"umo", "value":"", "status":"", "type":"string"}';
							purchase_req = purchase_req & 	', {"id":"estcost", "value":"", "status":"", "type":"decimal"}';
							purchase_req = purchase_req & 	', {"id":"needdate", "value":"", "status":"", "type":"date"}';
							purchase_req = purchase_req & 	', {"id":"manufacturedesc", "value":"", "status":"", "type":"string"}';
							purchase_req = purchase_req & 	', {"id":"purchasequantity", "value":"", "status":"", "type":"int"}';
							purchase_req = purchase_req & 	', {"id":"price", "value":"", "status":"", "type":"decimal"}';
							purchase_req = purchase_req & 	', {"id":"dockdate", "value":"", "status":"", "type":"date"}]}';
							*/
							purchase_req = purchase_req & ']';

purchase_req = purchase_req & '}';



</cfscript>
<cfset purchase_req_obj = DeserializeJSON(#purchase_req#)>

<cfparam name="FORM.PurchaseRequisition_Number_Anchor" default="">
<cfdump var="#FORM.PurchaseRequisition_Number_Anchor#">

<cfoutput>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=11" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Customer Service Portal</title>
    <meta name="Title" content="Customer Service Portal">
    <meta name="Description" content="">
    <meta name="Keywords" content="">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="favicon.ico?v=4">

	<!-- Custom styles for this template -->
    <link href="css/purchaseRequest.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
</head>
<body>

            <div class="container" style="width:98%;">
                  <ul class="nav navbar-nav navbar-right">

                  <li style="padding-top:8px;padding-right:10px;"><button id="pfd_purchase_req" onclick="print_pr();" type="button" class="btn btn-primary" value="Submit" >PDF</button></li>
                  <li style="padding-top:8px;padding-right:10px;"><button id="delete_purchase_req" onclick="submit_pr('delete');" type="button" class="btn btn-primary" value="Submit" >Delete</button></li>
                  <li style="padding-top:8px;padding-right:10px;"><button id="new_purchase_req" onclick="new_pr();" type="button" class="btn btn-primary" value="Submit" >New</button></li>
                  <li style="padding-top:8px;padding-right:10px;"><button id="save_purchase_req" disabled=true onclick="submit_pr('update');" type="button" class="btn btn-primary" value="Submit" >Save</button></li>
                  <li style="padding-top:8px;padding-right:10px; display:none;"><button id="cancel_purchase_req" onclick="cancel_pr();" type="button" class="btn btn-default" value="Submit" >Cancel</button></li>
                  </ul>
	<div id="container_row1" class="row">
	<cfinclude template="./cfincludes/orderform.cfm">
	</div>
	<cfinclude template="./cfincludes/requestor.cfm">
 	<cfinclude template="./cfincludes/itemHeader.cfm">
	<cfinclude template="./cfincludes/purchaseItems.cfm">
	<cfinclude template="./cfincludes/commentFooter.cfm">
 </div>
 

<cfinclude template="./cfincludes/deleteItemRowModal.cfm">
<cfinclude template="./cfincludes/purchase-req-searchModal.cfm">
<cfinclude template="./cfincludes/update-purchase-reqModal.cfm">   
<cfinclude template="./cfincludes/newLineItem.cfm">
<cfinclude template="./cfincludes/json_empty_purchase_lineItem.cfm">  

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<cfoutput>
<cfinclude template="./cfincludes/userFunctions.cfm">

<script type="text/javascript">
var json = JSON.parse('#purchase_req#');
var json_new_line_item  = JSON.parse('#line_item_clone#');
var json_new_purchase_req = JSON.parse('#new_purchase_req#');

</script>
</cfoutput>
<script type="text/javascript" src="js/purchase-request.js"></script> 
<script type="text/javascript" src="js/autoComplete.js"></script> 
<script type="text/javascript" src="js/spin.min.js"></script>
<script type="text/javascript" src="js/spinnerOptions.js"></script> 
<script type="text/javascript" src="js/page_setup.js"></script> 
<script type="text/javascript" src="js/supplier_shipping.js"></script> 
<script type="text/javascript" src="js/jsonsql-0.1.js"></script> 
</body>
</html>

</cfoutput>