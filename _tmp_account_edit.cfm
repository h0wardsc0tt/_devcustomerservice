<cfoutput>
    <div class="col-lg-12">
    	<h2>Account Information</h2>
    </div>
    
    <!--- Error Message --->
    <cfif ERR.ErrorFound>
    	<div class="col-lg-12">
            <div class="form-message form-error">
                <ul>
                    <cfloop list="#ERR.ErrorMessage#" index="thisError"><li>#thisError#</li></cfloop>
                </ul>
            </div>
        </div>
    <cfelse>
    	<cfif Len(SuccessMsg) GT 0>
            <div class="col-lg-12">
                <div class="form-message form-success animated fadeIn">
                    <ul>
                        <li>#SuccessMsg#</li>
                    </ul>
                </div>
            </div>
        </cfif>
    </cfif>

	<!--- User Information Form --->
    <form action="./?pg=Account&st=Confirm" method="POST" id="Account_Info_Form" class="form-horizontal col-lg-11 form-marg" role="form">
    	<div class="col-lg-6">
            <div class="form-group">
                <label for="User_Company" class="control-label col-lg-6">Company:<span class="red-text">*</span></label>
                <div class="col-lg-6">
                    <select name="User_Company" id="User_Company" class="companies" multiple>
                      <option value="" disabled selected>Choose your option</option>
                      <cfloop from="1" to="#getUserCompanies.RecordCount#" index="Company">
                          <option value="#getUserCompanies.Company_Code[Company]#" <cfif ListFind(qry_getUserInfo.User_Company, getUserCompanies.Company_Code[Company]) NEQ 0>selected="selected"</cfif>> #getUserCompanies.Company_Name[Company]#</option>
                      </cfloop>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="User_Cust_ID" class="control-label col-lg-6">Customer ID:</label>
                <div class="col-lg-6">
                    <input type="text" maxlength="100" name="User_Cust_ID" class="form-control" value="#FORM.User_Cust_ID#" disabled="disabled"/>
                </div>
            </div>
            <div class="form-group">
                <label for="User_CompanyName" class="control-label col-lg-6">Company Name:</label>
                <div class="col-lg-6">
                	<input type="text" maxlength="100" name="User_CompanyName" class="form-control" value="#FORM.User_CompanyName#" />
                </div>
            </div>
            <div class="form-group">
                <label for="User_Brand" class="control-label col-lg-6">Company Brand:</label>
                <div class="col-lg-6">
                	<input type="text" maxlength="100" name="User_Brand" class="form-control" value="#FORM.User_Brand#" />
                </div>
            </div>
            <div class="form-group">
                <label for="User_Address_Line1" class="control-label col-lg-6">Company Address 1:</label>
                <div class="col-lg-6">
                	<input type="text" maxlength="100" name="User_Address_Line1" class="form-control" value="#FORM.User_Address_Line1#" />
                </div>
            </div>
            <div class="form-group">
                <label for="User_Address_Line2" class="control-label col-lg-6">Company Address 2:</label>
                <div class="col-lg-6">
                	<input type="text" maxlength="100" name="User_Address_Line2" class="form-control" value="#FORM.User_Address_Line2#" />
                </div>
            </div>
            <div class="form-group">
                <label for="User_Locality" class="control-label col-lg-6">Company City/Locality:</label>
                <div class="col-lg-6">
                	<input type="text" maxlength="100" name="User_Locality" class="form-control" value="#FORM.User_Locality#" />
                </div>
            </div>
        </div>
        <div class="col-lg-6">
        	<div class="form-group">
                <label for="User_Region" class="control-label col-lg-6">Company State/Region:</label>
                <div class="col-lg-6">
                	<input type="text" maxlength="100" name="User_Region" class="form-control" value="#FORM.User_Region#" />
                </div>
            </div>
        	<div class="form-group">
                <label for="User_Country" class="control-label col-lg-6">Company Country:</label>
                <div class="col-lg-6">
                	<input type="text" maxlength="100" name="User_Country" class="form-control" value="#FORM.User_Country#" />
                </div>
            </div>
            <div class="form-group">
                <label for="User_PostCode" class="control-label col-lg-6">Company Postal Code:</label>
                <div class="col-lg-6">
                	<input type="text" maxlength="100" name="User_PostCode" class="form-control" value="#FORM.User_PostCode#" />
                </div>
            </div>
            <div class="form-group">
                <label for="User_EmailAddress" class="control-label col-lg-6">Email Address:</label>
                <div class="col-lg-6">
                	<input type="text" maxlength="100" name="User_EmailAddress" class="form-control" value="#FORM.User_EmailAddress#"/>
                </div>
            </div>
            <div class="form-group">
                <label for="User_FirstName" class="control-label col-lg-6">First Name:</label>
                <div class="col-lg-6">
                	<input type="text" maxlength="100" name="User_FirstName" class="form-control" value="#FORM.User_FirstName#" />
                </div>
            </div>
            <div class="form-group">
                <label for="User_LastName" class="control-label col-lg-6">Last Name:</label>
                <div class="col-lg-6">
                	<input type="text" maxlength="100" name="User_LastName" class="form-control" value="#FORM.User_LastName#" />
                </div>
            </div>
            <div class="form-group">
                <label for="User_Phone_Number" class="control-label col-lg-6">Phone Number:</label>
                <div class="col-lg-6">
                	<input type="text" maxlength="100" name="User_Phone_Number" class="form-control" value="#FORM.User_Phone_Number#" />
                </div>
            </div>
        </div>
        
        <div class="col-lg-12">&nbsp;</div>
        <div class="col-lg-12">
        	<div class="form-group">
            	<div class="col-lg-5"></div>
                <div class="col-lg-3">
                	<input type="submit" value="Submit" class="btn btn-primary form-control" />
                </div>
            </div>
        </div>
    </form>
</cfoutput>