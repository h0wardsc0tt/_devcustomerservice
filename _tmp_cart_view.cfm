
<cfif ArrayLen(CartItems.DATA.Cart_ID) EQ 0>
	<cfinclude template="./cfincludes/_tmp_cart_empty.cfm">
<cfelse>
	<cfoutput>
    
    	<div class="product-cart-empty-hidden">
        	<cfinclude template="./cfincludes/_tmp_cart_empty.cfm">
        </div>
    
        <!--- Cart --->
        <div class="product-cart-cont-show">
            <div class="col-lg-12">
                <div class="col-lg-6 no-pad">
                    <h2>Cart <span class="product-cart-item-count">(<span class="product-cart-count">#ArrayLen(CartItems.DATA.Cart_ID)#</span> items)</span></h2>
                </div>
            </div>
            
            <div class="col-lg-12 product-cart">
                <!--- Cart Header --->
                <div class="col-lg-12 product-cart-head">
                    <div class="col-lg-6"></div>
                    <div class="col-lg-4 pull-right no-pad">
                        <div class="col-lg-4 no-pad pull-right">
                            <div class="col-lg-10 no-pad min-pad pull-right">Price</div>
                        </div>
                        <div class="col-lg-4 no-pad pull-right">
                            <div class="col-lg-10 no-pad pull-right">Quantity</div>
                        </div>
                        <div class="col-lg-4 no-pad pull-right">
                            <div class="col-lg-10 no-pad pull-right">Unit Price</div>
                        </div>
                    </div>
                </div>
                
                <!--- Cart Items --->
                <cfloop from="1" to="#ArrayLen(CartItems.DATA.Cart_ID)#" index="item">
                
                    <cfquery name="qry_getProductDetail" datasource="#APPLICATION.Datasource_AX#" maxrows="1">
                        SELECT [ITEMID], [NAMEALIAS], [DAXINTEGRATIONKEY], [DATAAREAID]
                        FROM [INVENTTABLE]
                        WHERE [DAXINTEGRATIONKEY] = <cfqueryparam cfsqltype="cf_sql_char" value="#CartItems.DATA.Cart_Product_UID[item]#">
                    </cfquery>
                
                    <div class="col-lg-12 product-cart-cont" id="#CartItems.DATA.Cart_Product_UID[item]#">
                        <div class="col-lg-6 col-xs-12 no-pad">
                            <div class="pull-left">
                                <a href="./?pg=Products&st=Details&id=#CartItems.DATA.Cart_Product_UID[item]#">
                                    <img src="./images/no-image.jpg" class="animated fadeIn" alt="No Image Available"/>
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="./?pg=Products&st=Details&id=#CartItems.DATA.Cart_Product_UID[item]#">
                                    <h5>#qry_getProductDetail.NAMEALIAS#</h5>
                                    <div class="product-part-number"><strong>Part Number:</strong> #qry_getProductDetail.ITEMID#</div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 no-pad pull-right">
                            <div class="col-lg-4 no-pad pull-right">
                                <div class="col-lg-10 no-pad min-pad pull-right Product_Cart_Price Product_Cart_Price_#CartItems.DATA.Cart_Product_UID[item]#">#DollarFormat(CartItems.DATA.Cart_Product_Price_SubTotal[item])#</div>
                            </div>
                            <div class="col-lg-4 no-pad pull-right">
                                <div class="col-lg-10 no-pad pull-right">
                                    <input type="text" id="Product_Cart_Qty" name="Product_Cart_Qty" class="form-control text-right Product_Cart_Qty Product_Cart_Qty_#CartItems.DATA.Cart_Product_UID[item]#" value="#CartItems.DATA.Cart_Product_QTY[item]#"/><br />
                                    <a href="javascript:void(0);" class="Product_Cart_Remove Product_Cart_Remove_#CartItems.DATA.Cart_Product_UID[item]#"><div><i class="fa fa-trash-o" aria-hidden="true"></i> Remove</div></a>
                                </div>
                            </div>
                            <div class="col-lg-4 no-pad pull-right price-text">
                                <div class="col-lg-10 no-pad pull-right Product_Cart_Unit_Price Product_Cart_Unit_Price_#CartItems.DATA.Cart_Product_UID[item]#">$6.00</div>
                            </div>
                        </div>
                    </div>
                </cfloop>
                
                <!--- Cart Footer --->
                <div class="col-lg-12 no-pad product-cart-footer">
                    <div class="col-lg-6 no-pad">
                        <a href="javascript:void(0);" class="Product_Cart_Clear">Clear Shopping Cart</a>
                    </div>
                    <div class="col-lg-4 product-shopping text-right"><a href="./?pg=Products">Continue Shopping</a></div>
                    <div class="col-lg-2">
                        Subtotal: <span class="price-text product-cart-subtotal"><strong> #DollarFormat(ArraySum(CartItems.DATA.Cart_Product_Price_SubTotal))#</strong></span><br />
                        <a href="" class="btn btn-primary btn-checkout">Checkout</a>
                    </div>
                </div>
            </div>
        </div>
    </cfoutput>
</cfif>