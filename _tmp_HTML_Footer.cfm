				<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
                <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
                <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
                <script type="text/javascript" src="./js/jquery.simplePagination.js"></script>
                
                <!--- Check Page, Include necessary JS files --->
                <cfif CGI.QUERY_STRING CONTAINS "Users">
                    <script type="text/javascript" src="./js/users.js?v=14"></script>
                </cfif>
                <cfif CGI.QUERY_STRING CONTAINS "Orders">
                    <script type="text/javascript" src="./js/orders.js?v=53"></script>
                </cfif>
                <cfif CGI.QUERY_STRING CONTAINS "PurchaseRequisition">
                    <script type="text/javascript" src="./js/PurchaseRequisition.js?v=33"></script>
                </cfif>
                <cfif CGI.QUERY_STRING CONTAINS "Customers">
                    <script type="text/javascript" src="./js/customers.js?v=33"></script>
                </cfif>
                <cfif CGI.QUERY_STRING CONTAINS "Invoices">
                    <script type="text/javascript" src="./js/invoices.js?v=72"></script>
                </cfif>
                <cfif CGI.QUERY_STRING CONTAINS "Products">
                    <script type="text/javascript" src="./js/store.js?v=23"></script>
                </cfif>
                <cfif CGI.QUERY_STRING CONTAINS "Cart">
                    <script type="text/javascript" src="./js/store.js?v=23"></script>
                </cfif>
                
                <!--- Include Main JS file --->
                <script type="text/javascript" src="./js/main.js?v=113"></script>
                
                <!--- Include Materialize Script for Custom Dropdown --->
                <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.js"></script>
                <script>
					$(document).ready(function() {
						$('.companies').material_select();
						
						$("[data-toggle=tooltip").tooltip();
					});
            	</script>
            </div>
        </div>
	</body>
</html>


