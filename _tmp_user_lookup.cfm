<!--- Set Defaults --->
<cfparam name="FORM.Cust_Number_Anchor" default="">

<cfoutput>
    <div class="col-lg-12">
    	<h2>Select User</h2>
    </div>
    
    <!--- Error Message --->
    <cfif ERR.ErrorFound>
    	<div class="col-lg-12">
            <div class="form-message form-error">
                <ul>
                    <cfloop list="#ERR.ErrorMessage#" index="thisError"><li>#thisError#</li></cfloop>
                </ul>
            </div>
        </div>
    </cfif>

    <form action="" method="POST" id="User_Search_Form" class="form-horizontal col-lg-12" role="form">
    	<div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="User_Cust_ID" class="control-label col-lg-5">Customer ID</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="User_Cust_ID" id="User_Cust_ID" class="form-control" value="#FORM.User_Cust_ID#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="User_EmailAddress" class="control-label col-lg-5">Email Address</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="User_EmailAddress" id="User_EmailAddress" class="form-control" value="#FORM.User_EmailAddress#" />
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="User_FirstName" class="control-label col-lg-5">
                    First Name</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="User_FirstName" id="User_FirstName" class="form-control" value="#FORM.User_FirstName#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="User_LastName" class="control-label col-lg-5">Last Name</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="User_LastName" id="User_LastName" class="form-control" value="#FORM.User_LastName#" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-4">
                <div class="form-group">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6 no-pad-search">
                        <input type="submit" name="Cust_Search_Submit" id="Cust_Search_Submit" class="btn btn-primary form-control" value="Search" />
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12">&nbsp;</div>
        
        <div class="col-lg-12 table-responsive no-pad">
            <div class="col-lg-10 visible-lg">
                <h4>Search Results <!---<span id="record-count">(0)</span>---></h4>
            </div>
            <a href="./?pg=Verify" class="btn btn-success pull-right col-lg-2 col-md-12 col-sm-12 col-xs-12 " id="verify_lookup">View Pending Users</a>
            
            <table id="User_Results" class="table" style="white-space:nowrap;margin:0px;">
                <thead id="User_Results_Header">
                    <tr>
                        <th></th>
                        <th>CUSTOMER ID</th>
                        <th>FIRST NAME</th>
                        <th>LAST NAME</th>
                        <th>EMAIL ADDRESS</th>
                        <th>USER TYPE</th>
                        <th>STATUS</th>
                    </tr>
                </thead>
                <tbody>
                    <cfloop from="1" to="#ArrayLen(UsersList.DATA.USER_UID)#" index="user">
                        <tr>
                        	<td><a href="./?pg=Users&st=Edit&uuid=#UsersList.DATA.User_UID[user]#">Edit</a></td>
                            <td>#UsersList.DATA.User_Cust_ID[user]#</a></td>
                            <td>#UsersList.DATA.User_FirstName[user]#</a></td>
                            <td>#UsersList.DATA.User_LastName[user]#</a></td>
                            <td>#UsersList.DATA.User_EmailAddress[user]#</a></td>
                            <td>#UsersList.DATA.UserType_Name[user]#</td>
                            <td>
                            	<cfif UsersList.DATA.User_IsActive[user]>
                                	<span class="glyphicon glyphicon-ok-circle greenStatus"></span>
                                <cfelse>
                                    <span class="glyphicon glyphicon-remove-circle redStatus"></span>
                                </cfif>
                            </td>
                        </tr>
                    </cfloop>
                </tbody>
            </table>
            
            <div class="col-lg-12 no-pad csp-paginate-cont">
                <div class="col-lg-6 no-pad">
                    <div class="col-lg-1 no-pad">
                        <button id="CSP_Prev_Page" name="CSP_Prev_Page" class="btn btn-primary btn-left" <cfif VARIABLES.Page_Start EQ 1>disabled="disabled"</cfif>><span class="glyphicon glyphicon-arrow-left"></span></button>
                    </div>
                    <div class="col-lg-11 text-left">
                        <div class="csp-perpage">
                            <span>
                                Show
                                <select id="CSP_Per_Page" name="CSP_Per_Page">
                                    <option value="5" <cfif FORM.CSP_Per_Page EQ 5>selected="selected"</cfif>>5</option>
                                    <option value="10" <cfif FORM.CSP_Per_Page EQ 10>selected="selected"</cfif>>10</option>
                                    <option value="20" <cfif FORM.CSP_Per_Page EQ 20>selected="selected"</cfif>>20</option>
                                </select>
                                items per page
                           </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 no-pad">
                    <div class="col-lg-11 text-right"><div class="csp-results">Showing <span class="csp-page-start">#VARIABLES.Page_Start#</span> - <span class="csp-page-end">#VARIABLES.Page_End#</span> of (#UsersCount#) Results</div></div>
                    <div class="col-lg-1 no-pad">
                        <button id="CSP_Next_Page" name="CSP_Next_Page" class="btn btn-primary btn-right pull-right" <cfif VARIABLES.Page_End GTE UsersCount>disabled="disabled"</cfif>><span class="glyphicon glyphicon-arrow-right"></span></button>
                    </div>
                </div>
                <input type="hidden" id="CSP_Current_Page" name="CSP_Current_Page" value="#FORM.CSP_Current_Page#" />
            </div>
        </div>
	</form>   
</cfoutput>

<form name="User_Anchor_Form" id="User_Anchor_Form" method="post" action="./?pg=Users&st=Edit">
		<input type="hidden" name="User_UID" id="User_UID" value="#FORM.User_UID#" />
    </form>
<cfinclude template="./_tmp_search_form.cfm">