<!--- Set Defaults --->
<cfparam name="FORM.Cust_Number_Anchor" default="">
<cfparam name="FORM.SO_Number_Anchor" default="">
<cfparam name="FORM.INV_Number_Anchor" default="">

<cfoutput>
    <div class="col-lg-8">
    	<h2>Select Invoice</h2>
    </div>
    
    <!--- Customer ID Tag --->
    <div class="col-lg-4 text-right">
    	<cfif Len(FORM.Cust_Number_Anchor) GT 0>
        	<div class="cust_tag_cont">
    			<span class="cust_tag">
                	<strong>Customer ID:</strong> #FORM.Cust_Number_Anchor# <a href="http://#APPLICATION.rootURL#/?pg=Invoices"><span class="glyphicon glyphicon-remove"></span></a>
                </span>
            </div>
        </cfif>
    </div>
    
    <!--- Error Message --->
    <cfif ERR.ErrorFound>
    	<div class="col-lg-12">
            <div class="form-message form-error">
                <ul>
                    <cfloop list="#ERR.ErrorMessage#" index="thisError"><li>#thisError#</li></cfloop>
                </ul>
            </div>
        </div>
    </cfif>
    
    <!--- Invoice Search Form --->
    <form action="./" method="POST" id="INV_Search_Form" class="form-horizontal col-lg-12" role="form">
    	<div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="INV_Number" class="control-label col-lg-5">Invoice Number:</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="INV_Number" id="INV_Number" class="form-control" value="" />
                    </div>
                </div>
                <cfif User_IsAdmin>
                    <div class="form-group cust-field">
                        <label for="Cust_Number" class="control-label col-lg-5">Customer ID:</label>
                        <div class="col-lg-7">
                            <input type="text" maxlength="100" name="Cust_Number" id="Cust_Number" class="form-control" value="" />
                        </div>
                    </div>
                </cfif>
                <div class="form-group">
                    <label for="INV_State" class="control-label col-lg-5">Invoice Status: </label>
                    <div class="col-lg-7">
                        <select name="INV_State" id="INV_State" class="form-control">
                            <option value=""></option>
                            <option value="Open">Open</option>
                            <option value="Paid">Paid</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="INV_Date_From" class="control-label col-lg-5">Date From: <div class="cal-cont"><span class="glyphicon glyphicon-calendar"></span></div></label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="INV_Date_From" id="INV_Date_From" class="form-control cal-field" value="" readonly="readonly" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="INV_Date_To" class="control-label col-lg-5">Date To: <div class="cal-cont"><span class="glyphicon glyphicon-calendar"></span></div></label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="INV_Date_To" id="INV_Date_To" class="form-control cal-field" value="" readonly="readonly" />
                    </div>
                </div>   
                <div class="form-group">
                    <label for="INV_Company" class="control-label col-lg-5">Company: </label>
                    <div class="col-lg-7">
                        <select name="INV_Company" id="INV_Company" class="form-control">
                        	<option value="hsc">HME</option>
                            <option value="jtch">JTECH</option>
                            <option value="cel">CE Repairs</option>
                            <option value="clrc">Clear-Com</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-4">
                <div class="form-group">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6 no-pad-search">
                    	<input type="hidden" name="SESSION_User_UID" id="SESSION_User_UID" value="#SESSION.User_UID#" />
                        <input type="submit" name="INV_Search_Submit" id="INV_Search_Submit" class="btn btn-primary form-control" value="Search" />
                    </div>
                </div>
            </div>
        </div>
    </form>
    
    <form name="Cust_Anchor_Form" id="Cust_Anchor_Form" method="post" action="./?pg=Invoices&st=View">
		<input type="hidden" name="Cust_Number_Anchor" id="Cust_Number_Anchor" value="#FORM.Cust_Number_Anchor#" />
        <input type="hidden" name="SO_Number_Anchor" id="SO_Number_Anchor" value="#FORM.SO_Number_Anchor#" />
        <input type="hidden" name="INV_Number_Anchor" id="INV_Number_Anchor" value="#FORM.INV_Number_Anchor#" />
    </form>
</cfoutput>

<div class="col-lg-12">&nbsp;</div>

<!--- Invoice Search Results --->
<div class="col-lg-12 table-responsive">
	<div class="col-lg-10 visible-lg">
    	<h4>Search Results</h4>
    </div>
	<input type="button" class="btn btn-success pull-right col-lg-2 col-md-12 col-sm-12 col-xs-12 " id="pay-invoice" value="Pay Invoice(s)" data-toggle="modal" data-target="#payModal"/>

    <table id="INV_Results" class="table" style="white-space:nowrap;">
    	<thead id="INV_Results_Header">
            <tr>
            	<th style="width:5%;"><input type="checkbox" name="selectINV_All" id="selectINV_All" class="selectCust_All" onClick="checkAllBoxes();" /></th>
                <th style="width:17%;">INVOICE</th>
                <th style="width:17%;">CUSTOMER ID</th>
                <th style="width:20%;">INVOICE BALANCE</th>
                <th style="width:20%;">PURCHASE ORDER</th>
                <th style="width:15%;">DATE</th>
                <th style="width:5%;"></th>
            </tr>
        </thead>
    </table>
    
	<div class="col-lg-12 text-center">
		<ul class="pagination"></ul>
	</div>
</div>

<cfset paymentType = "Invoice">
<cfinclude template="./_tmp_payment_form.cfm">
<cfinclude template="./_tmp_search_form.cfm">