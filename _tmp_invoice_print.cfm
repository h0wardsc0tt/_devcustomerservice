<cfset downloadFile = "invoices\InvoicePrint_#URL.inv#_#DateFormat(Now(),'yyyy-mm-dd')#.pdf">

<cfdocument 
	filename="#downloadFile#" 
	format="pdf"
	pagetype="letter" 
	backgroundvisible="yes" 
	fontembed="no" 
	mimetype="text/html" 
	marginbottom="0.3" 
	marginleft="0.4" 
	marginright="0.4" 
	margintop="0.3" 
	orientation="portrait"
	overwrite="yes">
    
<cfoutput>
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Invoice PDF</title>
            <meta name="Title" content="Invoice PDF">
            <meta name="Description" content="Invoice PDF">
            <meta name="Keywords" content="Invoice PDF">
            <meta name="format-detection" content="telephone=no">
            <link rel="stylesheet" href="./css/main-print.css?v=64">
        </head>
        <body style="height:900px;">
            <div class="container pdf_nav">
                <div class="hme-logo"><img src="./images/HME-Logo-large.png" alt="HME" width="50" /></div>
                <div class="hme-title">Customer Service Portal</div>
            </div>
            <div class="container">
                <div id="Content">
                    
                    <h2>INVOICE## #qry_getInvoiceDetails.INVOICENUMBER#</h2>
                    
                    <div class="inv_info_row">
                        <table class="fleft inv_left">
                            <tr>
                                <th>Customer No:</th>
                                <td>#qry_getInvoiceDetails.CUSTOMERID#</td>
                            </tr>
                            <tr>
                                <th>Order No:</th>
                                <td>#qry_getInvoiceDetails.SALESORDERNUMBER#</td>
                            </tr>
                            <tr>
                                <th>Order Date:</th>
                                <td>#DateFormat(qry_getInvoiceDetails.SALESORDERDATE, "yyyy-mm-dd")#</td>
                            </tr>
                            <tr>
                                <th>Order Status:</th>
                                <td>#qry_getInvoiceDetails.SALESSTATUSLABEL#</td>
                            </tr>
                            <tr>
                                <th>Purchase Order:</th>
                                <td>#qry_getInvoiceDetails.PURCHASEORDERNUMBER#</td>
                            </tr>
                            <tr>
                                <th>Contact:</th>
                                <td>#qry_getInvoiceDetails.CONTACTNAME#</td>
                            </tr>
                            <tr>
                                <th>Phone:</th>
                                <td>#qry_getInvoiceDetails.CONTACTPHONE#</td>
                            </tr>
                        </table>
                        
                        
                        <table class="fleft inv_right">
                            <tr>
                                <th class="inv_address">Bill To</th>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="2">#qry_getBillToAddress.CUSTOMERNAME#</td>
                            </tr>
                            <tr>
                                <td colspan="2">#qry_getBillToAddress.CONTACTNAME#</td>
                            </tr>
                            <tr>
                                <td colspan="2">#qry_getBillToAddress.ADDRESS#</td>
                            </tr>
                            <tr>
                                <td colspan="2">#qry_getBillToAddress.ADDRESSCITY#, #qry_getBillToAddress.ADDRESSSTATE# #qry_getBillToAddress.ADDRESSZIPCODE#</td>
                            </tr>
                        </table>
                    </div>
                    
                    <div class="inv_info_row">
                        <table class="fleft inv_left">
                            <tr>
                                <th class="inv_address">Sold To</th>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="2">#qry_getSoldToAddress.CUSTOMERNAME#</td>
                            </tr>
                            <tr>
                                <td colspan="2">#qry_getSoldToAddress.CONTACTNAME#</td>
                            </tr>
                            <tr>
                                <td colspan="2">#qry_getSoldToAddress.ADDRESS#</td>
                            </tr>
                            <tr>
                                <td colspan="2">#qry_getSoldToAddress.ADDRESSCITY#, #qry_getSoldToAddress.ADDRESSSTATE# #qry_getSoldToAddress.ADDRESSZIPCODE#</td>
                            </tr>
                        </table>
                        
                        <table class="fleft inv_right">
                            <tr>
                                <th class="inv_address">Ship To</th>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="2">#qry_getShipToAddress.CUSTOMERNAME#</td>
                            </tr>
                            <tr>
                                <td colspan="2">#qry_getShipToAddress.CONTACTNAME#</td>
                            </tr>
                            <tr>
                                <td colspan="2">#qry_getShipToAddress.ADDRESS#</td>
                            </tr>
                            <tr>
                                <td colspan="2">#qry_getShipToAddress.ADDRESSCITY#, #qry_getShipToAddress.ADDRESSSTATE# #qry_getShipToAddress.ADDRESSZIPCODE#</td>
                            </tr>
                        </table>
                    </div>    
                    
                    <div class="inv_info_row" style="height:500px;">
                        <table class="inv_lines" style="white-space:nowrap;" cellspacing="0">
                            <thead>
                                <tr>
                                    <th class="pad_left">LINE</th>
                                    <th>PART NUMBER</th>
                                    <th>DESCRIPTION</th>
                                    <th>QUANTITY</th>
                                    <th>PRICE</th>
                                    <th>DISCOUNT</th>
                                    <th>EXTENDED</th>
                                </tr>
                            </thead>
                            <tbody>
                            	<cfloop from="1" to="#qry_getInvoiceLines.RecordCount#" index="inv">
                                	<tr>
                                        <td class="pad_left">#NumberFormat(Int(qry_getInvoiceLines.LINENUM[inv]),"000")#</td>
                                        <td>#qry_getInvoiceLines.PARTNUMBER[inv]#</td>
                                        <td>#qry_getInvoiceLines.DESCRIPTION[inv]#</td>
                                        <td>#Int(qry_getInvoiceLines.QUANTITY[inv])#</td>
                                        <td>#DollarFormat(qry_getInvoiceLines.UNITPRICE[inv])#</td>
                                        <td>#DollarFormat(qry_getInvoiceLines.DISCAMOUNT[inv])#</td>
                                        <td>#DollarFormat(qry_getInvoiceLines.TOTALPRICE[inv])#</td>
                                        <td></td>
                                    </tr>
                                </cfloop>
                                <tr>
                                    <td colspan="6" class="blank_row blank_pad">Tax:</td>
                                    <td class="total_row blank_pad" style="padding-top:10px;">#DollarFormat(qry_getInvoiceDetails.SALESTAXES)#</td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="blank_row">Freight:</td>
                                    <td class="total_row">#DollarFormat(qry_getInvoiceDetails.SALESFREIGHT)#</td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="blank_row">Invoice Amount:</td>
                                    <td class="total_row">#DollarFormat(qry_getInvoiceDetails.INVOICEAMOUNT)#</td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="blank_row">Paid Amount:</td>
                                    <td class="total_row">#DollarFormat(qry_getInvoiceDetails.INVOICEPAYMENTS)#</td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="blank_row">Open Amount:</td>
                                    <td class="total_row">#DollarFormat(qry_getInvoiceDetails.INVOICEBALANCE)#</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </body>
    </html>
</cfoutput>

</cfdocument>

<cfmail to="#SESSION.User_EmailAddress#" from="no-reply@hme.com" subject="Customer Service Portal: Invoice PDF" type="html">
    This is a one-time email from the HME Customer Service Portal. You received this email because you requested this Invoice to be sent to you.  
    No future email will be sent to you unless you make another request through the HME Customer Service Portal.
    <cfmailparam file="#expandPath('.')#\#downloadFile#" type="application/pdf">
</cfmail>