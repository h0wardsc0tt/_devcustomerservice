<cfparam name="FORM.CSP_Page_Form" default="">
<cfparam name="FORM.CSP_Per_Page" default="5">
<cfparam name="FORM.CSP_Current_Page" default="1">
<cfparam name="VARIABLES.Page_Start" default="1">
<cfparam name="VARIABLES.Page_End" default="5">

<cfparam name="FORM.Purchase_Order" default="">
<cfparam name="FORM.Cust_Name" default="">
<cfparam name="FORM.Cust_City" default="">
<cfparam name="FORM.Cust_State" default="">
<cfparam name="FORM.Cust_PostCode" default="">
<cfparam name="FORM.Cust_SO" default="">
<cfparam name="FORM.Cust_INV" default="">
<cfparam name="FORM.Cust_SN" default="">
<cfparam name="FORM.Cust_Email" default="">

<cfscript>
	object = CreateObject("component", "_cfcs.productReq"); 
	pr_Struct =  StructNew();
	pr_Struct['id'] = '';
	pr_Struct['type'] = '';
	pr_Struct['pageNumber'] = FORM.CSP_Current_Page;
	pr_Struct['itemsPerPage'] = FORM.CSP_Per_Page;
	pr_Struct['supplier'] = '';
	pr_Struct['purchaseOrder'] = FORM.Purchase_Order;
	PurchaseRequisitionSelectList = DeserializeJSON(object.PurchaseRequisitionSelect(argumentCollection=pr_Struct));
	StructClear(pr_Struct);
	
	CustInfoObj = CreateObject("component", "_cfcs.CustInfo");
	CustomerCount = 0;//DeserializeJSON(CustInfoObj.GetCustomerCount(SESSION.User_UID,FORM.Purchase_Order,FORM.Cust_Name,FORM.Cust_City,FORM.Cust_State,FORM.Cust_PostCode,FORM.Cust_SO,FORM.Cust_INV,FORM.Cust_SN,FORM.Cust_Email));
	CustomerList = DeserializeJSON(CustInfoObj.GetCustomerList(SESSION.User_UID,FORM.CSP_Current_Page,FORM.CSP_Per_Page,FORM.Purchase_Order,FORM.Cust_Name,FORM.Cust_City,FORM.Cust_State,FORM.Cust_PostCode,FORM.Cust_SO,FORM.Cust_INV,FORM.Cust_SN,FORM.Cust_Email));
	
	VARIABLES.Page_Start = ((FORM.CSP_Current_Page-1)*FORM.CSP_Per_Page)+1;
	VARIABLES.Page_End = FORM.CSP_Current_Page*FORM.CSP_Per_Page;
</cfscript>
<!---<cfdump var="#PurchaseRequisitionSelectList#">--->
