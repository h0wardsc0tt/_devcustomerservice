
<cfif qry_getUserInfo.RecordCount EQ 0>
    <cfset ERR.ErrorFound = true>
    <cfset ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "User Account not found.")>
</cfif>

<div class="col-lg-12">
    <h2>User Verification</h2>
</div>

<!--- Error Message --->
<cfif ERR.ErrorFound>
    <cfoutput>
        <div class="col-lg-12">
            <div class="form-message form-error">
                <ul>
                    <cfloop list="#ERR.ErrorMessage#" index="thisError"><li>#thisError#</li></cfloop>
                </ul>
            </div>
        </div>
    </cfoutput>
</cfif>

<!--- Verification Information Form --->
<form action="./?pg=Verify&st=Confirm&uuid=<cfoutput>#URL.uuid#</cfoutput>" method="POST" class="form-horizontal col-lg-12 form-marg" role="form">
    <cfoutput>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="User_Company" class="control-label col-lg-5">Company:<span class="red-text">*</span></label>
                <div class="col-lg-7">
                    <select name="User_Company" id="User_Company" class="companies" multiple>
                      <option value="" disabled selected>Choose your option</option>
                      <cfloop from="1" to="#getUserCompanies.RecordCount#" index="Company">
                          <option value="#getUserCompanies.Company_Code[Company]#" <cfif ListFind(qry_getUserInfo.User_Company, getUserCompanies.Company_Code[Company]) NEQ 0>selected="selected"</cfif>> #getUserCompanies.Company_Name[Company]#</option>
                      </cfloop>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="User_Cust_ID" class="control-label col-lg-5">Customer ID:<span class="red-text">*</span></label>
                <div class="col-lg-7">
                    <input type="text" maxlength="100" name="User_Cust_ID" class="form-control" value="#qry_getUserInfo.User_Cust_ID#" <!---<cfif Len(qry_getUserInfo.User_Cust_ID) NEQ 0>readonly="readonly"</cfif>--->/>
                	<span class="note-field">Use commas for multiple IDs: (Ex: 123,456,789)</span>
                </div>
            </div>
            <div class="form-group">
                <label for="User_CompanyName" class="control-label col-lg-5">Company Name:<span class="red-text">*</span></label>
                <div class="col-lg-7">
                    <input type="text" maxlength="100" name="User_CompanyName" class="form-control" value="#qry_getUserInfo.User_CompanyName#" readonly="readonly"/>
                </div>
            </div>
            <div class="form-group">
                <label for="User_Brand" class="control-label col-lg-5">Company Brand:</label>
                <div class="col-lg-7">
                    <input type="text" maxlength="100" name="User_Brand" class="form-control" value="#qry_getUserInfo.User_Brand#" readonly="readonly"/>
                </div>
            </div>
            <div class="form-group">
                <label for="User_Address_Line1" class="control-label col-lg-5">Company Address 1:<span class="red-text">*</span></label>
                <div class="col-lg-7">
                    <input type="text" maxlength="100" name="User_Address_Line1" class="form-control" value="#qry_getUserInfo.User_Address_Line1#" readonly="readonly"/>
                </div>
            </div>
            <div class="form-group">
                <label for="User_Address_Line2" class="control-label col-lg-5">Company Address 2:</label>
                <div class="col-lg-7">
                    <input type="text" maxlength="100" name="User_Address_Line2" class="form-control" value="#qry_getUserInfo.User_Address_Line2#" readonly="readonly"/>
                </div>
            </div>
            <div class="form-group">
                <label for="User_Locality" class="control-label col-lg-5">Company City/Locality:<span class="red-text">*</span></label>
                <div class="col-lg-7">
                    <input type="text" maxlength="100" name="User_Locality" class="form-control" value="#qry_getUserInfo.User_Locality#" readonly="readonly"/>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="User_Region" class="control-label col-lg-5">Company State/Region:<span class="red-text">*</span></label>
                <div class="col-lg-7">
                    <input type="text" maxlength="100" name="User_Region" class="form-control" value="#qry_getUserInfo.User_Region#" readonly="readonly"/>
                </div>
            </div>
            <div class="form-group">
                <label for="User_Country" class="control-label col-lg-5">Company Country:<span class="red-text">*</span></label>
                <div class="col-lg-7">
                    <input type="text" maxlength="100" name="User_Country" class="form-control" value="#qry_getUserInfo.User_Country#" readonly="readonly"/>
                </div>
            </div>
            <div class="form-group">
                <label for="User_PostCode" class="control-label col-lg-5">Company Postal Code:<span class="red-text">*</span></label>
                <div class="col-lg-7">
                    <input type="text" maxlength="100" name="User_PostCode" class="form-control" value="#qry_getUserInfo.User_PostCode#" readonly="readonly"/>
                </div>
            </div>
            <div class="form-group">
                <label for="User_EmailAddress" class="control-label col-lg-5">Email Address:<span class="red-text">*</span></label>
                <div class="col-lg-7">
                    <input type="text" maxlength="100" name="User_EmailAddress" class="form-control" value="#qry_getUserInfo.User_EmailAddress#" readonly="readonly"/>
                </div>
            </div>
            <div class="form-group">
                <label for="User_FirstName" class="control-label col-lg-5">First Name:<span class="red-text">*</span></label>
                <div class="col-lg-7">
                    <input type="text" maxlength="100" name="User_FirstName" class="form-control" value="#qry_getUserInfo.User_FirstName#" readonly="readonly"/>
                </div>
            </div>
            <div class="form-group">
                <label for="User_LastName" class="control-label col-lg-5">Last Name:<span class="red-text">*</span></label>
                <div class="col-lg-7">
                    <input type="text" maxlength="100" name="User_LastName" class="form-control" value="#qry_getUserInfo.User_LastName#" readonly="readonly"/>
                </div>
            </div>
            <div class="form-group">
                <label for="User_Phone_Number" class="control-label col-lg-5">Phone Number:<span class="red-text">*</span></label>
                <div class="col-lg-7">
                    <input type="text" maxlength="100" name="User_Phone_Number" class="form-control" value="#qry_getUserInfo.User_Phone_Number#" readonly="readonly"/>
                </div>
            </div>
        </div>
    </cfoutput>
    
    <div class="col-lg-12 line-break"></div>
    
    <div class="col-lg-12 no-pad">
        <h2>User Permissions</h2>
    </div>
    
    <div class="col-lg-6">
        <div class="form-group">
            <label for="User_Type" class="control-label col-lg-5">User Type:<span class="red-text">*</span></label>
            <div class="col-lg-7">
                <select name="User_Type" id="User_Type" class="form-control" onchange="getUserTypeRoleList();">
                    <cfoutput query="getUserTypes">
                    	<cfif User_IsFullAdmin>
                            <option value="#UserType_Name#" <cfif UserType_Name EQ "Customer">selected="selected"</cfif>>#UserType_Name#</option>
                    	<cfelseif User_IsAdmin>
                        	<cfif UserType_Name NEQ "Full Admin">
                        		<option value="#UserType_Name#" <cfif UserType_Name EQ "Customer">selected="selected"</cfif>>#UserType_Name#</option>
                            </cfif>
                        <cfelse>
                        	<cfif UserType_Name DOES NOT CONTAIN "Admin">
                        		<option value="#UserType_Name#" <cfif UserType_Name EQ "Customer">selected="selected"</cfif>>#UserType_Name#</option>
                            </cfif>
                        </cfif>
                    </cfoutput>
                </select>
            </div>
        </div>
        <div class="form-group" style="display:none;">
            <label for="User_Role" class="control-label col-lg-5">User Role:<span class="red-text">*</span></label>
            <div class="col-lg-7">
                <select name="User_Role" id="User_Role" class="form-control">
                    <cfoutput query="getUserTypeRoles">
                    	<option value="#TypeRole_ID#" selected="selected">#TypeRole_Name#</option>
                    </cfoutput>
                </select>
            </div>
        </div>
        <div class="form-group">
        	<label for="User_Company" class="control-label col-lg-5">User Company:<span class="red-text">*</span></label>
            <div class="col-lg-7">
                <select name="User_Company" id="User_Company" class="companies" multiple>
                  <option value="" disabled selected>Choose your option</option>
                  <cfoutput query="getUserCompanies">
                      <option value="#Company_Code#"> #Company_Name#</option>
                  </cfoutput>
                </select>
            </div>
        </div>
        <div class="form-group">
        	<label for="User_Select_Apps" class="control-label col-lg-5">User Applications:<span class="red-text">*</span></label>
            <div class="col-lg-7">
                <select name="User_Select_Apps" id="User_Select_Apps" class="companies apps" multiple>
                  <option value="" disabled selected>Choose your option</option>
                  <cfoutput query="getUserApplications">
                      <option value="#App_FullName#"> #App_FullName#</option>
                  </cfoutput>
                </select>
            </div>
        </div>
    </div>
    
    <div class="col-lg-6 admin-perm-cont" style="display:none;">
		<div class="form-group">
        	<label for="Apply_Credit" class="control-label col-lg-5">Apply Credit:<span class="red-text">*</span></label>
            <div class="col-lg-2 apply-check-cont">
                <input type="checkbox" name="Apply_Credit" id="Apply_Credit" class="form-control apply-check"/>
            </div>
        </div>
        <div class="form-group">
        	<label for="Apply_Deposit" class="control-label col-lg-5">Apply Deposit:<span class="red-text">*</span></label>
            <div class="col-lg-2 apply-check-cont">
                <input type="checkbox" name="Apply_Deposit" id="Apply_Deposit" class="form-control apply-check"/>
            </div>
        </div>
    </div>
    
    <div class="col-lg-12 line-break pad-btm"></div>
    
    <cfoutput>
        <div class="col-lg-2">
            <input type="submit" name="User_Submit" class="btn btn-danger form-control" value="Decline" />
        </div>
        <div class="col-lg-8"></div>
        <div class="col-lg-2">
            <input type="hidden" name="User_UID" value="#qry_getUserInfo.User_UID#" />
            <input type="submit" name="User_Submit" class="btn btn-success form-control" value="Accept" />
        </div>
    </cfoutput>
</form>
