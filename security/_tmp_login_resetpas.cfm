<cfparam name="FORM.User_EmailAddress" default="">

<cfsilent>
	<cfscript>
		if(FORM.User_EmailAddress IS NOT "") {
            //Validate proper email
            if(ReFindNoCase("^([a-zA-Z0-9_\-\.\+])+@(([0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5])|((([a-zA-Z0-9\-])+\.)+([a-zA-Z\-])+))$", FORM.User_EmailAddress) EQ 0) {
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Email Address is invalid. Please try again");
            }
        } else {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Email Address is invalid. Please try again");
        }
	</cfscript>
</cfsilent>

<cfif ERR.ErrorFound>
	<cfinclude template="./_tmp_login_resetreq.cfm">
	<cfexit method="exittemplate">
</cfif>

<cfscript>
	QUERY = StructNew();
	QUERY.QueryName = "qry_getUser_UID";
	QUERY.SelectSQL = "usrs.User_UID";
	QUERY.User_EmailAddress = FORM.User_EmailAddress;
</cfscript>
<cfinclude template="../_qry_select_users.cfm">

<cfif qry_getUser_UID.RecordCount NEQ 0>
	<cfscript>
		getRUID = CreateObject("component", "_cfcs.Generate_UID");
		FORM.Reset_UID = getRUID.genUID(Length=32);
	</cfscript>
	
	<cfquery name="qry_insertReset" datasource="#APPLICATION.Datasource#">
		INSERT INTO [ServicePortal].[dtbl_Pass_Reset]
		(
        	[User_UID],
			[Reset_UID],
			[Reset_DTS],
			[Reset_IP],
			[Reset_Agent]
        )
		VALUES
		(
        	<cfqueryparam cfsqltype="cf_sql_varchar" value="#qry_getUser_UID.User_UID#">,
			<cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.Reset_UID#">,
			<cfqueryparam cfsqltype="cf_sql_varchar" value="#CurrentDateTime#">,
			<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.REMOTE_ADDR#">,
			<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.HTTP_USER_AGENT#">
        )
	</cfquery>

    <cfmail to="#FORM.User_EmailAddress#" from="no-reply@hme.com" bcc="Web_Support@hme.com" subject="Customer Service Portal: Password Reset Requested" type="html">
        <p>We have received a password change request for your acount: #FORM.User_EmailAddress#</p>
        <p>If you made this request, then please click on the link below or by pasting it into your browser's address bar.</p>
        <p><a href="http://#APPLICATION.rootURL#/?pg=ManageAccount&st=rl&ruid=#FORM.Reset_UID#">http://#APPLICATION.rootURL#/?pg=ManageAccount&st=rl&ruid=#FORM.Reset_UID#</a></p>
        <p>This link will work for 2 hours or until you reset your password.</p>
        <p>If you did not ask to change your password, then please ignore this email. Another user may have entered your username by mistake. No changes will be made to your account.</p>
        <p>Thank you</p>
    </cfmail>
    
    <!---<cfoutput>
        <p>Password Reset Request has been sent to <strong>#FORM.User_EmailAddress#</strong>.</p>
        <p>Please check your email to reset your password.</p>
    </cfoutput>--->
<cfelse>
	<cfmail to="Web_Support@hme.com" from="no-reply@hme.com" subject="Bad Record Count">
		#FORM.User_EmailAddress# - Not Found In _qry_select_users
	</cfmail>
    
    <!---<cfset ERR.ErrorFound = true>
    <cfset ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Email Address is invalid. Please try again.")>

	<cfinclude template="./_tmp_login_resetreq.cfm">
	<cfexit method="exittemplate">--->
</cfif>

<cfoutput>
    <p>Password Reset Request has been sent to <strong>#FORM.User_EmailAddress#</strong>.</p>
    <p>Please check your email to reset your password.</p>
</cfoutput>

