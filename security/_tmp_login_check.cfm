<cfsilent>
	<!--- COMMENT OUT DURING DEVELOPMENT TO BYPASS LOGIN --->
	<cfscript>
        if(FORM.Username IS NOT "") {
            //Validate proper email
            if(ReFindNoCase("^([a-zA-Z0-9_\-\.\+])+@(([0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5])|((([a-zA-Z0-9\-])+\.)+([a-zA-Z\-])+))$", FORM.Username) EQ 0) {
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Username is invalid. Please try again.");
            }
        } else {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Username is invalid. Please try again.");
        }
        if(Len(FORM.Password) GT 0) {
            if(Len(FORM.Password) LT 8 OR Len(FORM.Password) GT 16) {
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Invalid credentials supplied. Please try again.");
            } 
        } else {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Password may not be blank. Please try again.");
        }
    </cfscript>
    <!--- X --->
</cfsilent>

<cfif StructKeyExists(SESSION,"loginAttempts") AND StructKeyExists(LOGIN,"maxAttempts")>
	<cfif SESSION.loginAttempts GTE LOGIN.maxAttempts>
		<cfinclude template="./_tmp_login_form.cfm">
		<cfexit method="exittemplate">
	</cfif>
<cfelse>
	<cfset ERR.ErrorFound = true>
	<cfset ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Login Attempt Invalid. The system administrator has been notified")>
	<cfmail to="Web_Support@hme.com" from="no-reply@hme.com" subject="Customer Service Portal: Bad Login Attempt">
		Bad Login Attempt:
		#CGI.REMOTE_ADDR#
		#CGI.HTTP_USER_AGENT#
	</cfmail>
	<cfinclude template="./_tmp_login_form.cfm">
	<cfexit method="exittemplate">
	<cfabort>
</cfif>

<cfif ERR.ErrorFound>
	<cfset SESSION.loginAttempts = SESSION.loginAttempts + 1>
	<cfinclude template="./_tmp_login_form.cfm">
	<cfexit method="exittemplate">
</cfif>

<!---Get Existing User--->
<cfquery name="qry_getUserRecord" datasource="#APPLICATION.Datasource#" maxrows="1">
	SELECT usrs.User_UID,usrs.User_PasswordHash,usrs.User_PasswordSalt,urol.UserType_ID,uper.TypeRole_ID,usrs.User_Cust_ID
	FROM [ServicePortal].[tbl_Users] usrs
    INNER JOIN [Permission].[User_Permission] uper ON usrs.User_ID = uper.User_ID
    INNER JOIN [Permission].[UserType_Role] urol ON uper.TypeRole_ID = urol.TypeRole_ID
	WHERE 0=0
	AND usrs.User_EmailAddress = <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.Username#">
</cfquery>

<cflock scope="session" type="exclusive" timeout="#APPLICATION.SessionTimeout#">
	<!---Test Credentials--->  
	<cfscript>
		InvokeCrypto = CreateObject("component", "_cfcs.secHash");
		User_Salt = qry_getUserRecord.User_PasswordSalt;
		User_Hash = InvokeCrypto.computeHash(FORM.Password, User_Salt);
		
		// COMMENT OUT CHECK DURING DEVELOPMENT TO BYPASS LOGIN
		if(Compare(qry_getUserRecord.User_PasswordHash, User_Hash) EQ 0) {
			
			if(qry_getUserRecord.UserType_ID EQ 1) {
				UserIsAdmin = true;
			} else {
				UserIsAdmin = false;
			}
			
			if(qry_getUserRecord.UserType_ID EQ 4) {
				UserIsFullAdmin = true;
				UserIsAdmin = true;
			} else {
				UserIsFullAdmin = false;
			}
			
			SESSION.IsLoggedIn = true;
			SESSION.loginAttempts = 0;
			SESSION.User_UID = qry_getUserRecord.User_UID;
			SESSION.User_EmailAddress = FORM.Username;
			SESSION.User_IsAdmin = UserIsAdmin;
			SESSION.User_IsFullAdmin = UserIsFullAdmin;
			SESSION.User_CustId = qry_getUserRecord.User_Cust_ID;
			
			CreateSession = CreateObject("component", "_cfcs.SessionMgmt");
			NewUser_Session = CreateSession.initSession(User_UID=SESSION.User_UID);
		} else {
			SESSION.IsLoggedIn = false;
		
			ERR.ErrorFound = true;
			ERR.ErrorMessage = "Login fail. The username or password you entered was not found in our system.<br />If this problem persists please contact your system administrator.";	
		}
    </cfscript>
</cflock>

<cfif ERR.ErrorFound>
	<cfset SESSION.loginAttempts = SESSION.loginAttempts + 1>
	<cfinclude template="./_tmp_login_form.cfm">
	<cfexit method="exittemplate">
</cfif>

<cfif SESSION.User_IsAdmin>
    <cflocation url="./?pg=Customers" addtoken="no">
<cfelse>
    <cflocation url="./?pg=Orders" addtoken="no">
</cfif>
