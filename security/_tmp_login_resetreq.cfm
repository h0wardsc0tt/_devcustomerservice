<cfoutput>
    
    <cfif ERR.ErrorFound>
        <div class="col-lg-12 animated fadeIn" style="margin-top:35px;">
            <div class="form-message form-error">
                <ul>
                    <cfloop list="#ERR.ErrorMessage#" index="thisError"><li>#thisError#</li></cfloop>
                </ul>
            </div>
        </div>
    </cfif>
    
    <div class="col-lg-12">
    	<h2>Reset Password</h2>
    </div>

    <form action="./?pg=ManageAccount&st=rp" method="POST" class="form-horizontal col-lg-12" role="form">
        <div class="col-lg-4">
            <div class="form-group">
            	<label for="User_EmailAddress" class="control-label col-lg-5">Email Address</label>
                <div class="col-lg-7">
                	<input type="text" maxlength="100" name="User_EmailAddress" class="form-control" value="" />
                </div>
            </div>
            <div class="form-group">
            	<div class="col-lg-5"></div>
                <div class="col-lg-7">
                	<input type="submit" value="Reset Password" class="btn btn-primary form-control"/>
                </div>
            </div>
        </div>
    </form>
</cfoutput>