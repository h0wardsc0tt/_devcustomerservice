<cfparam name="FORM.Username" default="">
<cfparam name="FORM.Password" default="">

<cfquery name="qry_getFields" datasource="#APPLICATION.Datasource#">
    SELECT COLUMN_NAME, DATA_TYPE, COLUMN_DEFAULT
    FROM INFORMATION_SCHEMA.COLUMNS 
    WHERE TABLE_NAME IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="tbl_Users," list="yes">)
</cfquery>

<cfset thisCount = 1>
<cfloop list="#ValueList(qry_getFields.COLUMN_NAME)#" index="thisField">
    <cfset defaultValue = ReReplaceNoCase(qry_getFields.COLUMN_DEFAULT[thisCount], "[(|\'|)]", "", "all")>
    <cfparam name="FORM.#thisField#" default="#defaultValue#">
    <cfset thisCount = thisCount+1>
</cfloop>

<cfquery name="qry_getCountries" datasource="#APPLICATION.Datasource_AX#">
	SELECT [COUNTRYNAME]
    FROM [HMECPCOUNTRIESLOOKUP]
    ORDER BY [COUNTRYNAME]
</cfquery>

<cfquery name="qry_getPendingUsers" datasource="#APPLICATION.Datasource#">
	SELECT [User_ID],[User_UID],[User_Cust_ID],[User_FirstName],[User_LastName],[User_EmailAddress]
    FROM [ServicePortal].[tbl_Users]
    WHERE [User_IsVerified] = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
</cfquery>

<cfparam name="URL.uuid" default="#FORM.User_UID#">

<cfquery name="qry_getUserInfo" datasource="#APPLICATION.Datasource#">
    SELECT [User_ID],[User_UID],[User_Cust_ID],[User_IsActive],[User_FirstName],[User_LastName],[User_EmailAddress],[User_Address_Line1],[User_Address_Line2],[User_Phone_Number],[User_Locality],[User_Region],[User_PostCode],[User_Country],[User_Company],[User_CompanyName],[User_Brand]
    FROM [ServicePortal].[tbl_Users]
    WHERE [User_UID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#URL.uuid#">
</cfquery>

<cfscript>
	getUserPermissions = CreateObject("component", "_cfcs.UserPermission");
	getUserTypes = getUserPermissions.getAllUserType();
	getUserTypeRoles = getUserPermissions.getUserRoleByType("Customer");
	getUserApplications = getUserPermissions.getApplicationByType("Customer");
	getUserCompanies = getUserPermissions.getAllCompanies();
</cfscript>