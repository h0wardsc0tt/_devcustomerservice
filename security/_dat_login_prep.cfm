<cfparam name="FORM.Username" default="">
<cfparam name="FORM.Password" default="">

<cfquery name="qry_getFields" datasource="#APPLICATION.Datasource#">
    SELECT COLUMN_NAME, DATA_TYPE, COLUMN_DEFAULT
    FROM INFORMATION_SCHEMA.COLUMNS 
    WHERE TABLE_NAME IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="tbl_Users" list="yes">)
</cfquery>

<cfset thisCount = 1>
<cfloop list="#ValueList(qry_getFields.COLUMN_NAME)#" index="thisField">
    <cfset defaultValue = ReReplaceNoCase(qry_getFields.COLUMN_DEFAULT[thisCount], "[(|\'|)]", "", "all")>
    <cfparam name="FORM.#thisField#" default="#defaultValue#">
    <cfset thisCount = thisCount+1>
</cfloop>

<cfquery name="qry_getCountries" datasource="#APPLICATION.Datasource_AX#">
	SELECT [COUNTRYNAME]
    FROM [HMECPCOUNTRIESLOOKUP]
    ORDER BY [COUNTRYNAME]
</cfquery>

<cfscript>
	getUserPermissions = CreateObject("component", "_cfcs.UserPermission");
	getUserCompanies = getUserPermissions.getAllCompanies();
</cfscript>