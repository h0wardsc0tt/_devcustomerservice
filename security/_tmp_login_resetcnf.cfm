<cfsilent>
	<cfscript>
        if(Len(URL.ruid) NEQ 32 OR ReFindNoCase("([0-9A-Z]){32}", URL.ruid) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage,"Password link invalid. Please try your request again.");
        }
    </cfscript>
</cfsilent>

<cfif ERR.ErrorFound>
	<cfinclude template="./_tmp_login_resetlnk.cfm">
	<cfexit method="exittemplate">
</cfif>

<cfsilent>
	<cfscript>
        //Password Match Criteria
        if(Len(FORM.Password1) LT 8 OR Len(FORM.Password1) GT 16) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Password must be between 8 and 16 characters in length.");
        } else { //Make sure password is in corrrect format
            if(ReFind("[A-Z]", FORM.Password1) EQ 0) { //Check Uppercase
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Password must contain at least 1 uppercase character.");
            }
            if(ReFind("[a-z]", FORM.Password1) EQ 0) { //Check Lowercase
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Password must contain at least 1 lowercase character.");
            }
            if(ReFind("[0-9]", FORM.Password1) EQ 0) { //Check Numeric
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Password must contain at least 1 number.");
            }
            /*20140227 CG: Added requirement [766]*/
            if(ReFind("[\s]", FORM.Password1) GT 0) { //Check Character
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Password may not contain blank spaces.");
            }
            /*20130917 CG: Added check to see if "1,l,O,0" is used in password creation [109] 
            if(ReFind("[1|l|0|O]", FORM.Password1) GT 0) { //Check Character
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Password may not contain the following: '1 l O 0'.");
            }*/
        }
        if(Len(FORM.Password2) LT 8 OR Len(FORM.Password2) GT 16) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Confirmed Password must be between 8 and 16 characters in length.");
        } else { //Make sure password is in corrrect format
            if(ReFind("[A-Z]", FORM.Password2) EQ 0) { //Check Uppercase
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Confirmed Password must contain at least 1 uppercase character.");
            }
            if(ReFind("[a-z]", FORM.Password2) EQ 0) { //Check Lowercase
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Confirmed Password must contain at least 1 lowercase character.");
            }
            if(ReFind("[0-9]", FORM.Password2) EQ 0) { //Check Numeric
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Confirmed Password must contain at least 1 number.");
            }
            /*20140227 CG: Added requirement [766]*/
            if(ReFind("[\s]", FORM.Password2) GT 0) { //Check Character
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Confirmed Password may not contain blank spaces.");
            }
            /*20130917 CG: Added check to see if "1,l,O,0" is used in password creation [109] 
            if(ReFind("[1|l|0|O]", FORM.Password2) GT 0) { //Check Character
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Confirmed Password may not contain the following: '1 l O 0'.");
            }*/
        }
        if(NOT ERR.ErrorFound) {
            if(Compare(FORM.Password1,FORM.Password2) NEQ 0) {
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Passwords do not match.");
            } 
        }
    </cfscript>
</cfsilent>

<cfif ERR.ErrorFound>
	<cfinclude template="./_tmp_login_resetlnk.cfm">
	<cfexit method="exittemplate">
</cfif>

<cfsilent>
	<cfscript>
        QUERY = StructNew();
        QUERY.QueryName = "qry_getReset_UID";
        QUERY.Reset_UID = URL.ruid;
    </cfscript>
    <cfinclude template="../_qry_select_reset.cfm">

	<cfif qry_getReset_UID.RecordCount EQ 0>
        <cfset ERR.ErrorFound = true>
        <cfset ERR.ErrorMessage = ListAppend(ERR.ErrorMessage,"Password link expired. Please try your request again.")>
    </cfif>
</cfsilent>

<cfif ERR.ErrorFound>
	<cfinclude template="./_tmp_login_resetlnk.cfm">
	<cfexit method="exittemplate">
</cfif>

<cfsilent>
	<cfscript>
        QUERY = StructNew();
        QUERY.QueryName = "qry_getUser_UID";
        QUERY.SelectSQL = "usrs.User_EmailAddress,usrs.User_UID";
        QUERY.User_UID = qry_getReset_UID.User_UID;
    </cfscript>
    <cfinclude template="../_qry_select_users.cfm">
    
    <cfif qry_getUser_UID.RecordCount NEQ 0>
        <cfscript>
            //Hash Password
            InvokeCrypto = CreateObject("component", "_cfcs.secHash");
            User_Salt = InvokeCrypto.genSalt();
            User_Hash = InvokeCrypto.computeHash(FORM.Password1, User_Salt);
        </cfscript>
        
        <cfquery name="qry_UpdatePassword" datasource="#APPLICATION.Datasource#" maxrows="1">
            UPDATE [ServicePortal].[tbl_Users]
            SET 
                User_PasswordHash = <cfqueryparam cfsqltype="cf_sql_varchar" value="#User_Hash#">,
                User_PasswordSalt = <cfqueryparam cfsqltype="cf_sql_varchar" value="#User_Salt#">
            WHERE 
                User_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#qry_getUser_UID.User_UID#">
        </cfquery>
        
        <cfquery name="qry_UpdatePassword" datasource="#APPLICATION.Datasource#" maxrows="1">
            UPDATE [ServicePortal].[dtbl_Pass_Reset]
            SET 
                Reset_IsCompleted = <cfqueryparam cfsqltype="cf_sql_tinyint" value="1">
            WHERE 
                Reset_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#qry_getReset_UID.Reset_ID#">
        </cfquery>
	
        <cfmail to="#qry_getUser_UID.User_EmailAddress#" bcc="Web_Support@hme.com" from="no-reply@hme.com" subject="Customer Service Portal: Password Reset Confirmation" type="html">
        	<p>This email confirms your recent HME CLOUD password change.</p>
        	<p>If your HME CLOUD password was changed without your knowledge, then please click the link below to change it again:</p>
        	<p>http://#APPLICATION.rootURL#/?pg=ManageAccount&st=rq</p>
			<p>Thank You</p>
        </cfmail>
    <cfelse>
        <cfmail to="Web_Support@hme.com" from="no-reply@hme.com" subject="Customer Service Portal: Password Reset Fail" type="html">
            <p>#URL.ruid#</p>
            <p>#CGI.REMOTE_ADDR#</p>
        </cfmail>
    </cfif>
</cfsilent>
<cfoutput>
	<p>Password Reset Confirmed</p>
	<p>Please <a href="http://#APPLICATION.rootURL#/?pg=Login">login</a> to your account.</p>
</cfoutput>