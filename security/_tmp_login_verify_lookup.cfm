<div class="col-lg-12">
    <h2>Pending Users</h2>
</div>

<cfif ERR.ErrorFound>
    <cfoutput>
        <div class="col-lg-12">
            <div class="form-message form-error">
                <ul>
                    <cfloop list="#ERR.ErrorMessage#" index="thisError"><li>#thisError#</li></cfloop>
                </ul>
            </div>
        </div>
    </cfoutput>
</cfif>

<div class="col-lg-12 table-responsive">
    <table id="User_Results" class="table" style="white-space:nowrap;">
        <thead id="User_Results_Header">
            <tr>
            	<th></th>
                <th>CUSTOMER ID</th>
                <th>FIRST NAME</th>
                <th>LAST NAME</th>
                <th>EMAIL ADDRESS</th>
            </tr>
        </thead>
        <tbody>
        	<cfoutput query="qry_getPendingUsers">
            	<tr>
                	<td width="125"></span><a href="./?pg=Verify&st=View&uuid=#User_UID#"> View Details</a></td>
                    <td>#User_Cust_ID#</td>
                    <td>#User_FirstName#</td>
                    <td>#User_LastName#</td>
                    <td>#User_EmailAddress#</td>
                </tr>
            </cfoutput>
        </tbody>
    </table>
    
    <div class="col-lg-12 text-center">
        <ul class="pagination"></ul>
	</div>
</div>