<cfparam name="regAttemptsMessage" default="">

<cfsilent>
	<cfscript>
		if(Len(FORM.User_CompanyName) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Company Name may not be blank. Please try again");
        }
		if(Len(FORM.User_Company) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Company may not be blank. Please try again");
        }
		if(Len(FORM.User_Address_Line1) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Company Address 1 may not be blank. Please try again");
        }
		if(Len(FORM.User_Locality) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Company City/Locality may not be blank. Please try again");
        }
		if(Len(FORM.User_Region) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Company State/Region may not be blank. Please try again");
        }
		if(Len(FORM.User_Country) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Company Country may not be blank. Please try again");
        }
		if(Len(FORM.User_PostCode) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Company Postal Code may not be blank. Please try again");
        }
        if(FORM.User_EmailAddress IS NOT "") {
            //Validate proper email
            if(ReFindNoCase("^([a-zA-Z0-9_\-\.\+])+@(([0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5])|((([a-zA-Z0-9\-])+\.)+([a-zA-Z\-])+))$", FORM.User_EmailAddress) EQ 0) {
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Email Address is invalid. Please try again");
            }
        } else {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Email Address is invalid. Please try again");
        }
		if(Len(FORM.User_FirstName) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "First Name may not be blank. Please try again");
        }
		if(Len(FORM.User_LastName) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Last Name may not be blank. Please try again");
        }
		if(Len(FORM.User_Phone_Number) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Phone Number may not be blank. Please try again");
        }
		/*if(Len(FORM.User_PasswordQuestion) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Password Question may not be blank. Please try again");
        }
		if(Len(FORM.User_PasswordAnswer) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Password Answer may not be blank. Please try again");
        }*/
		/*if(Len(FORM.User_Password1) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Password may not be blank. Please try again");
        }
		if(Len(FORM.User_Password2) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Password Confirm may not be blank. Please try again");
        }
		if(FORM.User_Password1 NEQ FORM.User_Password2){
			ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Passwords must match. Please try again");	
		}*/
		
		//Password Match Criteria
        if(Len(FORM.User_Password1) LT 8 OR Len(FORM.User_Password1) GT 16) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Password must be between 8 and 16 characters in length.");
        } else { //Make sure password is in corrrect format
            if(ReFind("[A-Z]", FORM.User_Password1) EQ 0) { //Check Uppercase
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Password must contain at least 1 uppercase character.");
            }
            if(ReFind("[a-z]", FORM.User_Password1) EQ 0) { //Check Lowercase
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Password must contain at least 1 lowercase character.");
            }
            if(ReFind("[0-9]", FORM.User_Password1) EQ 0) { //Check Numeric
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Password must contain at least 1 number.");
            }
            /*20140227 CG: Added requirement [766]*/
            if(ReFind("[\s]", FORM.User_Password1) GT 0) { //Check Character
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Password may not contain blank spaces.");
            }
            /*20130917 CG: Added check to see if "1,l,O,0" is used in password creation [109] 
            if(ReFind("[1|l|0|O]", FORM.User_Password1) GT 0) { //Check Character
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Password may not contain the following: '1 l O 0'.");
            }*/
        }
        if(Len(FORM.User_Password2) LT 8 OR Len(FORM.User_Password2) GT 16) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Confirmed Password must be between 8 and 16 characters in length.");
        } else { //Make sure password is in corrrect format
            if(ReFind("[A-Z]", FORM.User_Password2) EQ 0) { //Check Uppercase
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Confirmed Password must contain at least 1 uppercase character.");
            }
            if(ReFind("[a-z]", FORM.User_Password2) EQ 0) { //Check Lowercase
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Confirmed Password must contain at least 1 lowercase character.");
            }
            if(ReFind("[0-9]", FORM.User_Password2) EQ 0) { //Check Numeric
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Confirmed Password must contain at least 1 number.");
            }
            /*20140227 CG: Added requirement [766]*/
            if(ReFind("[\s]", FORM.User_Password2) GT 0) { //Check Character
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Confirmed Password may not contain blank spaces.");
            }
            /*20130917 CG: Added check to see if "1,l,O,0" is used in password creation [109] 
            if(ReFind("[1|l|0|O]", FORM.User_Password2) GT 0) { //Check Character
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Confirmed Password may not contain the following: '1 l O 0'.");
            }*/
        }
        if(NOT ERR.ErrorFound) {
            if(Compare(FORM.User_Password1,FORM.User_Password2) NEQ 0) {
                ERR.ErrorFound = true;
                ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Passwords do not match.");
            } 
        }
    </cfscript>
</cfsilent>

<cfif StructKeyExists(SESSION,"registerAttempts")>
	<cfif SESSION.registerAttempts GTE 5>
		<cfset regAttemptsMessage = "*This user has exeeded the Customer ID registration limit. The user entered an invalid Customer ID more than 5 times during registration. Please contact the customer for additional verification.">
	</cfif>
</cfif>

<cfif Len(FORM.User_Cust_ID) NEQ 0>
    <cfquery name="qry_checkCustID" datasource="#APPLICATION.Datasource_AX#">
        SELECT CUSTOMERID
        FROM HMECPCUSTOMERLOOKUP
        WHERE CUSTOMERID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_Cust_ID#" list="yes">)
    </cfquery>
    
    <cfif qry_checkCustID.RecordCount NEQ ListLen(FORM.User_Cust_ID)>
    	 <cfset ERR.ErrorFound = true>
         <cfset ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Customer ID Invalid. One or more of the Customer IDs entered are invalid.")>
         <cfset SESSION.registerAttempts = SESSION.registerAttempts + 1>
    </cfif>
</cfif>

<cfif Len(FORM.User_EmailAddress) NEQ 0>
    <cfquery name="qry_checkUserEmail" datasource="#APPLICATION.Datasource#" maxrows="1">
        SELECT 1
        FROM ServicePortal.tbl_Users
        WHERE User_EmailAddress = <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_EmailAddress#">
    </cfquery>
    
    <cfif qry_checkUserEmail.RecordCount NEQ 0>
    	 <cfset ERR.ErrorFound = true>
         <cfset ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Email Address is already in use.")>
    </cfif>
</cfif>

<cfif ERR.ErrorFound>
	<cfinclude template="./_tmp_login_create.cfm">
	<cfexit method="exittemplate">
</cfif>

<cfscript>
	FORM.User_UID = '';
	TempPassword = '';
	
	//Get User_UID
	getUUID = CreateObject("component", "_cfcs.Generate_UID");
	FORM.User_UID = getUUID.genUID(Length=32);
	
	InvokeCrypto = CreateObject("component", "_cfcs.secHash");
	FORM.User_PasswordSalt = InvokeCrypto.genSalt();
	FORM.User_PasswordHash = InvokeCrypto.computeHash(FORM.User_Password1, FORM.User_PasswordSalt);
	//FORM.User_PasswordAnswerHash = InvokeCrypto.computeHash(FORM.User_PasswordAnswer, FORM.User_PasswordSalt);
</cfscript>

<cfquery name="qry_InsertNewUser" datasource="#APPLICATION.Datasource#">
	INSERT INTO [ServicePortal].[tbl_Users]
	(
		[User_UID],
		[User_Cust_ID],
		[User_IsVerified],
		[User_IsActive],
		[User_FirstName],
		[User_LastName],
		[User_EmailAddress],
		[User_Address_Line1],
		[User_Address_Line2],
		[User_Phone_Number],
		[User_Locality],
		[User_Region],
		[User_PostCode],
		[User_Country],
		[User_Company],
		[User_CompanyName],
		[User_PasswordHash],
		[User_PasswordSalt],
		[User_Created_DTS],
		[User_Created_By],
        [User_Brand]
	)
	VALUES 
	(
    	<cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_UID#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_Cust_ID#">,
        <cfqueryparam cfsqltype="cf_sql_integer" value="0">,
        <cfqueryparam cfsqltype="cf_sql_integer" value="0">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_FirstName#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_LastName#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_EmailAddress#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_Address_Line1#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_Address_Line2#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_Phone_Number#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_Locality#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_Region#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_PostCode#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_Country#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_Company#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_CompanyName#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_PasswordHash#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_PasswordSalt#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#CurrentDateTime#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.REMOTE_ADDR#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_Brand#">
	)
</cfquery>

<!--- Add switch statement for "to" email address list based on Company --->


<!------------------------------------------------------------------------->

<!--- Email to HME --->
<cfmail from="no-reply@hme.com" to="#APPLICATION.HME_Email#" bcc="Web_Support@hme.com" subject="Customer Service Portal: New User Login Request" type="html">
	<p>This user has requested access to the Customer Service Portal: #FORM.User_EmailAddress#</p>
	<!---<p>Please click on the link below or paste it into your browser to <strong style="color:green;">Accept</strong> or <strong style="color:red;">Decline</strong> this users request:</p>
    <p><a href="http://#APPLICATION.rootURL#/?pg=Login&st=Verify&uuid=#FORM.User_UID#">http://#APPLICATION.rootURL#/?pg=Login&st=Verify&uuid=#FORM.User_UID#</a></p>--->
    <p>Please login to the Customer Service Portal and browse to the "Pending Users" page to Accept or Decline this users request.</p>
    <p>#APPLICATION.rootURL#/?pg=Verify</p>
    <cfif Len(regAttemptsMessage) NEQ 0>
    	<p style="font-style:italic;">#regAttemptsMessage#</p>
    </cfif>
    <p>Thank you</p>
</cfmail>

<!--- Email to Customer --->
<cfmail from="no-reply@hme.com" to="#FORM.User_EmailAddress#" bcc="Web_Support@hme.com" subject="Customer Service Portal: Registration Notification" type="html">
	<p>Registration Complete!</p>
    <p>Your request is now being processed.</p>
    <p>You will be notified within 1-2 business days regarding your account access.</p>
    <p>Thank you</p>
</cfmail>

<div class="col-lg-12">
	<h2>Registration</h2>
</div>
<div class="col-lg-12">
    <div class="form-message form-success">
        <ul>
            <li>Registration Complete! You will be notified within 1-2 business days regarding your account access. Thank you.</li>
        </ul>
    </div>
</div>


