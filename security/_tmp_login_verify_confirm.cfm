<cfparam name="FORM.User_Submit" default="Decline">
<cfparam name="FORM.User_UID" default="">
<cfparam name="FORM.User_Cust_ID" default="">
<cfparam name="FORM.User_Type" default="Customer">
<cfparam name="FORM.User_Role" default="4">
<cfparam name="FORM.User_Company" default="">
<cfparam name="FORM.User_Select_Apps" default="">
<cfparam name="FORM.Apply_Credit" default="0">
<cfparam name="FORM.Apply_Deposit" default="0">

<cfsilent>
	<cfscript>
		if(Len(FORM.User_Cust_ID) EQ 0 AND FORM.User_Submit EQ "Accept") {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Customer ID may not be blank.");
        }
		if(Len(FORM.User_Company) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "User Company may not be blank.");
        }
		if(Len(FORM.User_Select_Apps) EQ 0) {
            ERR.ErrorFound = true;
            ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "User Applications may not be blank.");
        }
		if(FORM.Apply_Credit EQ "on" OR FORM.Apply_Credit EQ 1) {
            FORM.Apply_Credit = 1;
        } else {
			FORM.Apply_Credit = 0;
		}
		if(FORM.Apply_Deposit EQ "on" OR FORM.Apply_Deposit EQ 1) {
            FORM.Apply_Deposit = 1;
        } else {
			FORM.Apply_Deposit = 0;
		}
		if(FORM.User_Type EQ "Full Admin"){
			FORM.Apply_Deposit = 1;
			FORM.Apply_Credit = 1;
		}
	</cfscript>
</cfsilent>

<cfif Len(FORM.User_Cust_ID) NEQ 0>  
    <cfquery name="qry_checkCustID" datasource="#APPLICATION.Datasource_AX#">
        SELECT CUSTOMERID
        FROM HMECPCUSTOMERLOOKUP
        WHERE CUSTOMERID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_Cust_ID#" list="yes">)
    </cfquery>
    
    <cfif qry_checkCustID.RecordCount NEQ ListLen(FORM.User_Cust_ID)>
    	 <cfset ERR.ErrorFound = true>
         <cfset ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Customer ID Invalid. One or more of the Customer IDs entered are invalid.")>
    </cfif>
</cfif>

<cfif ERR.ErrorFound>
	<cfinclude template="./_tmp_login_verify.cfm">
	<cfexit method="exittemplate">
</cfif>

<cfif FORM.User_Submit EQ "Accept">
	<cfquery name="qry_UpdateUserInfo" datasource="#APPLICATION.Datasource#">
    	UPDATE [ServicePortal].[tbl_Users]
        SET    [User_Cust_ID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_Cust_ID#">,
        	   [User_IsVerified] = <cfqueryparam cfsqltype="cf_sql_integer" value="1">,
               [User_IsActive] = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
        WHERE  [User_UID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.User_UID#">
    </cfquery>
    
    <cfscript>
		getCompanyID = getUserPermissions.getCompanyIDByName(FORM.User_Company);
		getApplicationID = getUserPermissions.getApplicationIDByName(FORM.User_Select_Apps);
		setUserPermission = getUserPermissions.createUserPermission(qry_getUserInfo.User_ID,FORM.User_Role,0,0,FORM.Apply_Credit,FORM.Apply_Deposit,CGI.REMOTE_ADDR);
		setUserCompanies = getUserPermissions.createCompanyUser(qry_getUserInfo.User_ID,ValueList(getCompanyID.Company_ID));
		setUserApplications = getUserPermissions.createApplicationUser(qry_getUserInfo.User_ID,ValueList(getApplicationID.App_ID));
	</cfscript>

    <cfmail from="no-reply@hme.com" to="#FORM.User_EmailAddress#" bcc="Web_Support@hme.com" subject="Customer Service Portal: New User Login Confirmation" type="html">
        <p>Please click the link below to login to your account:</p>
        <p>http://#APPLICATION.rootURL#/?pg=Login</p>
        <p>Your username is: <strong style="color:green;">#FORM.User_EmailAddress#</strong></p>
        <p>Your password will be the same password you entered while registering. If you have forgotten your password, please click the link below to request a new one:</p>
        <p>http://#APPLICATION.rootURL#/?pg=ManageAccount&st=rq</p>
        <p>Thank You</p>
    </cfmail>
    
    <div class="col-lg-12">
    	<h2>Registration Confirmation</h2>
    </div>
    <div class="col-lg-12">
        <div class="form-message form-success animated fadeIn">
            <ul>
                <li>The user <strong><cfoutput>#FORM.User_EmailAddress#</cfoutput></strong> has been activated.</li>
            </ul>
        </div>
    </div>
<cfelse>
	<!--- Send email to customer notifying them that there request has been DENIED --->
    <cfmail from="no-reply@hme.com" to="#FORM.User_EmailAddress#" bcc="Web_Support@hme.com" subject="Customer Service Portal: New User Login Denied" type="html">
    	<p>Your registration request has been Denied.</p>
        <p>Please try your request again or contact customer service for further assistance.</p>
        <p>http://#APPLICATION.rootURL#/?pg=Login</p>
        <p>Thank You</p>
    </cfmail>
    
    <div class="col-lg-12">
    	<h2>Registration Confirmation</h2>
    </div>
    <div class="col-lg-12">
        <div class="form-message form-error">
            <ul>
                <li>The user <strong><cfoutput>#FORM.User_EmailAddress#</cfoutput></strong> has not been activated.</li>
            </ul>
        </div>
    </div>
</cfif>