<cfsilent>
	<cfif Len(URL.ruid) EQ 32 AND ReFindNoCase("([0-9A-Z]){32}", URL.ruid) GT 0>
        <cfscript>
            QUERY = StructNew();
            QUERY.QueryName = "qry_checkReset";
            QUERY.Reset_UID = URL.ruid;
        </cfscript>
        <cfinclude template="../_qry_select_reset.cfm">
        
        <cfif qry_checkReset.RecordCount EQ 0>
            <cfset ERR.ErrorFound = true>
            <cfset ERR.ErrorMessage = ListAppend(ERR.ErrorMessage,"Password link expired. Please try your request again.")>
        <cfelse>
            <!---Reset Application In Case of Logins Exceeded--->
            <cfif StructClear(SESSION)></cfif>
            <cfset THIS.OnApplicationStart()>
        </cfif>
    <cfelse>
        <cfset ERR.ErrorFound = true>
        <cfset ERR.ErrorMessage = ListAppend(ERR.ErrorMessage,"Password link invalid. Please try your request again.")>
    </cfif>
</cfsilent>

<cfoutput>
	<cfif ERR.ErrorFound>
        <div>
            <ul>
                <cfloop list="#ERR.ErrorMessage#" index="thisError"><li>#thisError#</li></cfloop>
            </ul>
        </div>
    </cfif>

	<cfif IsDefined("ERR.ErrorMessage") AND ERR.ErrorMessage CONTAINS "link">
        <cfset badlink = true>
    <cfelse>
        <cfset badlink = false>
    </cfif>
    <cfif NOT badlink>
		<form action="./?pg=ManageAccount&st=rc&ruid=#URL.ruid#" method="POST" class="form-horizontal col-lg-12" role="form">
        	<div class="col-lg-1"></div>
        	<div class="col-lg-6">
        		<div class="form-group">
                    <label for="Password1" class="control-label col-lg-5">New Password:</label>
                    <div class="col-lg-7">
                    	<input type="password" maxlength="16" name="Password1" class="form-control" value="" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="Password2" class="control-label col-lg-5">Confirm Password:</label>
                    <div class="col-lg-7">
                    	<input type="password" maxlength="16" name="Password2" class="form-control" value="" />
                    </div>
                </div>
               <div class="form-group">
               		<div class="col-lg-5"></div>
                    <div class="col-lg-7">
                    	<input type="submit" value="Reset Password" class="btn btn-primary form-control"/>
                    </div>
                </div>
            </div>
			<div class="col-lg-6">
                <p>Password Requirements</p> 
                <ul>
                    <li>The length of the password must be at least eight (8) characters and not more than sixteen (16) characters.</li>
                    <li>The password may not contain blank spaces.</li>
                    <li>The password must contain at least one (1) character from each of the three (3) groups below:
                        <ul>
                            <li>English uppercase characters (A through Z)</li>
                            <li>English lowercase characters (a through z)</li>
                            <li>Numerals (0 through 9)</li>
                        </ul>
                    </li>
                </ul>
			</div>
            
		</form>
	</cfif>
</cfoutput>