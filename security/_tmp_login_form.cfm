<cfparam name="NOLOGIN" default="false">

<cfoutput>

    <cfif StructKeyExists(SESSION, "loginAttempts")>
        <cfif SESSION.loginAttempts GTE LOGIN.maxAttempts>
            <cfset NOLOGIN = true>
            <cfif StructKeyExists(SESSION, "beginTimeout")>
                <cfif DateDiff("n",SESSION.beginTimeout,Now()) GTE 20>
                    <!---If User Has Been Locked out longer than 20mins open login screen--->
                    <cfif StructDelete(SESSION, "beginTimeout")></cfif>
                    <cfset SESSION.loginAttempts = 0>
                    <cfset NOLOGIN = false>
                </cfif>
            <cfelse>
                <!---Begin Timeout Timer--->
                <cfset SESSION.beginTimeout = Now()>
            </cfif>
        </cfif>
    <cfelse>
        <cfset SESSION.loginAttempts = 0>
    </cfif>
    
    <!---<cfif StructKeyExists(SESSION, "loginAttempts") AND SESSION.loginAttempts GT 0>
        <div>
            <h4>Login attempts (#SESSION.loginAttempts#)#LOGIN.maxAttempts#</h4>
        </div>
    </cfif>--->

    <cfif NOLOGIN>
        <div class="col-lg-9 col-centered col-login-exceed text-center">
            <h4>Login attempts exceeded please try your request at a later time. Thank you.</h4>
        </div>
    <cfelse>
        <form action="./?pg=Login&st=Validate" class="form-horizontal" method="POST">
			<cfif ERR.ErrorFound>
                <div class="col-lg-9 col-centered col-login-error animated fadeIn">
                    <div class="form-message form-error">
                        <ul>
                            <cfloop list="#ERR.ErrorMessage#" index="thisError"><li>#thisError#</li></cfloop>
                        </ul>
                    </div>
                </div>
            </cfif>
        
            <div class="col-lg-9 col-centered container-login <cfif NOT ERR.ErrorFound>animated fadeInDown</cfif>">
                <div class="col-lg-6 col-login-left">
                    <div class="form-group">
                        <div class="brand-login-sm hidden-lg col-md-12">
                            <img src="./images/HME-Logo-large.png" alt="HME" width="70" /><span>CSP</span>
                        </div>
                        <div class="col-lg-12 text-center">
                            <h4>New Customers</h4>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-4 col-md-4 col-sm-4"></div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <a href="./?pg=Login&st=Create" class="btn btn-default form-control">Register</a>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12 register-phone text-center">
                            <p><strong>Register by phone at 800.848.4468</strong></p>
                        </div>
                    </div>
                    <div class="form-group visible-lg" style="margin-bottom:0px;">
                        <div class="col-lg-12 security-warning">
                            <p><strong>Security Warning:</strong><br />It is your responsibility to maintain the confidentiality and control of passwords used to access data through our website. For your protection, we strongly recommend that you do not provide your account information, user name, or password to anyone. If you become aware of any suspicious activity relating to your account, please contact us immediately.</p>
                            <p>To protect access to your information we recommend using at least eight characters, two of which should be special characters or numbers. Change your password frequently and especially after employees leave your company. Do not leave your computer unattended while logged in and log out when finished.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-login-right">
                    <div class="form-group visible-lg">
                    	<div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <div class="brand-login">
                            	<img src="./images/HME-Logo-large.png" alt="HME" width="70" /><span>Customer Service Portal</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Username" class="control-label col-lg-3 col-no-pad-r">Email</label>
                        <div class="col-lg-8">
                            <input type="text" maxlength="100" name="Username" class="form-control" value="" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Password" class="control-label col-lg-3 col-no-pad-r">Password</label>
                        <div class="col-lg-8">
                            <input type="password" maxlength="16" name="Password" class="form-control" value="" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-3"></div>
                        <div class="col-lg-6">
                            <input type="submit" value="Login" class="btn btn-primary form-control btn-submit"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12 text-center">
                            <a href="./?pg=ManageAccount&st=rq">Forgot your password?</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </cfif>
</cfoutput>