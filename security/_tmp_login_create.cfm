  
    <div class="col-lg-12">
    	<h2>Registration</h2>
    </div>
    
    <!--- Error Message --->
    <cfif ERR.ErrorFound>
    	<cfoutput>
            <div class="col-lg-12">
                <div class="form-message form-error">
                    <ul>
                        <cfloop list="#ERR.ErrorMessage#" index="thisError"><li>#thisError#</li></cfloop>
                    </ul>
                </div>
            </div>
        </cfoutput>
    </cfif>

	<!--- User Registration Form --->
    <form action="./?pg=Login&st=CreateConfirm" method="POST" class="form-horizontal col-lg-11 form-marg" role="form">
    	<div class="col-lg-6">
        	<cfoutput>
                <div class="form-group">
                    <label for="User_Company" class="control-label col-lg-6">Company:<span class="red-text">*</span></label>
                    <div class="col-lg-6">
                        <select name="User_Company" id="User_Company" class="companies" multiple>
                          <option value="" disabled selected>Choose your option</option>
                          <cfloop from="1" to="#getUserCompanies.RecordCount#" index="Company">
                              <option value="#getUserCompanies.Company_Code[Company]#"> #getUserCompanies.Company_Name[Company]#</option>
                          </cfloop>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="User_Cust_ID" class="control-label col-lg-6">Customer ID:</label>
                    <div class="col-lg-6">
                        <cfif StructKeyExists(SESSION, "registerAttempts") AND SESSION.registerAttempts GT 4>
                            <input type="text" maxlength="100" name="User_Cust_ID" class="form-control" value="" disabled="disabled" />
                            <span class="error-field">You have exceeded the limit.</span>
                        <cfelse>
                            <input type="text" maxlength="100" name="User_Cust_ID" class="form-control" value="#FORM.User_Cust_ID#" />
                            <span class="note-field">Use commas for multiple IDs: (Ex: 123,456,789)</span>
                        </cfif>
                    </div>
                </div>
                <div class="form-group">
                    <label for="User_CompanyName" class="control-label col-lg-6">Company Name:<span class="red-text">*</span></label>
                    <div class="col-lg-6">
                        <input type="text" maxlength="100" name="User_CompanyName" class="form-control" value="#FORM.User_CompanyName#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="User_Brand" class="control-label col-lg-6">Company Brand:</label>
                    <div class="col-lg-6">
                        <input type="text" maxlength="100" name="User_Brand" class="form-control" value="#FORM.User_Brand#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="User_Address_Line1" class="control-label col-lg-6">Company Address 1:<span class="red-text">*</span></label>
                    <div class="col-lg-6">
                        <input type="text" maxlength="100" name="User_Address_Line1" class="form-control" value="#FORM.User_Address_Line1#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="User_Address_Line2" class="control-label col-lg-6">Company Address 2:</label>
                    <div class="col-lg-6">
                        <input type="text" maxlength="100" name="User_Address_Line2" class="form-control" value="#FORM.User_Address_Line2#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="User_Locality" class="control-label col-lg-6">Company City/Locality:<span class="red-text">*</span></label>
                    <div class="col-lg-6">
                        <input type="text" maxlength="100" name="User_Locality" class="form-control" value="#FORM.User_Locality#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="User_Region" class="control-label col-lg-6">Company State/Region:<span class="red-text">*</span></label>
                    <div class="col-lg-6">
                        <input type="text" maxlength="100" name="User_Region" class="form-control" value="#FORM.User_Region#" />
                    </div>
                </div>
            </cfoutput>
        </div>
        <div class="col-lg-6">
        	<div class="form-group">
                <label for="User_Country" class="control-label col-lg-6">Company Country:<span class="red-text">*</span></label>
                <div class="col-lg-6">
                	<select name="User_Country" class="form-control">
                    	<cfoutput query="qry_getCountries">
                        	<option value="#COUNTRYNAME#" <cfif COUNTRYNAME EQ "United States">selected="selected"</cfif>>#COUNTRYNAME#</option>
                        </cfoutput>
                    </select>
                </div>
            </div>
            <cfoutput>
                <div class="form-group">
                    <label for="User_PostCode" class="control-label col-lg-6">Company Postal Code:<span class="red-text">*</span></label>
                    <div class="col-lg-6">
                        <input type="text" maxlength="100" name="User_PostCode" class="form-control" value="#FORM.User_PostCode#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="User_EmailAddress" class="control-label col-lg-6">Email Address:<span class="red-text">*</span></label>
                    <div class="col-lg-6">
                        <input type="text" maxlength="100" name="User_EmailAddress" class="form-control" value="#FORM.User_EmailAddress#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="User_FirstName" class="control-label col-lg-6">First Name:<span class="red-text">*</span></label>
                    <div class="col-lg-6">
                        <input type="text" maxlength="100" name="User_FirstName" class="form-control" value="#FORM.User_FirstName#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="User_LastName" class="control-label col-lg-6">Last Name:<span class="red-text">*</span></label>
                    <div class="col-lg-6">
                        <input type="text" maxlength="100" name="User_LastName" class="form-control" value="#FORM.User_LastName#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="User_Phone_Number" class="control-label col-lg-6">Phone Number:<span class="red-text">*</span></label>
                    <div class="col-lg-6">
                        <input type="text" maxlength="100" name="User_Phone_Number" class="form-control" value="#FORM.User_Phone_Number#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="User_Password1" class="control-label col-lg-6">Password:<span class="red-text">*</span></label>
                    <div class="col-lg-6">
                        <input type="password" maxlength="100" name="User_Password1" class="form-control" value="" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="User_Password2" class="control-label col-lg-6">Password Confirm:<span class="red-text">*</span></label>
                    <div class="col-lg-6">
                        <input type="password" maxlength="100" name="User_Password2" class="form-control" value="" />
                    </div>
                </div>
            </cfoutput>
        </div>
        
        <div class="col-lg-12">&nbsp;</div>
        <div class="col-lg-12">
        	<div class="form-group">
            	<div class="col-lg-5"></div>
                <div class="col-lg-3">
                	<input type="submit" value="Register" class="btn btn-primary form-control" />
                </div>
            </div>
        </div>
    </form>