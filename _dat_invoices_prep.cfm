<cfparam name="FORM.CSP_Page_Form" default="">
<cfparam name="FORM.CSP_Per_Page" default="5">
<cfparam name="FORM.CSP_Current_Page" default="1">
<cfparam name="VARIABLES.Page_Start" default="1">
<cfparam name="VARIABLES.Page_End" default="5">

<cfparam name="FORM.Cust_Number_Anchor" default="">
<cfparam name="FORM.SO_Number_Anchor" default="">
<cfparam name="FORM.INV_Number_Anchor" default="">

<cfparam name="FORM.Cust_Number" default="#FORM.Cust_Number_Anchor#">
<cfparam name="FORM.SO_Number" default="">
<cfparam name="FORM.INV_Number" default="">
<cfparam name="FORM.INV_Date_From" default="">
<cfparam name="FORM.INV_Date_To" default="">
<cfparam name="FORM.INV_Company" default="hsc">
<cfparam name="FORM.INV_State" default="">

<cfif LEN(FORM.Cust_Number) NEQ 0>
	<cfset FORM.Cust_Number_Anchor = FORM.Cust_Number>
</cfif>

<cfscript>
	InvoicesInfoObj = CreateObject("component", "_cfcs.OrderInfo");
	InvoicesCount = DeserializeJSON(InvoicesInfoObj.GetInvoicesCount(SESSION.User_UID,FORM.Cust_Number,FORM.SO_Number,FORM.INV_Number,FORM.INV_Date_From,FORM.INV_Date_To,FORM.INV_Company,FORM.INV_State));
	InvoicesList = DeserializeJSON(InvoicesInfoObj.GetInvoicesList(SESSION.User_UID,FORM.CSP_Current_Page,FORM.CSP_Per_Page,FORM.Cust_Number,FORM.SO_Number,FORM.INV_Number,FORM.INV_Date_From,FORM.INV_Date_To,FORM.INV_Company,FORM.INV_State));
	
	VARIABLES.Page_Start = ((FORM.CSP_Current_Page-1)*FORM.CSP_Per_Page)+1;
	VARIABLES.Page_End = FORM.CSP_Current_Page*FORM.CSP_Per_Page;
</cfscript>
