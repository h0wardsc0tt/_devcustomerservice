<cfparam name="SuccessMsg" default="">
<cfparam name="URL.uuid" default="">
<cfparam name="FORM.User_UID" default="#URL.uuid#">
<cfparam name="FORM.User_Company" default="">
<cfparam name="FORM.User_Company_Main" default="">
<cfparam name="FORM.User_Select_Apps" default="">
<cfparam name="FORM.Apply_Credit" default="0">
<cfparam name="FORM.Apply_Deposit" default="0">

<cfscript>
	QUERY = StructNew(); //Only retrieve user information for valid sessions
	QUERY.QueryName = "qry_getSingleUserInfo";
	QUERY.Datasource = APPLICATION.Datasource;
	QUERY.User_UID = FORM.User_UID;
</cfscript>
<cfinclude template="./_qry_select_users.cfm">

<cfquery name="qry_getFields" datasource="#APPLICATION.Datasource#">
    SELECT COLUMN_NAME, DATA_TYPE, COLUMN_DEFAULT
    FROM INFORMATION_SCHEMA.COLUMNS 
    WHERE TABLE_NAME IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="tbl_Users" list="yes">)
</cfquery>

<cfset thisCount = 1>
<cfloop list="#ValueList(qry_getFields.COLUMN_NAME)#" index="thisField">
    <cfset defaultValue = ReReplaceNoCase(qry_getFields.COLUMN_DEFAULT[thisCount], "[(|\'|)]", "", "all")>
    <cfparam name="FORM.#thisField#" default="#Evaluate('qry_getSingleUserInfo.#thisField#')#">
    <cfset thisCount = thisCount+1>
</cfloop>

<cfquery name="qry_getCountries" datasource="#APPLICATION.Datasource_AX#">
	SELECT [COUNTRYNAME]
    FROM [HMECPCOUNTRIESLOOKUP]
    ORDER BY [COUNTRYNAME]
</cfquery>

<cfscript>
	getUserPermissions = CreateObject("component", "_cfcs.UserPermission");
	getUserTypes = getUserPermissions.getAllUserType();
	getUserTypeRole = getUserPermissions.getPermissionByUserID(qry_getSingleUserInfo.User_ID);
	getUserTypeRoles = getUserPermissions.getUserRoleByType(getUserTypeRole.UserType_Name);
	getUserApplications = getUserPermissions.getApplicationByType(getUserTypeRole.UserType_Name);
	getUserApplication = getUserPermissions.getApplicationByUserID(qry_getSingleUserInfo.User_ID);
	getUserCompanies = getUserPermissions.getAllCompanies();
	getUserCompany = getUserPermissions.getCompanyByUserID(qry_getSingleUserInfo.User_ID);
</cfscript>