<cfparam name="SuccessMsg" default="">

<cfquery name="qry_getFields" datasource="#APPLICATION.Datasource#">
    SELECT COLUMN_NAME, DATA_TYPE, COLUMN_DEFAULT
    FROM INFORMATION_SCHEMA.COLUMNS 
    WHERE TABLE_NAME IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="tbl_Users" list="yes">)
</cfquery>

<cfset thisCount = 1>
<cfloop list="#ValueList(qry_getFields.COLUMN_NAME)#" index="thisField">
    <cfset defaultValue = ReReplaceNoCase(qry_getFields.COLUMN_DEFAULT[thisCount], "[(|\'|)]", "", "all")>
    <cfparam name="FORM.#thisField#" default="#Evaluate('qry_getUserInfo.#thisField#')#">
    <cfset thisCount = thisCount+1>
</cfloop>

<cfscript>
	getUserPermissions = CreateObject("component", "_cfcs.UserPermission");
	getUserCompanies = getUserPermissions.getAllCompanies();
</cfscript>