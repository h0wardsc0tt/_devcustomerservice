
<cfoutput>
	<!--- Product Detail --->
    <div class="col-lg-12 product-head-details">
        <div class="col-lg-6 no-pad">
            <h2>#ProductDetails.DATA.NAMEALIAS[1]#</h2>
            <div class="product-part"><strong>Part Number: </strong> #ProductDetails.DATA.ITEMID[1]#</div>
        </div>
    </div>
    <div class="col-lg-12 no-pad product-cont product-detail-cont">      
    
    	<!--- Product Image --->	
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="col-lg-12 product-image">
                <a href="" data-toggle="modal" data-target="##Product_Image_Modal"><img src="./images/no-image.jpg" class="animated fadeIn" alt="No Image Available" /></a>
            </div>
        </div>
        
        <!--- Product Description --->
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </div>
        
        <!--- Product Type/Qty --->
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
        	<form action="./?pg=Cart" name="Product_Add_Cart" id="Product_Add_Cart" method="POST">
                <table class="table product-detail-type">
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr>
                        <td class="product-type-radio"><input type="radio" name="Product_Detail_Radio" id="Product_Detail_Radio" class="with-gap" checked="checked" /><label for="Product_Detail_Radio"></label></td>
                        <td><span class="product-type-span">NEW</span><br /><strong>$6.00</strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="product-type-radio"><input type="radio" name="Product_Detail_Radio" id="Product_Detail_Radio2" class="with-gap" /><label for="Product_Detail_Radio2"></label></td>
                        <td><span class="product-type-span">REFURBISHED</span><br /><strong>$6.00</strong></td>
                        <td><a href="" style="text-decoration:none;font-size:12px;">Details</a></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">
                            <span>
                                Quantity
                                <select id="Product_Detail_Qty" name="Product_Detail_Qty">
                                    <cfloop from="1" to="100" index="qty">
                                        <option value="#qty#">#qty#</option>
                                    </cfloop>
                                </select>
                           </span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="product-add-cart">
                        	<button type="submit" class="btn btn-primary" id="Product_Detail_Add" name="Product_Detail_Add">Add to Cart</button>
                            <input type="hidden" name="Product_Detail_UID" id="Product_Detail_UID" value="#ProductDetails.DATA.DAXINTEGRATIONKEY[1]#" />
                        </td>
                    </tr>
                </table>
			</form>
        </div>
    </div>
    
    <!--- Product Image Modal --->
    <div id="Product_Image_Modal" class="modal fade" role="dialog">
		<div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="modal-close">
                            <span data-dismiss="modal">X</span>
                        </div>
                        <div class="modal-img">
                            <img src="./images/no-image.jpg" class="animated fadeIn" alt="No Image Available"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</cfoutput>