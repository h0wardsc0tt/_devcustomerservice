<!--- Generate a clean feed by suppressing white space and debugging information. --->
<cfprocessingdirective suppresswhitespace="yes">
<cfsetting showdebugoutput="no">
<!--- 
	Author: wwang (2014-08-08)
	
	The purpose of this template is to allow front end pages (usually Javascript) to make local calls to built-in CFC's. 
	We use this to hide CFC directory from being exposed to the front end pages, as well as avoid CFC methods exposed 
	as remote service. So only local calls are needed.
	  
	Any call to proxy.cfm page is expected to contain two groups of parameters:
	
	URL params - it may contain the following parameters:
		1. service - (required) this would be the name of the cf component
		2. method - (required) this would be the function within the component
		3. returnFormat - (optional) only two values are supported: "json" or "plain". If left out, pain text is returned
		
	FORM params - the arguments needed by a function call are passed in through form variables
		IMPORTANT: each name of form variables must match the name of argument in the function it's calling
	
	Results are returned as javascript data back to the calling page. Numbers or text are returned as plain text, while
	complex objects such as query will be returned as json format.
	
	Example:
	function fetchAudio(a_id, u_id, audio_uid, this_obj){
		jQuery.ajax({ 
			type: 'POST', // data are posted to server as form variables
			url: './proxy.cfm?service=audio&method=createAudioFile&returnFormat=plain', // those params are passed as URL variables
			data: {audio_id: a_id, account_id: u_id},  // the name of each data field should mactch the argument in the cfc method
			success: function() {
				....
			} 
		});
	};
--->
<cfcontent type="application/x-javascript">

<!--- checking for required URL params --->
<cfif NOT isDefined('url.service')>
	<cfheader statuscode="500" statustext="Missing service name">
	<h1>This proxy requires a url variable named "service" containing parameters</h1><cfabort>
</cfif>

<cfif NOT isDefined('url.method')>
	<cfheader statuscode="500" statustext="Missing form request">
	<h1>This proxy requires a url variable named "method" containing parameters</h1>
</cfif>

<cfscript>
	requestFormParams = FORM;
	requestUrlParams = URL;
	// dynamically create an instance of the service
	serviceObj = CreateObject('component', '_cfcs.#requestUrlParams.service#');
	// dynamically make the requested function call
	returnResult = Evaluate("serviceObj.#requestUrlParams.method#(argumentCollection=requestFormParams)");
	
	// the return format can be either json or plain
	switch(requestUrlParams.returnFormat){
		case "json": 
			outputResult = SerializeJSON(returnResult);
			break;
		default: // return plain text by default
			outputResult = returnResult;
	}
	
	writeOUtput(outputResult);
</cfscript>
</cfprocessingdirective>