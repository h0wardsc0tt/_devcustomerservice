<cfparam name="QUERY" default="#StructNew()#">
<cfparam name="QUERY.QueryName" default="qry_getAll">
<cfparam name="QUERY.Datasource" default="#APPLICATION.Datasource#">
<cfparam name="QUERY.SelectSQL" default="usrs.*">
<cfparam name="QUERY.OrderBy" default="usrs.User_LastName,usrs.User_FirstName">

<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#">
	SELECT #QUERY.SelectSQL#
	FROM 
		ServicePortal.tbl_Users usrs
	WHERE 0=0
	<cfif StructKeyExists(QUERY,"User_ID")>
	AND usrs.User_ID IN (<cfqueryparam cfsqltype="cf_sql_integer" value="#QUERY.User_ID#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY,"User_UID")>
	AND usrs.User_UID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.User_UID#" list="yes">)
	</cfif>
    <cfif StructKeyExists(QUERY,"User_Cust_ID")>
	AND usrs.User_Cust_ID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.User_Cust_ID#" list="yes">)
	</cfif>
    <cfif StructKeyExists(QUERY,"User_FirstName")>
	AND usrs.User_FirstName IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.User_FirstName#" list="yes">)
	</cfif>
    <cfif StructKeyExists(QUERY,"User_LastName")>
	AND usrs.User_LastName IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.User_LastName#" list="yes">)
	</cfif>
    <cfif StructKeyExists(QUERY,"User_EmailAddress")>
	AND usrs.User_EmailAddress IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.User_EmailAddress#" list="yes">)
	</cfif>
	ORDER BY #QUERY.OrderBy#
</cfquery>

<cfquery name="#QUERY.QueryName#_Total" dbtype="query">
	SELECT Count(*) AS Records
	FROM #QUERY.QueryName#
</cfquery>
<cfif StructClear(QUERY)></cfif>