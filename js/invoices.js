/* Set Pagination Defaults */
var Records_Per_Page = 5;
var Page_Start = 0;
var Page_End = Records_Per_Page;
var Paginated = false;

$(function() {
	
	 $('span.invoice_frame').empty();

	/* Datepicker Defaults */
	$( "#INV_Date_From" ).click(function(e) {
        $(this).datepicker('setDate', null);
    });
	
	$( "#INV_Date_To" ).click(function(e) {
        $(this).datepicker('setDate', null);
    });
	
	$( "#INV_Date_From" ).datepicker({
		onSelect: function(){
			if(!$("#INV_Date_To").val().length){
				$("#INV_Date_To").val($(this).val());
			}
		}	
	});
	
	$( "#INV_Date_To" ).datepicker({
		onSelect: function(){
			if(!$("#INV_Date_From").val().length){
				$("#INV_Date_From").val($(this).val());
			}
		}	
	});
	/* Datepicker Defaults */
	
	if($('#Cust_Number_Anchor').val().length || $('#SO_Number_Anchor').val().length){
		getInvoices();
	}
	
	$("#INV_Search_Form").submit(function(e){
        e.preventDefault();
		
		var form_empty = true;
		
		$('#INV_Search_Form *').filter(':input').each(function(){
			if($(this).attr('type') != "submit" && $(this).attr('type') == "text"){
				if($(this).val().length > 0) {
					form_empty = false;
				} 
			} 
		});
		
		if(!form_empty){
			$('#SO_Number_Anchor').val("");
			getInvoices();
		} else {
			$('#searchMessage').modal("show");
		}
		
    });

	if($('#Cust_Number_Anchor').val().length){
		$('.cust-field').hide();
		$('.no-cust-field').show();
	} else {
		$('.no-cust-field').hide();	
		$('.cust-field').show();
	}
	
	
	
});

function getInvoices(){
	var Cust_Number = "";
	var SO_Number = "";
	
	if($('#Cust_Number_Anchor').val().length){
		Cust_Number = $('#Cust_Number_Anchor').val();	
	} else {
		Cust_Number = $('#Cust_Number').val();	
	}
	
	if($('#SO_Number_Anchor').val().length){
		SO_Number = $('#SO_Number_Anchor').val();	
	}

	/* Set Invoice Defaults */
	var SESSION_User_UID = $('#SESSION_User_UID').val();
	var INV_Result_Rows = "";
	var INV_Number = $('#INV_Number').val();
	var INV_Date_From = $('#INV_Date_From').val();
	var INV_Date_To = $('#INV_Date_To').val();
	var INV_State = $('#INV_State').val();
	var INV_Company = $('#INV_Company').val();
	var IndexList = [];
	var payAmount = "";
	
	checkList = [];
	checkValue = '';
	checkAmounts = [];
	checkAmount = '';
	checkCusts = [];
	checkCust = '';
	
	/* Get Invoice Records */
	$.ajax({ 
		async: false,
		type: 'POST', 
		url: './proxy.cfm?service=OrderInfo&method=getInvoices&returnFormat=json', 
		data: {User_UID: SESSION_User_UID, Cust_Number: Cust_Number, SO_Number: SO_Number, INV_Number: INV_Number, INV_Date_From: INV_Date_From, INV_Date_To: INV_Date_To, INV_State: INV_State, INV_Company: INV_Company},
		cache: false, 
		dataType: 'json',
		success: function(invInfo) {
			var inv_temp = JSON.parse(invInfo);
			tempInvNumber = inv_temp.DATA.INVOICENUMBER;
			tempInvBal = inv_temp.DATA.INVOICEBALANCE;
			tempCustID = inv_temp.DATA.CUSTOMERID;
			tempInvDate = inv_temp.DATA.INVOICEDATE;
			tempPONumber = inv_temp.DATA.SALESORDERPONUMBER;
		}
	});
	
	$('#record-count').text("("+tempInvNumber.length+")");
	
	/* Get Invoice Payment Line Records */
	$.ajax({ 
		async: false,
		type: 'POST', 
		url: './proxy.cfm?service=OrderInfo&method=getInvoicePayments&returnFormat=json', 
		data: {Cust_Number: Cust_Number, SO_Number: SO_Number, INV_Number: INV_Number, INV_Date_From: INV_Date_From, INV_Date_To: INV_Date_To, INV_Company: INV_Company},
		cache: false, 
		dataType: 'json',
		success: function(payInfo) {
			var pay_temp = JSON.parse(payInfo);
			tempPayINV = pay_temp.DATA.INVOICENUMBER;
			tempPayAmount = pay_temp.DATA.SETTLEAMOUNTMST;
			tempPayBalance = pay_temp.DATA.INVOICEBALANCE;
			tempPayApplied = pay_temp.DATA.CREATEDBYUSER;
			tempPayDate = pay_temp.DATA.DATEOFSETTLEMENT;
		}
	});
	
	/* Clear Customer Table */
	$("table#INV_Results tbody").remove(); 
			
	/* Populate Customer Table */
	for(inv=Page_Start;inv<Page_End;inv++){
		if (tempInvNumber[inv] != undefined) {
			
			if(currencyFormat(tempInvBal[inv]) == "$0.00" || tempInvBal[inv] < 0){
				inv_state = 'disabled="disabled"';	
			} else {
				inv_state = '';
			}
			
			INV_Result_Rows += '<tr class="INV_Result_Rows INV_Result_Row_'+inv+'" id="'+inv+'" data-toggle="collapse" data-target=".inv-line-'+inv+'" onclick="updateStatus('+inv+');">'
			+'  <td><input type="checkbox" name="selectINV_'+tempInvNumber[inv]+'" id="selectINV_'+tempInvNumber[inv]+'" class="selectCHK selectCust_'+tempCustID[inv]+'" '+inv_state+'/></td>'
			+'	<td><a href="./?pg=Invoices&st=View&inv='+tempInvNumber[inv]+'" onclick="pauseStatus(\''+tempInvNumber[inv]+'\')">'+tempInvNumber[inv]+'</a></td>'
			+'	<td>'+tempCustID[inv]+'</td>'
			+'	<td class="selectAMT_'+tempInvNumber[inv]+'">'+currencyFormat(tempInvBal[inv])+'</td>'
			+'	<td>'+tempPONumber[inv]+'</td>'
			+'	<td>'+dateFormat(tempInvDate[inv])+'</td>'
			+'  <td style="text-align:right;"><span class="plus'+inv+' glyphicon glyphicon-plus-sign"></span></td>'
			+'</tr>';
			
			if($.inArray(tempInvNumber[inv], tempPayINV) !== -1){
			
				INV_Result_Rows += '<tr class="inv-row inv-row-'+inv+'">'
				+'	<td></td>'
				+'	<td></td>'
				+'	<td colspan="5">'
				+'		<div class="inv-line-'+inv+' collapse">'
				+'		<table class="table-striped">'
				+'			<thead>'
				+'			<tr>'
				+'				<th>LINE</th>'
				+'				<th>PAYMENT AMOUNT</th>'
				+'				<th>REMAINING BALANCE</th>'
				//+'				<th>PAYMENT APPLIED BY</th>'
				+'				<th>PAYMENT DATE</th>'
				+'			</tr>'
				+'			</thead>'
				+'			<tbody>';
				
				IndexList = getIndexList(tempPayINV, tempInvNumber[inv]);
				
				for(i=0;i<IndexList.length;i++){
					
					//payAmount =  '$' + parseFloat(tempPayAmount[IndexList[i]], 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
					//payBalance =  '$' + parseFloat(tempPayBalance[IndexList[i]], 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
				
					INV_Result_Rows += '<tr>'
					+'				<td>00'+(i+1)+'</td>'
					+'				<td>'+currencyFormat(tempPayAmount[IndexList[i]])+'</td>'
					+'				<td>'+currencyFormat(tempPayBalance[IndexList[i]])+'</td>'
					//+'				<td>'+tempPayApplied[IndexList[i]]+'</td>'
					+'				<td>'+dateFormat(tempPayDate[IndexList[i]])+'</td>'
					+'			</tr>';
				
				}
				
				IndexList = [];
				
				INV_Result_Rows += '</tbody>'
				+'		</table>'
				+'		</div>'
				+'	</td>'
				+'</tr>';
			}
		}
	}
	
	$('.no-records').remove();

	if (!tempInvNumber.length) {
		$("<p class='no-records'>No Records Found.</p>").insertAfter($('#INV_Results'));
		$('ul.pagination').hide();	
	} else {
		$('ul.pagination').show();	
	}

	$('<tbody>'+INV_Result_Rows+"</tbody>").insertAfter($('table thead#INV_Results_Header'));	
	
	/* Set to Page 1 for new search */
	if(Paginated){
		$('ul.pagination').pagination('selectPage', 1);
	}
	
	/* Generate Pagination */
	$('ul.pagination').pagination({
        items: tempInvNumber.length,
        itemsOnPage: Records_Per_Page,
		onPageClick: function(pageNumber) {
			
			getCheckedBoxes();
			
			/* Clear Customer Table */
			$("table#INV_Results tbody").remove(); 
			
			/* Set Pagination Settings */
			INV_Result_Rows = "";
			Page_Start = Records_Per_Page * (pageNumber - 1);
			Page_End = Page_Start + Records_Per_Page;
	
			/* Populate Customer Table */
			for(inv=Page_Start;inv<Page_End;inv++){
				if (tempInvNumber[inv] != undefined) {
					
					if($.inArray(tempInvNumber[inv], getCheckedBoxes()) !== -1){
						isChecked = 'checked="checked"';
					} else {
						isChecked = '';	
					}
					
					if(currencyFormat(tempInvBal[inv]) == "$0.00" || tempInvBal[inv] < 0){
						inv_state = 'disabled="disabled"';	
					} else {
						inv_state = '';
					}
			
					INV_Result_Rows += '<tr class="INV_Result_Rows INV_Result_Row_'+inv+'" id="'+inv+'" data-toggle="collapse" data-target=".inv-line-'+inv+'" onclick="updateStatus('+inv+');">'
					+'  <td><input type="checkbox" name="selectINV_'+tempInvNumber[inv]+'" id="selectINV_'+tempInvNumber[inv]+'" class="selectCHK selectCust_'+tempCustID[inv]+'" '+isChecked+' '+inv_state+'/></td>'
					+'	<td><a href="./?pg=Invoices&st=View&inv='+tempInvNumber[inv]+'" onclick="pauseStatus(\''+tempInvNumber[inv]+'\')">'+tempInvNumber[inv]+'</a></td>'
					+'	<td>'+tempCustID[inv]+'</td>'
					+'	<td class="selectAMT_'+tempInvNumber[inv]+'">'+currencyFormat(tempInvBal[inv])+'</td>'
					+'	<td>'+tempPONumber[inv]+'</td>'
					+'	<td>'+dateFormat(tempInvDate[inv])+'</td>'
					+'  <td style="text-align:right;"><span class="plus'+inv+' glyphicon glyphicon-plus-sign"></span></td>'
					+'</tr>';
					
					if($.inArray(tempInvNumber[inv], tempPayINV) !== -1){
					
						INV_Result_Rows += '<tr class="inv-row inv-row-'+inv+'">'
						+'	<td></td>'
						+'	<td></td>'
						+'	<td colspan="5">'
						+'		<div class="inv-line-'+inv+' collapse">'
						+'		<table class="table-striped">'
						+'			<thead>'
						+'			<tr>'
						+'				<th>LINE</th>'
						+'				<th>PAYMENT AMOUNT</th>'
						+'				<th>REMAINING BALANCE</th>'
						//+'				<th>PAYMENT APPLIED BY</th>'
						+'				<th>PAYMENT DATE</th>'
						+'			</tr>'
						+'			</thead>'
						+'			<tbody>';
						
						IndexList = getIndexList(tempPayINV, tempInvNumber[inv]);
						
						for(i=0;i<IndexList.length;i++){
							
							//payAmount = '$' + parseFloat(tempPayAmount[IndexList[i]], 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
							//payBalance = '$' + parseFloat(tempPayBalance[IndexList[i]], 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
						
							INV_Result_Rows += '<tr>'
							+'				<td>00'+(i+1)+'</td>'
							+'				<td>'+currencyFormat(tempPayAmount[IndexList[i]])+'</td>'
							+'				<td>'+currencyFormat(tempPayBalance[IndexList[i]])+'</td>'
							//+'				<td>'+tempPayApplied[IndexList[i]]+'</td>'
							+'				<td>'+dateFormat(tempPayDate[IndexList[i]])+'</td>'
							+'			</tr>';
						
						}
						
						IndexList = [];
						
						INV_Result_Rows += '</tbody>'
						+'		</table>'
						+'		</div>'
						+'	</td>'
						+'</tr>';
					}
				}
			}
		
			$('<tbody>'+INV_Result_Rows+"</tbody>").insertAfter($('table thead#INV_Results_Header'));	
			Paginated = true;
        }
    });
}

function updateStatus(Row) {
	$('.inv-row-'+Row).on('shown.bs.collapse', function() {$(".plus"+Row).addClass('glyphicon-minus-sign').removeClass('glyphicon-plus-sign');});
	$('.inv-row-'+Row).on('hidden.bs.collapse', function() {$(".plus"+Row).addClass('glyphicon-plus-sign').removeClass('glyphicon-minus-sign');});
}

function pauseStatus(Row) {
	$('.INV_Result_Rows').on('click', function (e) { e.stopPropagation(); })
	$('#INV_Number_Anchor').val(Row);
	$('#Cust_Anchor_Form').submit();
}

function printInvoice() {
	var inv_param = getUrlParameter("inv");
	$('<iframe src="./?pg=Invoices&st=Print&inv='+inv_param+'" style="display:none;"></iframe>').appendTo('span.invoice_frame');
	$('#invoicePrint').modal("show");
}