//$( "#auto_complete_supplier" ).on( "autocompleteselect", function( event, ui ) {console.log('xxsssss');} );
//$( "#tags" ).on( "autocompleteselect", function( event, ui ) {console.log('xxsssss');} );

var supplierItem = '';
var supplierspinner = '';
function autocompletesupplier() {
	$( "#autocomplete_supplier" ).autocomplete({
		select: function( event, ui ) {
			supplierItem = JSON.parse('{"item":{"value":"' + ui.item.value + '","key":"' + ui.item.key + '","who":"' + ui.item.who + '"}}');
			//Start spinner				
			var target = document.getElementById('autocomplete_supplier_dropdown');
        	var opts = spinnerOptions();
        	supplierspinner = new Spinner(opts).spin(target);
			updateContactInfo($(this).parent().parent().parent());
		}
	});
	
	$("#autocomplete_supplier").autocomplete({
		//delay: 500,
		minLength: 1,
		source: function(request, response) {
			$.ajax({
				type: "POST",
				contentType: "application/json; charset=utf-8",
				url: "ajax/supplier_lookup.cfm",
				data: '{"lookup":"' + request.term + '"}',
				dataType: "json",
				success: function (data) {	
				console.log(data);				
					if (data !== null) {
						//response(data);							
						var array = $.map(data, function(m) {
								return {
									label: m.label,
									value: m.label,
									key: m.value,
									who: 'supplier_info'
								};
						});
						response(array);											
					}
				}
				/*,
				error: function(result) {
					console.log("Error");
				},
				focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
				},
				select: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					//window.open(ui.item.url);
				}
				*/
			});
		}
	});
}

var shipperItem = '';
var shipperspinner;
function autocompleteshipping() {
	$( "#autocomplete_shipping" ).autocomplete({
		select: function( event, ui ) {
			shipperItem = JSON.parse('{"item":{"value":"' + ui.item.value + '","key":"' + ui.item.key + '","who":"' + ui.item.who + '"}}');
			//Start spinner				
			var target = document.getElementById('autocomplete_shipping_dropdown');
        	var opts = spinnerOptions();
        	shipperspinner = new Spinner(opts).spin(target);
			updateContactInfo($(this).parent().parent().parent());
		}
	});
	
	$("#autocomplete_shipping").autocomplete({
		//delay: 500,
		minLength: 1,
		source: function(request, response) {
			$.ajax({
				type: "POST",
				contentType: "application/json; charset=utf-8",
				url: "ajax/shipping_lookup.cfm",
				data: '{"lookup":"' + request.term + '"}',
				dataType: "json",
				success: function (data) {					
					if (data !== null) {
						//response(data);							
						var array = $.map(data, function(m) {
								return {
									label: m.label,
									value: m.label,
									key: m.value,
									who: 'ship_to_info'
								};
						});
						response(array);											
					}
				}
			});
		}
	});
}