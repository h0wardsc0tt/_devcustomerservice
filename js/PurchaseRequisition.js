$(function() {
	
	// Toggle items per page
	$('#CSP_Per_Page').change(function(e) {
		//$('#CSP_Search_Page').val($('#Product_Search').val());
		$('#CSP_Current_Page').val(1);
        $('#Cust_Search_Form').submit();
    });
	
	// Set to Page 1 if new search
	$('#Cust_Search_Submit').click(function(e) {
        $('#CSP_Current_Page').val(1);
    });

});

function setCustNumber(cust){
	$('#PurchaseRequisition_Number_Anchor').val(cust);
	$('#PurchaseRequisition_Anchor_Form').submit();
}