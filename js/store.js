
$(function() {
	
	// Add/Remove Hover Class to Cart Count
	$('.product-shopping-cart').hover(function() {
			$('.product-cart-count-symbol').addClass('product-cart-count-symbol-hover');
	}, function() {
		$('.product-cart-count-symbol').removeClass('product-cart-count-symbol-hover');
    });
	
	// Hide Cart Count
	$('.product-cart-count-symbol').hide();
	
	// Get Cart Count
	$.ajax({ 
		async: false,
		type: 'POST', 
		url: './proxy.cfm?service=CartMgmt&method=GetCartCount&returnFormat=json', 
		cache: false, 
		dataType: 'json',
		success: function(CartCount) {
			if(CartCount > 0){
				$('.product-shopping-cart').addClass('product-cart-full');
				$('.product-cart-count-symbol').text(CartCount);
				$('.product-cart-count-symbol').show();
			} else {
				$('.product-cart-count-symbol').hide();
				$('.product-cart-count-symbol').text("0");
				$('.product-shopping-cart').removeClass('product-cart-full');	
			}
		}
	});
	
	// Toggle items per page
	$('#Product_Per_Page').change(function(e) {
		$('#Product_Search_Page').val($('#Product_Search').val());
		$('#Product_Current_Page').val(1);
        $('#Product_Page_Form').submit();
    });
	
	// Paginate to Next page
	$('#Product_Next_Page').click(function(e) {
        $('#Product_Current_Page').val(parseInt($('#Product_Current_Page').val())+1);
		$('#Product_Search_Page').val($('#Product_Search').val());
    });
	
	// Paginate to Previous page
	$('#Product_Prev_Page').click(function(e) {
        $('#Product_Current_Page').val(parseInt($('#Product_Current_Page').val())-1);
		$('#Product_Search_Page').val($('#Product_Search').val());
    });
	
	// Dynamically set modal width to image
	$('.product-detail-cont img').click(function(e) {
        $('#Product_Image_Modal .modal-dialog').css("width", $('#Product_Image_Modal .modal-dialog img').width());
    });
	
	// Remove Item from Cart
	$('.Product_Cart_Remove').click(function(e) {
        var Product_Cart_Item = $(this).attr('class').split(' ')[1].split('_')[3];
		var Cart_Count = $('.product-cart-count').text();
		
		$.ajax({ 
			async: false,
			type: 'POST', 
			url: './proxy.cfm?service=CartMgmt&method=RemoveCartItem&returnFormat=json', 
			data: {ItemUID: Product_Cart_Item},
			cache: false, 
			dataType: 'json',
			success: function(removeStatus) {
				
				$.ajax({ 
					async: false,
					type: 'POST', 
					url: './proxy.cfm?service=CartMgmt&method=GetCartCount&returnFormat=json', 
					cache: false, 
					dataType: 'json',
					success: function(CartCount) {
						Cart_Count = CartCount;
					}
				});
				
				if(Cart_Count > 0){
					$('#'+Product_Cart_Item).fadeOut();
					
					setTimeout(function(){
						$('#'+Product_Cart_Item).remove();
					}, 3000);
					
					// Set Cart Count
					$('.product-cart-count-symbol').text(Cart_Count);
				} else {
					$('.product-cart-count-symbol').hide();
					$('.product-cart-count-symbol').text("0");
					$('.product-shopping-cart').removeClass('product-cart-full');
					$('.product-cart-cont-show').hide();
					$('.product-cart-empty-hidden').fadeIn();	
				}
				
			}
		});
		
		$.ajax({ 
			async: false,
			type: 'POST', 
			url: './proxy.cfm?service=CartMgmt&method=GetCartSubtotal&returnFormat=json', 
			cache: false, 
			dataType: 'json',
			success: function(subtotal) {
				$('.product-cart-count').text(Cart_Count);
				$('.product-cart-subtotal strong').text(subtotal);
			}
		});
    });
	
	// Update Item in Cart
	$('.Product_Cart_Qty').change(function(e) {
        var Product_Qty_Item = $(this).attr('class').split(' ')[3].split('_')[3];
		var Product_Qty = $(this).val();
		
		$.ajax({ 
			async: false,
			type: 'POST', 
			url: './proxy.cfm?service=CartMgmt&method=UpdateCartItem&returnFormat=json', 
			data: {ItemUID: Product_Qty_Item, ItemQty: Product_Qty},
			cache: false, 
			dataType: 'json',
			success: function(updateStatus) {
				$('.Product_Cart_Price_'+Product_Qty_Item).text(updateStatus);
			}
		});
		
		$.ajax({ 
			async: false,
			type: 'POST', 
			url: './proxy.cfm?service=CartMgmt&method=GetCartSubtotal&returnFormat=json', 
			cache: false, 
			dataType: 'json',
			success: function(subtotal) {
				$('.product-cart-subtotal strong').text(subtotal);
			}
		});
		
    });
	
	// Clear Items from Cart
	$('.Product_Cart_Clear').click(function(e) {
		var Product_Items = [];
		
		$('.Product_Cart_Remove').each(function(cart_item) {
            var Product_Cart_Clear_Item = $(this).attr('class').split(' ')[1].split('_')[3];
			Product_Items[cart_item] = Product_Cart_Clear_Item;
        });
		
		$.ajax({ 
			async: false,
			type: 'POST', 
			url: './proxy.cfm?service=CartMgmt&method=RemoveCartItem&returnFormat=json', 
			data: {ItemUID: JSON.stringify(Product_Items)},
			cache: false, 
			dataType: 'json',
			success: function(removeStatus) {
				$('.product-cart-cont-show').hide();
				$('.product-cart-empty-hidden').fadeIn();
			}
		});
		
		// Set Cart Count
		$('.product-cart-count-symbol').hide();
		$('.product-cart-count-symbol').text("0");
		$('.product-shopping-cart').removeClass('product-cart-full');
	});
});

