Array.prototype.remove = function (id) {this.splice(this.indexOf(id) == -1 ? this.length : this.indexOf(id), 1); }
$(document).ready(function() {	
	$("[data-toggle=tooltip").tooltip();

	$('.searchClick').attr('onclick','updateContactInfo($(this).parent().parent().parent())')	
	$('.closeSearch').attr('onclick',"$(this).parent().parent().parent().prev().click();")
	$('.showDropdown').attr('onclick',"toggleDropDown($(this));")
		
	$('.modal-dialog').draggable({
		handle: ".modal-header"
	});
	
	$('.expandable-element').height(0);

	autocompletesupplier();
	autocompleteshipping();
	
	console.log(json);


});

function jsonEscape(str)  {
	console.log(str)
    return str.replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r").replace(/\t/g, "\\\\t");
}

$('.text-box-update').change(function(e){
	edit_element(e);
});

var line_items_filtered;
function edit_element(e){
	"use strict";
	var index, obj, line_item, item_id, obj_line_item, obj_item;
	$(e.currentTarget).css('border-color','');

	if($(e.currentTarget).hasClass('line-item')){			
		line_item = $(e.currentTarget).parent().parent('.items').prop('id');
		if($(e.currentTarget).hasClass('numeric')){
			if($(e.currentTarget).val() !== '' && !$.isNumeric($(e.currentTarget).val().replace(/[,]+/g, "").replace(/[$£€]+/g, ""))){
				$(e.currentTarget).css('border-color','#F40E12');
				$(e.currentTarget).focus();
				return false;
			}
		}			
		index = json.line_items.map(function (id) { return id['id']; }).indexOf(line_item);
		obj_line_item=json.line_items[index];
		item_id = $(e.currentTarget).attr('itemid');
		index = obj_line_item.line_item.map(function (id) { return id['id']; }).indexOf(item_id);
		obj_item = obj_line_item.line_item[index];
		obj_item.value = $(e.currentTarget).val();
		obj_item.status = 'modified';
		obj_line_item.status = 'modified';
		json.line_item_change = 'modified';
	}else
	{
		index = json.order.map(function (id) { return id['id']; }).indexOf(e.currentTarget.id);
		console.log(e.currentTarget.id);
		json.order[index].value = $(e.currentTarget).val();
		json.order[index].status = 'modified';
		json.order_change = 'modified';
	}
	
	if(json.key != ''){
		json.status = 'modified';
	}
	$('#save_purchase_req').prop('disabled', false);

	console.log(json);	
}

function toggleInput(obj){
	if (obj.attr('disabled')) 
		obj.removeAttr('disabled');
	else 
		obj.attr('disabled', 'disabled');
}

function toggleContentEditable(objToEdit, objSaveBtn){
	objToEdit.attr('contenteditable', true);
	objToEdit.attr('save', objToEdit.html());
	objToEdit.css('background-color','#ffffff');
	objSaveBtn.show();
}

function toggleContentEditableSave(objToSave, objSaveBtn){
	//console.log(objToSave.html()); 
	//console.log(objToSave.attr('id')); 
	
	objToSave.css('background-color','');
	objSaveBtn.hide();
}

function toggleContentEditableCancel(objToCancel, objSaveBtn){
	console.log(objToCancel.html()); 
	console.log(objToCancel.attr('id')); 
	
	objToCancel.html(objToCancel.attr('save'));
	objToCancel.css('background-color','');
	objSaveBtn.hide();
}

function newLineItem(){
	var next_item = $("#itemRows").children().length + 1;
	var clone = $(".itemRowClone").clone().removeClass('itemRowClone').prop('id','itemRow_' + next_item);
	$("#itemRows").append(clone);
	var date_Picker = clone.find(".datepickerClone");
	date_Picker.removeAttr('id');
	date_Picker.removeClass('hasDatepicker datepickerClone hasDatepicker datepicker')
		.addClass('datepicker').datepicker();
	$('.text-box-update').change(function(e){
		edit_element(e);
	});
	console.log(json);
	var new_link_item_class = JSON.parse(JSON.stringify(json_new_line_item));
	console.log(json.line_items);	
 	new_link_item_class.id = 'itemRow_' + next_item;
	console.log(json.line_items);	
	json.line_items.push(new_link_item_class);

	console.log(json);
}

function clean_json(data){
	json.status = '';
	json.line_item_change = '';
	json.order_change = '';
	$.each(json.order, function(index, prop) {
		prop.status = '';
	});	
	$.each(json.line_items, function(index, prop) {
		prop.status = '';
		$.each(prop.line_item, function(index, prop) {
			prop.status = '';
		});
	});
	$.each(data.lineitems, function(index, prop) {
			if(prop.type == 'insert'){
				index = json.line_items.map(function (id) { return id['id']; }).indexOf(prop.lineitemid);
				console.log(json.line_items[index]);
				if(json.line_items[index].key == '')
					json.line_items[index].key = prop.identity;
			}
	});	
	console.log(data);

}
var update_spinner;
function submit_pr(type){
	var purchase_order_filtered, line_items_filtered;
	if(type != 'delete' && json.line_item_change != 'modified' && json.order_change != 'modified')
		return;
/*		
	if(json.line_item_change == 'modified'){
		line_items_filtered = jsonsql.query("select * from json.line_items where (status == 'modified') desc",json);
		console.log(line_items_filtered);
	}
	
	if(json.order_change == 'modified'){
		purchase_order_filtered = jsonsql.query("select * from json.order where (status == 'modified') desc",json);
		console.log(purchase_order_filtered);
	}
*/
	console.log(json);
	switch(type){
		case 'delete':
			$('#update_Title').html('Deleting Purchase Requisition');
		break;
		case 'update':
		if(json.status == 'new')
			$('#update_Title').html('Adding Purchase Requisition');
		else
			$('#update_Title').html('Updating Purchase Requisition');
		break

	}
	$('#update_Purchase_Requisition_spinner').height(100);
	$('#update_Purchase_Requisition_messages').height(0);
	toggle_modal($('#update_Purchase_Requisition'));
	var target = document.getElementById('update_Purchase_Requisition_spinner');
	var opts = spinnerOptions();
	update_spinner  = new Spinner(opts).spin(target);
	$.ajax({
		type: "POST",
		contentType: "application/json; charset=utf-8",
		url: "ajax/purchase_req_update.cfm",
		data: '{"type":"' + type + '", "json":' + JSON.stringify(json) + '}',
		dataType: "json",
		success: function (data) {					
			if (data !== null) {
					console.log(data);
				//response = data;//jQuery.parseJSON(data);
				if(data.results == 'OK'){
					update_spinner.stop();

					switch(data.type){
						case 'delete':
							$('#update_Purchase_Requisition_spinner').html('Purchase Requisition Deleted.');
							new_pr();
							break;
						case 'update':
							$('#update_Purchase_Requisition_spinner').html('Purchase Requisition Updated.');
							clean_json(data);
							break
						case 'insert':
							$('#update_Purchase_Requisition_spinner').html('Purchase Requisition Added.');
							json.key = data.identity;
							clean_json(data);
							break;
					}
					$('#save_purchase_req').prop('disabled', true);
					setTimeout(function(){ $('#update_Purchase_Requisition').modal('hide'); }, 3000);
					console.log(json);					
				}else{
					update_spinner.stop();  
					$('#update_Purchase_Requisition_spinner').height(0);
					$('#update_Purchase_Requisition_message').html('An error has occured:<br>'+data.results); 
					$('#update_Purchase_Requisition_messages').height(100);
				}
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			update_spinner.stop();  
			$('#update_Purchase_Requisition_spinner').height(0);
			$('#update_Purchase_Requisition_message').html('An error has occured:<br>'+thrownError); 
			$('#update_Purchase_Requisition_messages').height(100);
               
			console.log(thrownError);
        }
	});
}

function print_pr(){
	window.print();
}

function new_pr(){
	/*
	json.key = '';
	json.status = 'new';
	json.order_change = '';
	json.line_item_change = '';
	$.each(json.order, function(index, prop) {
		prop.value = '';
		prop.status = '';
	});	
	*/														
	$('.line-item').val('');
	$('.pr-item').val('');
	$('.content-edit').val('');
	$('#itemRows').html('');
	newLineItem();
	json = JSON.parse(JSON.stringify(json_new_purchase_req));
	$('#save_purchase_req').prop('disabled', true);
	$('#delete_purchase_req').prop('disabled', true);
	
	console.log(json);
}

function toggle_modal(obj){
	obj.modal('toggle');
}

function deleteRowRequest(obj){
	objToDelete = obj;
	$('#delete_message').show();
	$('#deleteBtn').show();
	$('#delete_error').hide();
	$('#delete_error_messages').height(0);;
	$('#deleteBtn').attr('onclick','deleteRowFulfill();');
	$('#delete_title').html('Delete line item');
	$('#delete_message').html('Are you sure you want to delete this line item.');
	$('#deleteItemRow').modal('show');
}

var delete_spinner;
var objToDelete; 
function deleteRowFulfill(){
	var index, line_item_key;
	var index = json.line_items.map(function (id) { return id['id']; }).indexOf(objToDelete.prop('id'));
	if(json.line_items[index].key != ''){
		$('#delete_message').html('');
		var target = document.getElementById('delete_message');
		var opts = spinnerOptions();
		delete_spinner  = new Spinner(opts).spin(target);
		line_item_key = json.line_items[index].key;
		$.ajax({
			type: "POST",
			contentType: "application/json; charset=utf-8",
			url: "ajax/purchase_req_lineItem_delete.cfm",
			data: '{"id":"' + line_item_key + '"}',
			dataType: "json",
			success: function (data) {					
				if (data !== null) {
					console.log(data);
					if(data.results == 'OK'){
						delete_spinner.stop();
						$('#delete_message').html('');
						$('#delete_title').html('');
						$('#deleteItemRow').modal('hide');
						objToDelete.css('display','none');
						json.line_items.splice(index, 1);
						if(json.line_items.length == 0)
							newLineItem()	
						console.log(json);
					}else{
						delete_spinner.stop();
						$('#delete_error_message').html('An error has occured:<br>' + data.results);
						$('#deleteBtn').hide();
						$('#delete_message').hide();
						$('#delete_error').show();
						$('#delete_error_messages').height(100);
					}
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				delete_spinner.stop();
				$('#delete_error_message').html('An error has occured:<br>' + thrownError);
				$('#deleteBtn').hide();
				$('#delete_message').hide();
				$('#delete_error').show();
				$('#delete_error_messages').height(100);
			}
		});		
	}else
	{
		$('#delete_message').html('');
		$('#delete_title').html('');
		$('#deleteItemRow').modal('hide');
		objToDelete.css('display','none');
		json.line_items.splice(index, 1);
		if(json.line_items.length == 0)
			newLineItem()	
		console.log(json);
	}
}

var focusObj;
function toggleDropDown(obj){
	if(obj.next().height()==0){
		var left = (obj.parent().width()/2) - (obj.next().width()/2)
		obj.next().children().css('width', obj.parent().width() + 'px');
		obj.next().children().css('height', ($('#container_row1').prop('offsetHeight') * .98) + 'px');
		obj.next().height(obj.next().prop('scrollHeight'));
		focusObj = obj.next().children().children('input');
		obj.next().children().children('input').val('');
		obj.next().children().children('a:first').children('i:first').css('left',obj.next().children().children('input:first').width()-15);
		setTimeout(function(){ focusObj.focus(); }, 500);
	}else{
		if(obj.next().children().prop('id') === 'autocomplete_supplier_dropdown'){
			try {supplierspinner.stop();}catch(err){}
		}else{
			try {shipperspinner.stop();}catch(err){}
		}
		obj.next().height(0);
		obj.next().children().children('.error-message').height(0);
	}
}

function updateContactInfo(obj){
	switch(obj.attr('id')){
		case 'supplier_info':
			if(supplierItem === '')return;
			supplier_shippingInfo(supplierItem);
			supplierItem= '';
			break;
		case 'ship_to_info':
			if(shipperItem === '')return;
			supplier_shippingInfo(shipperItem);
			shipperItem = '';
			break;
		default:
			break;
	}
}

//window.addEventListener("hashchange", function(e) {})

$('.content-edit').keyup(function(e){
	var elem = ($(this).prop('id'));

	var supplier_edit_obj = $('#supplier_edit');
	var shipper_edit_obj = $('#shipping_edit');
});

$(function(){
    $( ".datepicker" ).datepicker();
 });
 
 $(function(){
	$('.searchIcon').css('top',24);
 });

$('.lookup-keypress').keypress(function(e){
	"use strict";
    if(e.which === 13) {
        $(this).next().click();
    }
});

$('#purchase_req_search').keypress(function(e){
	"use strict";
    if(e.which === 13) {
        search_for_purchase_req();
    }
});

function search_for_purchase_req(){
	var clone, date_Picker;
	console.log($("input[name='searchBy']:checked").val());
	$('.text-box-update').val('');
	$('#itemRows').html('');
	$.ajax({
		type: "POST",
		contentType: "application/json; charset=utf-8",
		url: "ajax/purchase_req_lookup.cfm",
		data: '{"lookup":"' + 1 + '"}',
		dataType: "json",
		success: function (data) {					
			if (data !== null) {
				var pur_req = JSON.parse(data);
				console.log(pur_req.order);							
				$.each(pur_req.order, function(index, prop) {
					if(prop.id == 'supplier_edit' || prop.id == 'shipping_edit'){ 
						$('#' + prop.id).val(prop.value);
						console.log($('#' + prop.id).val()); 
					}else
						$('#' + prop.id).val(prop.value);
					console.log(prop.id + '   value=' + prop.value); 
				});															
				console.log(pur_req.line_items);							
				$.each(pur_req.line_items, function(index, prop) {
					console.log(prop.id); 
					clone = $(".itemRowClone").clone().removeClass('itemRowClone').prop('id','itemRow_' + (index + 1));
					$("#itemRows").append(clone);
					date_Picker = clone.find(".datepickerClone");
					date_Picker.removeAttr('id');
					date_Picker.removeClass('hasDatepicker datepickerClone hasDatepicker datepicker')
						.addClass('datepicker').datepicker();
						$('.text-box-update').change(function(e){
						edit_element(e);
					});
					$.each(prop.line_item, function(index, prop) {
						//clone.find('#[id*=shot_type_id_]').attr('id', 'shot_type_id_' +  numOfShots).attr('name','shot_type_id_' +  numOfShots).val(0); 
						clone.find("[itemid='"+prop.id+"']").val(prop.value);           
						//clone[prop.id] = prop.value; 
						console.log(prop.id + '   value=' + prop.value); 
					});															
				});
				$('#save_purchase_req').prop('disabled', true);
				$('#delete_purchase_req').prop('disabled', false);
				toggle_modal($('#searchModal'))															
			}
		}
	});
}
$(document).bind('keydown', function(event){
	//console.log(event.which+'     '+event.altKey)
	"use strict";
	if(event.altKey){
		switch(event.which){
			case 49:
				$('#supplier_info').children('a').click();
				break;
			case 50:
				$('#ship_to_info').children('a').click();
				break;
			default:
				break;
		}
	}
});

$(':input').keyup(function (e) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if ((regex.test(str)) || (e.which == 8) ||  (e.which == 13 && (this.id == 'supplier_edit' || this.id == 'shipping_edit' || this.id == 'comments'))) {
		$('#save_purchase_req').prop('disabled', false);
    }
});
/*
event.shiftKey
event.ctrlKey
event.altKey
event.metaKey
*/
