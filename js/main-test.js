$(function(){

	$("#Cust_Search_Form").submit(function(e){
        e.preventDefault();
		Current_Page = 1;
		$("ul.pagination li").remove(); 
		getCustomers();
    });
	
	$("#SO_Search_Form").submit(function(e){
        e.preventDefault();
		Current_Page = 1;
		$("ul.pagination li").remove(); 
		getSalesOrders();
    });

});

/*  Pagination BEGIN */
var Current_Page = 1;
var Pages = 0;
var Records_Per_Page = 5;
var Page_Buttons = "";
var Page_Start = Current_Page*Records_Per_Page - Records_Per_Page;
var	Page_End = Current_Page*Records_Per_Page;

function setPaginate(records){
	
	if($('ul.pagination li.active').text().length){
		Current_Page = $('ul.pagination li.active').text();
	}

	Page_Start = Current_Page*Records_Per_Page - Records_Per_Page;
	Page_End = Current_Page*Records_Per_Page;
	Page_Buttons = "";
	Pages = Math.ceil(records/Records_Per_Page);

	if(Pages > 1){
		for(page=0;page<Pages;page++){
			if(page == Current_Page-1){
				Page_Buttons += '<li class="active page'+(page+1)+'" onclick="getPage('+(page+1)+');"><a href="javascript:void(0);" >'+(page+1)+'</a></li>';	
			} else {
				Page_Buttons += '<li class="page'+(page+1)+'" onclick="getPage('+(page+1)+');"><a href="javascript:void(0);" >'+(page+1)+'</a></li>';		
			}
		}
	}
	
	$("ul.pagination li").remove(); 
	$(Page_Buttons).appendTo($('ul.pagination'));
}

function getPage(page){
	$('ul.pagination li.active').removeClass("active");
	$('ul.pagination li.page'+page).addClass("active");	
	
	if($("#SO_Search_Form").length){
		getSalesOrders();
	}
	
	if($("#Cust_Search_Form").length){
		getCustomers();
	}
}
/*  Pagination END */

function getCustomers(CustId){
	if (CustId != undefined) {
		$('#Cust_Number').val(CustId);
	}
	
	var Cust_Result_Rows = "";
	var Cust_Number = $('#Cust_Number').val();
	var Cust_Name = $('#Cust_Name').val();
	/*var Cust_SO = $('#Cust_SO').val();
	var Cust_INV = $('#Cust_INV').val();
	var Cust_SN = $('#Cust_SN').val();*/
	var Cust_City = $('#Cust_City').val();
	var Cust_State = $('#Cust_State').val();
	var Cust_PostCode = $('#Cust_PostCode').val();
	
	//data: {Cust_Number: Cust_Number, Cust_Name: Cust_Name, Cust_SO: Cust_SO, Cust_INV: Cust_INV, Cust_SN: Cust_SN, Cust_City: Cust_City, Cust_State: Cust_State, Cust_PostCode: Cust_PostCode},
	
	$.ajax({ 
		async: false,
		type: 'POST', 
		url: './proxy.cfm?service=CustInfo&method=getCustomers&returnFormat=json', 
		data: {Cust_Number: Cust_Number, Cust_Name: Cust_Name, Cust_City: Cust_City, Cust_State: Cust_State, Cust_PostCode: Cust_PostCode},
		cache: false, 
		dataType: 'json',
		success: function(custInfo) {
			var cust_temp = JSON.parse(custInfo);
			tempId = cust_temp.DATA.CUSTOMERID;
			tempName = cust_temp.DATA.CUSTOMERNAME;
			tempAddress = cust_temp.DATA.ADDRESS;
			tempCity = cust_temp.DATA.ADDRESSCITY;
			tempState = cust_temp.DATA.ADDRESSSTATE;
			tempPostCode = cust_temp.DATA.ADDRESSZIPCODE;
		}
	});
	
	
	setPaginate(tempId.length);
	
	$('#record-count').text("("+tempId.length+")");
	
	$("table#Cust_Results tr.Cust_Results_Row").remove(); 
	
	for(cust=Page_Start;cust<Page_End;cust++){
		//Cust_Result_Rows += '<tr class="Cust_Results_Row" onclick="getSalesOrders(\''+tempId[cust]+'\');">'
		//onclick="window.location=\'./?pg=SalesOrders&cust='+tempId[cust]+'\';"
		if (tempId[cust] != undefined) {
			Cust_Result_Rows += '<tr class="Cust_Results_Row">'
			+'	<td><a href="./?pg=SalesOrders&cust='+tempId[cust]+'">'+tempId[cust]+'</a></td>'
			+'	<td>'+tempName[cust]+'</td>'
			+'	<td>'+tempAddress[cust]+'</td>'
			+'	<td>'+tempCity[cust]+'</td>'
			+'	<td>'+tempState[cust]+'</td>'
			+'	<td>'+tempPostCode[cust]+'</td>'
			+'</tr>';
		}
	}

	$(Cust_Result_Rows).insertAfter($('table tr#Cust_Results_Header'));
}

function displayCustomers(){
	
}

function getSalesOrders(CustId){
	var Cust_Number = "";
	
	if (CustId != undefined) {
		Cust_Number = CustId;
	}
	
	var SO_Result_Rows = "";
	var SO_Number = $('#SO_Number').val();
	var PO_Number = $('#PO_Number').val();
	var INV_Number = $('#INV_Number').val();
	var Part_Number = $('#Part_Number').val();
	
	$.ajax({ 
		async: false,
		type: 'POST', 
		url: './proxy.cfm?service=OrderInfo&method=getOrders&returnFormat=json', 
		data: {Cust_Number: Cust_Number, SO_Number: SO_Number, PO_Number: PO_Number, INV_Number: INV_Number, Part_Number: Part_Number},
		cache: false, 
		dataType: 'json',
		success: function(soInfo) {
			var so_temp = JSON.parse(soInfo);
			tempSONumber = so_temp.DATA.CUSTOMERID;
			tempPONumber = so_temp.DATA.CUSTOMERNAME;
			tempDate = so_temp.DATA.ADDRESS;
		}
	});
	
	$("table#SO_Results tr.SO_Results_Row").remove(); 
	
	for(order=0;order<tempId.length;order++){
		SO_Result_Rows += '<tr class="Cust_Results_Row" onclick="test(\''+tempSONumber[order]+'\');">'
		+'	<td>'+tempSONumber[order]+'</td>'
		+'	<td>'+tempPONumber[order]+'</td>'
		+'	<td>'+tempDate[order]+'</td>'
		+'</tr>';
	}
	
	$(SO_Result_Rows).insertAfter($('table tr#SO_Results_Header'));
}
