var checkList = [];
var checkValue = '';
var checkAmounts = [];
var checkAmount = '';
var checkCusts = [];
var checkCust = '';

$(function() {
	var company_box = [];
	var apps_box = [];
	
	$('.company_checkbox').change(function(e) {
		 if(this.checked){
			company_box.push(" " + $(this).attr('id')); 
		 } else {
			company_box.splice( $.inArray($(this).attr('id'), company_box), 1);
		 }
		 
		 $('.selected_company').text(company_box);
    });
	
	$('.apps_checkbox').change(function(e) {
		 if(this.checked){
			apps_box.push(" " + $(this).attr('id')); 
		 } else {
			apps_box.splice( $.inArray($(this).attr('id'), apps_box), 1);
		 }
		 
		 $('.selected_apps').text(apps_box);
    });
	
	/* Pagination */
	
	// Paginate to Next page
	$('#CSP_Next_Page').click(function(e) {
        $('#CSP_Current_Page').val(parseInt($('#CSP_Current_Page').val())+1);
    });
	
	// Paginate to Previous page
	$('#CSP_Prev_Page').click(function(e) {
        $('#CSP_Current_Page').val(parseInt($('#CSP_Current_Page').val())-1);
    });
	
	/* Email Receipt Recipients Form */
	$('#payRecipients').click(function() {
		$('.send-receipt-msg').hide();
		$('.send-receipt-err').hide();
		$('.send-receipt-cont').show();
		$('#sendReceipt').show();
		$('#Recipient_List').val("");
		$('#payReciept').modal("show");
	});
	
	/* Email Receipt Status Message */
	$('#sendReceipt').click(function() {
		$('.send-receipt-cont').hide();
		$('#sendReceipt').hide();
		
		if($('#Recipient_List').val()){
			
			var Recipient_List = $('#Recipient_List').val();
			var Payment_Numbers = checkList;
			var Payment_Amounts = checkAmounts;
			var Payment_Type = $('#payModal .modal-title span').text();
			var Payment_Company = $('#INV_Company').val();
			var Payment_Confirmation = $('.pay-confirm-number').text();
			var Card_Number = $('#Card_Number').val();
			var Card_Type = $('#Card_Type').val();
			
			$.ajax({ 
				async: false,
				type: 'POST', 
				url: './proxy.cfm?service=OrderInfo&method=sendEmailReceipt&returnFormat=json', 
				data: {Email_To: Recipient_List, Payment_Numbers: JSON.stringify(Payment_Numbers), Payment_Amounts: JSON.stringify(Payment_Amounts), Payment_Type: Payment_Type, Payment_Company: Payment_Company, Payment_Confirmation: Payment_Confirmation, Card_Number: Card_Number, Card_Type: Card_Type},
				cache: false, 
				dataType: 'json',
				success: function(payInfo) {
					//var pay_temp = JSON.parse(payInfo);
					//Payment_Message = pay_temp.PAYMENT_MESSAGE;
					//Payment_Confirmation = pay_temp.PAYMENT_CONFIRMATION;
				}
			});
			
			$('.send-receipt-msg').fadeIn();
		} else {
			$('.send-receipt-err').fadeIn();	
		}
	});
	
	/* On payment window closes */
	$('#payMessage').on('hidden.bs.modal', function () {
		$('#Card_Number').val("");
		$('#Card_Zip').val("");
		checkList = [];
		checkAmounts = [];
	})

	/* Pay Invoices */
	$('#pay-invoice').click(function() {
		$('.paySpinner').hide();
		$('.payMessage').show();
		$('.paySpinnerBtns').show();
		
		if(getUrlParameter("st") == "View"){
			checkCusts = [];
			checkList = [];
			checkAmounts = [];
			
			checkCusts.push($('span.cust-id-view').text());
			checkList.push(getUrlParameter("inv"));
			checkAmounts.push($('.total-amt').text().replace("$",""));
		} else {
        	getCheckedBoxes();
		}
		
		setCheckedRows();
		
		if(checkList.length){
			$('.no-payments-cont').hide();
			$('.payments-cont').show();
			$('#payConfirm_submit').show();
		} else {
			$('.payments-cont').hide();
			$('#payConfirm_submit').hide();
			$('.no-payments-cont').show();
		}
    });

	$("#Card_Confirm").submit(function(e){
        e.preventDefault();
		
		$('.paySpinner').show();
		$('.payMessage').hide();
		$('.paySpinnerBtns').hide();
		
		var Cust_Number = "";
		
		if($('#Cust_Number_Anchor').val().length){
			Cust_Number = $('#Cust_Number_Anchor').val();	
		}
		
		var Payment_Message = "";
		var Payment_Type = $('.modal-title span').text();
		var Payment_Cust_Numbers = checkCusts;
		var Payment_Numbers = checkList;
		var Payment_Amounts = checkAmounts;
		var Payment_Company = $('#INV_Company').val();
		var Payment_Total = $('.card-amount-confirm').text();
		var Card_Holder = $('#Card_Holder').val();
		var Card_Number = $('#Card_Number').val();
		var Card_Zip = $('#Card_Zip').val();
		var Card_Exp = $('#Card_Exp').val();
		var Card_CVC = $('#Card_CVC').val();
		var Card_Type = $('#Card_Type').val();
		
		$.ajax({ 
			async: false,
			type: 'POST', 
			url: './proxy.cfm?service=OrderInfo&method=setPayments&returnFormat=json', 
			data: {Payment_Cust_Numbers: JSON.stringify(Payment_Cust_Numbers), Payment_Numbers: JSON.stringify(Payment_Numbers), Payment_Amounts: JSON.stringify(Payment_Amounts), Payment_Total: Payment_Total, Payment_Type: Payment_Type, Payment_Company: Payment_Company, Card_Holder: Card_Holder, Card_Number: Card_Number, Card_Zip: Card_Zip, Card_Exp: Card_Exp, Card_CVC: Card_CVC, Card_Type: Card_Type},
			cache: false, 
			dataType: 'json',
			success: function(payInfo) {
				var pay_temp = JSON.parse(payInfo);
				Payment_Message = pay_temp.PAYMENT_MESSAGE;
				Payment_Confirmation = pay_temp.PAYMENT_CONFIRMATION;
			}
		});

		setTimeout(
		function() {
			$("#payModal").modal("hide");
			$("#payConfirm").modal("hide");
			$("#payMessage").modal("show");
		
			/* If successful */
			if(Payment_Confirmation.length && Payment_Message == "Approved"){
				
				$('.card-img img').hide();
			
				$('#Card_Holder').val("");
				//$('#Card_Number').val("");
				$('#Card_Zip').val("");
				$('#Card_CVC').val("");
				
				checkCusts = [];
				//checkList = [];
				//checkAmounts = [];
				checkValue = '';
				
				$('.selectCHK').each(function(index, element) {
					$(this).removeAttr('checked');
				});
				
				$('#selectINV_All').removeAttr('checked');
				
				$('.pay-error').hide();
				$('.pay-success').show();
				
				$('.pay-confirm-number').text(Payment_Confirmation);
			
			} else {
				$('.pay-success').hide();
				$('.pay-error').show();
				$('.pay-error-list ul li').text(Payment_Message);
			}
			
		}, 5000); 

    });
	
	$("#Card_Payment").submit(function(e){
        e.preventDefault();
		$("#payConfirm").modal("show");
	});
	/* Pay Invoices */
	
	$('.card-img img').hide();
});

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function currencyFormat(amount) {
	var dollarAmount;
	
	dollarAmount = '$' + parseFloat(amount, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
	
	return dollarAmount;
}

function dateFormat(date) {
	var dateFormat;
	
	 dateFormat = $.datepicker.formatDate('mm/dd/yy', new Date(date));
	
	return dateFormat;
}

function getIndexList(payment_array, payment_value){
    var indexMatches = [];
	
	for(index=0;index<payment_array.length;index++){
		if(payment_value == payment_array[index]){
			indexMatches.push(index);
		}
	}
	
    return indexMatches;
}

function checkAllBoxes(){
	if($('#selectINV_All').is(":checked")){
		$('.selectCHK').each(function(index, element) {
			if(!$(this).is(":disabled")){
				$(this).prop("checked", true);
			}
		});
	} else {
		$('.selectCHK').each(function(index, element) {
			$(this).prop("checked", false);	
		});
	}
	
	getCheckedBoxes();
}

function getCheckedBoxes(){
	
	$('.selectCHK').each(function(index, element) {
		checkValue = $(this).attr('id').split("_");
		checkAmount = Number($('.selectAMT_'+checkValue[1]).text().replace(/[^0-9\.\-]+/g,""));
		
		checkCust = $(this).attr('class').split(" ");
		checkCust = checkCust[1].split("_");
		
        if(this.checked){
			if($.inArray(checkValue[1], checkList) == -1){
				checkList.push(checkValue[1]);
				checkAmounts.push(checkAmount);
				checkCusts.push(checkCust[1]);
			}
		} else {
			if($.inArray(checkValue[1], checkList) !== -1){
				checkList.splice($.inArray(checkValue[1], checkList), 1);
				checkAmounts.splice($.inArray(checkAmount, checkAmounts), 1);
				checkCusts.splice($.inArray(checkCust[1], checkCusts), 1);
			}
		}
    });
	
	return checkList;
}

function setCheckedRows(){
	var Cart_Payment_Rows = "";
	var Cart_Total = 0;
	
	$("tr.cart_row").remove(); 
	
	if(checkList.length){
		for(cart_val=0;cart_val<checkList.length;cart_val++){
			
			Cart_Payment_Rows += '<tr class="cart_row">'
			+'	<td class="text-center"></td>'
			+'	<td><span class="glyphicon glyphicon-check"></span> '+checkList[cart_val]+'</td>'
			+'	<td><input type="text" value="'+currencyFormat(checkAmounts[cart_val])+'" size="11" onBlur="setPaymentDetails();"></td>'
			+'</tr>'
				
		}
	}
	
	if(checkAmounts.length){
		for(cart_amt=0;cart_amt<checkAmounts.length;cart_amt++){	
			Cart_Total += checkAmounts[cart_amt];
		}
	}
	
	$(Cart_Payment_Rows).insertBefore($('tr.total-row'));
	$('span.card-count').text(checkList.length);
	$('span.card-amount').text(currencyFormat(Cart_Total));
	$('.card-amount-confirm').text(currencyFormat(Cart_Total));
}

function setPaymentDetails(){
	var Cart_Total = 0;
	var temp_amount = 0;
	
	checkAmounts = [];
	
	$('.cart_row input').each(function(index, element) {
		temp_amount = parseFloat($(this).val().replace("$","").replace(",",""));
        Cart_Total += temp_amount;
		checkAmounts.push(temp_amount);
		$(this).val(currencyFormat(temp_amount));
    });
	
	$('span.card-amount').text(currencyFormat(Cart_Total));
	$('.card-amount-confirm').text(currencyFormat(Cart_Total));
}

function isNumberKey(evt)
{
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	return false;
	
	return true;
}

function numbersOnly(){
	var card_num = $('#Card_Number').val();
	card_num = card_num.replace(/[^0-9]/g, '');
	$('#Card_Number').val(card_num);
}

function triggerAnimation(element, value) {
	$(element).removeClass().addClass(value + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
	  $(this).removeClass();
	});
}

function getCardType() {
	var Card_ID = $('#Card_Number').val();
	var Visa = new RegExp('^4[0-9]{6,}$');
	var MasterCard = new RegExp('^5[1-5][0-9]{5,}$');
	var AmericanExpress = new RegExp('^3[47][0-9]{5,}$');
	var Discover = new RegExp('^6(?:011|5[0-9]{2})[0-9]{3,}$');
	
	if(Card_ID.length >=6) {
		$('.card-img img').show();
		
		if(Visa.test(Card_ID)) {
			triggerAnimation(".card-img img", "flipInX");
			$('.card-img img').attr("src","./images/Visa.png");
			$('#Card_Type').val("visa");
		} else if(MasterCard.test(Card_ID)) {
			triggerAnimation(".card-img img", "flipInX");
			$('.card-img img').attr("src","./images/Mastercard.png");	
			$('#Card_Type').val("mastercard");
		} else if(AmericanExpress.test(Card_ID)) {
			triggerAnimation(".card-img img", "flipInX");
			$('.card-img img').attr("src","./images/AmericanExpress.png");	
			$('#Card_Type').val("amex");
		} else if(Discover.test(Card_ID)) {
			triggerAnimation(".card-img img", "flipInX");
			$('.card-img img').attr("src","./images/Discover.png");
			$('#Card_Type').val("discover");
		} else {
			$('.card-img img').hide();
		}
	} else {
		$('.card-img img').hide();	
	}
}

function getUserTypeRoleList() {
	var UserType = $('#User_Type').val(); 
	var UserRoles = [];
	var UserApps = [];
	
	if(UserType === "Customer"){
		$('.admin-perm-cont').hide(); 
		$('#Apply_Credit').prop("checked", false);
		$('#Apply_Deposit').prop("checked", false);
	} else {
		if(UserType === "Full Admin"){
			$('#Apply_Credit').prop("checked", true);
			$('#Apply_Deposit').prop("checked", true);
		}
		$('.admin-perm-cont').show(); 	
	}
	
	$.ajax({ 
		async: false,
		type: 'POST', 
		url: './proxy.cfm?service=UserPermission&method=getUserRoleByType&returnFormat=json', 
		data: {UserType: UserType},
		cache: false, 
		dataType: 'json',
		success: function(roleInfo) {
			$('#User_Role option').remove();
			
			for(i=0;i<roleInfo.DATA.length;i++){
				UserRoles.push(roleInfo.DATA[i][1]);
				
				$('<option value="'+roleInfo.DATA[i][0]+'">'+roleInfo.DATA[i][1]+'</option>').appendTo('#User_Role');
			}
		}
	});

	$.ajax({ 
		async: false,
		type: 'POST', 
		url: './proxy.cfm?service=UserPermission&method=getApplicationByType&returnFormat=json', 
		data: {UserType: UserType},
		cache: false, 
		dataType: 'json',
		success: function(appInfo) {
			
			$('#User_Select_Apps option').remove();
			$('.apps ul li').remove();
			
			$('<li class="disabled"><span><input type="checkbox" disabled=""><label></label>Choose your option</span></li>').appendTo('.apps ul');
			$('<option value="" disabled="" selected="">Choose your option</option>').appendTo('#User_Select_Apps');
			
			for(i=0;i<appInfo.DATA.length;i++){
				UserApps.push(appInfo.DATA[i][1]);
				
				$('<li class=""><span><input type="checkbox"><label></label> '+appInfo.DATA[i][1]+'</span></li>').appendTo('.apps ul');
				$('<option value="'+appInfo.DATA[i][1]+'">'+appInfo.DATA[i][1]+'</option>').appendTo('#User_Select_Apps');
			}
			
			$('.companies.apps').material_select('destroy');
			$('.companies.apps').material_select();
		}
	});
}

