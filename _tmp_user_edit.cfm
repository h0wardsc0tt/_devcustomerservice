<div class="col-lg-12">
    <h2>User Information</h2>
</div>

<cfoutput>
	<!--- Error Message --->
    <cfif ERR.ErrorFound>
        <div class="col-lg-12">
            <div class="form-message form-error">
                <ul>
                    <cfloop list="#ERR.ErrorMessage#" index="thisError"><li>#thisError#</li></cfloop>
                </ul>
            </div>
        </div>
    <cfelse>
        <cfif Len(SuccessMsg) GT 0>
            <div class="col-lg-12">
                <div class="form-message form-success animated fadeIn">
                    <ul>
                        <li>#SuccessMsg#</li>
                    </ul>
                </div>
            </div>
        </cfif>
    </cfif>
</cfoutput>

<!--- User Information Form --->
<form action="./?pg=Users&st=Confirm" method="POST" id="User_Info_Form" class="form-horizontal col-lg-11 form-marg" role="form">
    <cfoutput>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="User_Company_Main" class="control-label col-lg-6">Company:<span class="red-text">*</span></label>
                <div class="col-lg-6">
                    <select name="User_Company_Main" id="User_Company_Main" class="companies" multiple>
                      <option value="" disabled selected>Choose your option</option>
                      <cfloop from="1" to="#getUserCompanies.RecordCount#" index="Company">
                          <option value="#getUserCompanies.Company_Code[Company]#" <cfif ListFind(qry_getSingleUserInfo.User_Company, getUserCompanies.Company_Code[Company]) NEQ 0>selected="selected"</cfif>> #getUserCompanies.Company_Name[Company]#</option>
                      </cfloop>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="User_Cust_ID" class="control-label col-lg-6">Customer ID:<span class="red-text">*</span></label>
                <div class="col-lg-6">
                    <input type="text" maxlength="100" name="User_Cust_ID" class="form-control" value="#FORM.User_Cust_ID#" disabled="disabled"/>
                </div>
            </div>
            <div class="form-group">
                <label for="User_CompanyName" class="control-label col-lg-6">Company Name:<span class="red-text">*</span></label>
                <div class="col-lg-6">
                    <input type="text" maxlength="100" name="User_CompanyName" class="form-control" value="#FORM.User_CompanyName#" />
                </div>
            </div>
            <div class="form-group">
                <label for="User_Brand" class="control-label col-lg-6">Company Brand:</label>
                <div class="col-lg-6">
                    <input type="text" maxlength="100" name="User_Brand" class="form-control" value="#FORM.User_Brand#" />
                </div>
            </div>
            <div class="form-group">
                <label for="User_Address_Line1" class="control-label col-lg-6">Company Address 1:<span class="red-text">*</span></label>
                <div class="col-lg-6">
                    <input type="text" maxlength="100" name="User_Address_Line1" class="form-control" value="#FORM.User_Address_Line1#" />
                </div>
            </div>
            <div class="form-group">
                <label for="User_Address_Line2" class="control-label col-lg-6">Company Address 2:</label>
                <div class="col-lg-6">
                    <input type="text" maxlength="100" name="User_Address_Line2" class="form-control" value="#FORM.User_Address_Line2#" />
                </div>
            </div>
            <div class="form-group">
                <label for="User_Locality" class="control-label col-lg-6">Company City/Locality:<span class="red-text">*</span></label>
                <div class="col-lg-6">
                    <input type="text" maxlength="100" name="User_Locality" class="form-control" value="#FORM.User_Locality#" />
                </div>
            </div>
            <div class="form-group">
                <label for="User_Region" class="control-label col-lg-6">Company State/Region:<span class="red-text">*</span></label>
                <div class="col-lg-6">
                    <input type="text" maxlength="100" name="User_Region" class="form-control" value="#FORM.User_Region#" />
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="User_Country" class="control-label col-lg-6">Company Country:<span class="red-text">*</span></label>
                <div class="col-lg-6">
                    <input type="text" maxlength="100" name="User_Country" class="form-control" value="#FORM.User_Country#" />
                </div>
            </div>
            <div class="form-group">
                <label for="User_PostCode" class="control-label col-lg-6">Company Postal Code:<span class="red-text">*</span></label>
                <div class="col-lg-6">
                    <input type="text" maxlength="100" name="User_PostCode" class="form-control" value="#FORM.User_PostCode#" />
                </div>
            </div>
            <div class="form-group">
                <label for="User_EmailAddress" class="control-label col-lg-6">Email Address:<span class="red-text">*</span></label>
                <div class="col-lg-6">
                    <input type="text" maxlength="100" name="User_EmailAddress" class="form-control" value="#FORM.User_EmailAddress#"/>
                </div>
            </div>
            <div class="form-group">
                <label for="User_FirstName" class="control-label col-lg-6">First Name:<span class="red-text">*</span></label>
                <div class="col-lg-6">
                    <input type="text" maxlength="100" name="User_FirstName" class="form-control" value="#FORM.User_FirstName#" />
                </div>
            </div>
            <div class="form-group">
                <label for="User_LastName" class="control-label col-lg-6">Last Name:<span class="red-text">*</span></label>
                <div class="col-lg-6">
                    <input type="text" maxlength="100" name="User_LastName" class="form-control" value="#FORM.User_LastName#" />
                </div>
            </div>
            <div class="form-group">
                <label for="User_Phone_Number" class="control-label col-lg-6">Phone Number:<span class="red-text">*</span></label>
                <div class="col-lg-6">
                    <input type="text" maxlength="100" name="User_Phone_Number" class="form-control" value="#FORM.User_Phone_Number#" />
                </div>
            </div>
            <div class="form-group">
                <label for="User_IsActive" class="control-label col-lg-6">Status:<span class="red-text">*</span></label>
                <div class="col-lg-6">
                    <input type="button" maxlength="100" name="User_IsActive_Label" id="User_IsActive_Label" class="form-control btn btn-success animated" value="<cfif FORM.User_IsActive EQ 1>Active<cfelse>Inactive</cfif>" />
                    <input type="hidden" name="User_IsActive" id="User_IsActive" value="#FORM.User_IsActive#" />
                </div>
            </div>
        </div>
    </cfoutput>
    
    <div class="col-lg-12 line-break"></div>
    
    <div class="col-lg-12 no-pad">
        <h2>User Permissions</h2>
    </div>

    <div class="col-lg-6">
        <div class="form-group">
            <label for="User_Type" class="control-label col-lg-5">User Type:<span class="red-text">*</span></label>
            <div class="col-lg-7">
                <select name="User_Type" id="User_Type" class="form-control" onchange="getUserTypeRoleList();">
                    <cfoutput query="getUserTypes">
                        <cfif User_IsFullAdmin>
                        	<option value="#UserType_Name#" <cfif getUserTypeRole.UserType_Name EQ UserType_Name>selected="selected"</cfif>>#UserType_Name#</option>
                        <cfelseif User_IsAdmin>
                        	<cfif UserType_Name NEQ "Full Admin">
                            	<option value="#UserType_Name#" <cfif getUserTypeRole.UserType_Name EQ UserType_Name>selected="selected"</cfif>>#UserType_Name#</option>
                            </cfif>
                        <cfelse>
                            <cfif UserType_Name DOES NOT CONTAIN "Admin">
                                <option value="#UserType_Name#" <cfif getUserTypeRole.UserType_Name EQ UserType_Name>selected="selected"</cfif>>#UserType_Name#</option>
                            </cfif>
                        </cfif>
                    </cfoutput>
                </select>
            </div>
        </div>
        <div class="form-group" style="display:none;">
            <label for="User_Role" class="control-label col-lg-5">User Role:<span class="red-text">*</span></label>
            <div class="col-lg-7">
                <select name="User_Role" id="User_Role" class="form-control">
                    <cfoutput query="getUserTypeRoles">
                        <option value="#TypeRole_ID#" <cfif getUserTypeRole.TypeRole_Name EQ TypeRole_Name>selected="selected"</cfif>>#TypeRole_Name#</option>
                    </cfoutput>
                </select>
            </div>
        </div>
        <div class="form-group">
        	<label for="User_Company" class="control-label col-lg-5">User Company:<span class="red-text">*</span></label>
            <div class="col-lg-7">
                <select name="User_Company" id="User_Company" class="companies" multiple>
                  <option value="" disabled selected>Choose your option</option>
                  <cfoutput query="getUserCompanies">
                      <option value="#Company_Code#" <cfif ListFind(ValueList(getUserCompany.Company_Name), Company_Name) NEQ 0>selected="selected"</cfif>> #Company_Name#</option>
                  </cfoutput>
                </select>
            </div>
        </div>
        <div class="form-group">
        	<label for="User_Select_Apps" class="control-label col-lg-5">User Applications:<span class="red-text">*</span></label>
            <div class="col-lg-7">
                <select name="User_Select_Apps" id="User_Select_Apps" class="companies apps" multiple>
                  <option value="" disabled selected>Choose your option</option>
                  <cfoutput query="getUserApplications">
                      <option value="#App_FullName#" <cfif ListFind(ValueList(getUserApplication.App_FullName), App_FullName) NEQ 0>selected="selected"</cfif>> #App_FullName#</option>
                  </cfoutput>
                </select>
            </div>
        </div>
    </div>
    
    <div class="col-lg-6 admin-perm-cont" <cfif getUserTypeRole.UserType_ID EQ 2>style="display:none;"</cfif>>
        <div class="form-group">
            <label for="Apply_Credit" class="control-label col-lg-5">Apply Credit:<span class="red-text">*</span></label>
            <div class="col-lg-2 apply-check-cont">
                <input type="checkbox" name="Apply_Credit" id="Apply_Credit" class="form-control apply-check" <cfif getUserTypeRole.CreditCard EQ 1>checked="checked"</cfif>/>
            </div>
        </div>
        <div class="form-group">
            <label for="Apply_Deposit" class="control-label col-lg-5">Apply Deposit:<span class="red-text">*</span></label>
            <div class="col-lg-2 apply-check-cont">
                <input type="checkbox" name="Apply_Deposit" id="Apply_Deposit" class="form-control apply-check" <cfif getUserTypeRole.DepositCard EQ 1>checked="checked"</cfif>/>
            </div>
        </div>
    </div>
    
    <div class="col-lg-12 line-break pad-btm"></div>
        
    <cfoutput>
        <div class="col-lg-12">&nbsp;</div>
        <div class="col-lg-12">
            <div class="form-group">
                <div class="col-lg-5"></div>
                <div class="col-lg-3">
                    <input type="hidden" name="User_UID" id="User_UID" value="#FORM.User_UID#" />
                    <input type="submit" value="Submit" class="btn btn-primary form-control" />
                </div>
            </div>
        </div>
    </cfoutput>
</form>
