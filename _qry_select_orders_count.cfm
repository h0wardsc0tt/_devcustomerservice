<cfparam name="QUERY" default="#StructNew()#">
<cfparam name="QUERY.QueryName" default="qry_getAllCustomers">
<cfparam name="QUERY.Datasource" default="#APPLICATION.Datasource_AX#">
<cfparam name="QUERY.SelectSQL" default="sorl.*">
<cfparam name="QUERY.OrderBy" default="sorl.CUSTOMERID">

<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#" maxrows="1000">
	SELECT #QUERY.SelectSQL#
	FROM 
		HMECPSALESORDERSLOOKUP sorl
	WHERE 0=0
	<cfif StructKeyExists(QUERY,"CUSTOMERID")>
	AND sorl.CUSTOMERID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.CUSTOMERID#" list="yes">)
	</cfif>
    <cfif StructKeyExists(QUERY,"DATAAREAID")>
	AND sorl.DATAAREAID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.DATAAREAID#">
	</cfif>
    <cfif StructKeyExists(QUERY,"SALESORDERNUMBER")>
	AND sorl.SALESORDERNUMBER LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.SALESORDERNUMBER#%">
	</cfif>
    <cfif StructKeyExists(QUERY,"PURCHASEORDERNUMBER")>
	AND sorl.PURCHASEORDERNUMBER LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.PURCHASEORDERNUMBER#%">
	</cfif>
    <cfif StructKeyExists(QUERY,"PARTNUMBER")>
	AND olin.PARTNUMBER LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.PARTNUMBER#%">
	</cfif>
    <cfif StructKeyExists(QUERY,"INVOICENUMBER")>
	AND ilup.INVOICENUMBER LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.INVOICENUMBER#%">
	</cfif>
    <cfif StructKeyExists(QUERY,"SALESORDERDATEFROM") AND StructKeyExists(QUERY,"SALESORDERDATETO")>
	AND sorl.SALESORDERDATE BETWEEN <cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.SALESORDERDATEFROM#"> AND <cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.SALESORDERDATETO#">
	</cfif>
</cfquery>

<cfquery name="#QUERY.QueryName#_Total" dbtype="query">
	SELECT Count(*) AS Records
	FROM #QUERY.QueryName#
</cfquery>
<cfif StructClear(QUERY)></cfif>