<!--- Set Defaults --->
<cfparam name="FORM.Cust_Number_Anchor" default="">

<cfoutput>
    <div class="col-lg-12">
    	<h2>Select Customer</h2>
    </div>
    
    <!--- Error Message --->
    <cfif ERR.ErrorFound>
    	<div class="col-lg-12">
            <div class="form-message form-error">
                <ul>
                    <cfloop list="#ERR.ErrorMessage#" index="thisError"><li>#thisError#</li></cfloop>
                </ul>
            </div>
        </div>
    </cfif>

	<!--- Customer Search Form --->
    <form action="" method="POST" id="Cust_Search_Form" class="form-horizontal col-lg-12" role="form">
    	<div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="Cust_Number" class="control-label col-lg-5">Customer ID:</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="Cust_Number" id="Cust_Number" class="form-control" value="#FORM.Cust_Number#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="Cust_Name" class="control-label col-lg-5">Customer Name:</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="Cust_Name" id="Cust_Name" class="form-control" value="#FORM.Cust_Name#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="Cust_City" class="control-label col-lg-5">City:</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="Cust_City" id="Cust_City" class="form-control" value="#FORM.Cust_City#" />
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="Cust_State" class="control-label col-lg-5">State:</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="Cust_State" id="Cust_State" class="form-control" value="#FORM.Cust_State#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="Cust_PostCode" class="control-label col-lg-5">Postal Code:</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="Cust_PostCode" id="Cust_PostCode" class="form-control" value="#FORM.Cust_PostCode#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="Cust_Email" class="control-label col-lg-5">Email:</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="Cust_Email" id="Cust_Email" class="form-control" value="#FORM.Cust_Email#" />
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
            	<div class="form-group">
                    <label for="Cust_SN" class="control-label col-lg-5">Store Number:</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="Cust_SN" id="Cust_SN" class="form-control" value="#FORM.Cust_SN#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="Cust_SO" class="control-label col-lg-5">Sales Order:</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="Cust_SO" id="Cust_SO" class="form-control" value="#FORM.Cust_SO#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="Cust_INV" class="control-label col-lg-5">Invoice Number:</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="Cust_INV" id="Cust_INV" class="form-control" value="#FORM.Cust_INV#" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-4">
                <div class="form-group">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6 no-pad-search">
                    	<input type="hidden" name="SESSION_User_UID" id="SESSION_User_UID" value="#SESSION.User_UID#" />
                        <input type="submit" name="Cust_Search_Submit" id="Cust_Search_Submit" class="btn btn-primary form-control" value="Search" />
                    </div>
                </div>
            </div>
        </div>
    
        <div class="col-lg-12">&nbsp;</div>
        
        <!--- Customer Search Results --->
        <div class="col-lg-12 table-responsive no-pad">
            <h4>Search Results</h4>
            
            <table id="Cust_Results" class="table " style="white-space:nowrap;margin:0px;">
                <thead id="Cust_Results_Header">
                    <tr>
                        <th>CUSTOMER ID</th>
                        <th>COMPANY</th>
                        <th>NAME</th>
                        <th>ADDRESS</th>
                        <th>CITY</th>
                        <th>STATE</th>
                        <th>POSTCODE</th>
                    </tr>
                </thead>
                <tbody>
                    <cfloop from="1" to="#ArrayLen(CustomerList.DATA.CUSTOMERID)#" index="cust">
                        <tr>
                            <td><a href="javascript:void(0);" onclick="setCustNumber('#CustomerList.DATA.CUSTOMERID[cust]#');">#CustomerList.DATA.CUSTOMERID[cust]#</a></td>
                            <td>#CustomerList.DATA.DATAAREAID[cust]#</td>
                            <td>#CustomerList.DATA.CUSTOMERNAME[cust]#</td>
                            <td>#CustomerList.DATA.ADDRESS[cust]#</td>
                            <td>#CustomerList.DATA.ADDRESSCITY[cust]#</td>
                            <td>#CustomerList.DATA.ADDRESSSTATE[cust]#</td>
                            <td>#CustomerList.DATA.ADDRESSZIPCODE[cust]#</td>
                        </tr>
                    </cfloop>
                </tbody>
            </table>
           
            <div class="col-lg-12 no-pad csp-paginate-cont">
                <div class="col-lg-6 no-pad">
                    <div class="col-lg-1 no-pad">
                        <button id="CSP_Prev_Page" name="CSP_Prev_Page" class="btn btn-primary btn-left" <cfif VARIABLES.Page_Start EQ 1>disabled="disabled"</cfif>><span class="glyphicon glyphicon-arrow-left"></span></button>
                    </div>
                    <div class="col-lg-11 text-left">
                        <div class="csp-perpage">
                            <span>
                                Show
                                <select id="CSP_Per_Page" name="CSP_Per_Page">
                                    <option value="5" <cfif FORM.CSP_Per_Page EQ 5>selected="selected"</cfif>>5</option>
                                    <option value="10" <cfif FORM.CSP_Per_Page EQ 10>selected="selected"</cfif>>10</option>
                                    <option value="20" <cfif FORM.CSP_Per_Page EQ 20>selected="selected"</cfif>>20</option>
                                </select>
                                items per page
                           </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 no-pad">
                    <div class="col-lg-11 text-right"><div class="csp-results">Showing <span class="csp-page-start">#VARIABLES.Page_Start#</span> - <span class="csp-page-end">#VARIABLES.Page_End#</span> of (#CustomerCount#) Results</div></div>
                    <div class="col-lg-1 no-pad">
                        <button id="CSP_Next_Page" name="CSP_Next_Page" class="btn btn-primary btn-right pull-right" <cfif VARIABLES.Page_End GTE CustomerCount>disabled="disabled"</cfif>><span class="glyphicon glyphicon-arrow-right"></span></button>
                    </div>
                </div>
                <input type="hidden" id="CSP_Current_Page" name="CSP_Current_Page" value="#FORM.CSP_Current_Page#" />
            </div>
        </div>
    </form>
    
    <form name="Cust_Anchor_Form" id="Cust_Anchor_Form" method="post" action="./?pg=Orders">
		<input type="hidden" name="Cust_Number_Anchor" id="Cust_Number_Anchor" value="#FORM.Cust_Number_Anchor#" />
    </form>
</cfoutput>

<cfinclude template="./_tmp_search_form.cfm">